from allauth.socialaccount.models import SocialApp, SocialAccount
from django.contrib.auth.models import User, Permission, Group
from sgas.apps.usuarios.models import Perfil
from sgas.apps.proyectos.models import Proyecto, Fase, LineaBase, SolicitudCambio
from sgas.apps.miembros.models import Miembro
from sgas.apps.tipoitem.models import TipoItem, AdicionalNumero, AdicionalCadena, AdicionalFecha, AdicionalArchivo
from sgas.apps.items.models import Item, CampoNumero, CampoCadena, CampoFecha, CampoArchivo, Version
from sgas.apps.roles.models import Rol
from sgas.apps.relaciones.models import Relacion
from django.db.models import Q
from datetime import datetime


def crearUsuarios():
    perm = Permission.objects.get(codename='acceso_usuario')
    usernames = ['fabri', 'ana', 'hugo', 'maxi', 'fer', 'fran']
    nombres = ['Fabrizio Sebastian', 'Ana', 'Hugo', 'Iván', 'Luis Fernando', 'Francisco Fernando']
    apellidos = ['Coscia Nuñez', 'Rivarola', 'Giménez', 'Duré', 'Caballero Ramoa', 'Candia Santacruz']
    emails = ['fabricoscia@fpuna.edu.py', 'anarivarola76@gmail.com', 'hg4489702@gmail.com', 'ivamax14@gmail.com', 'luisfercaballero27@fpuna.edu.py', 'ffcs98@fpuna.edu.py']
    telefonos = ['0984289180', '0992147562', '0985555471', '0991710656', '0981907288', '0984769015']
    cis = ['5173025', '4177075', '4177074', '4976664', '4488245', '4361087']
    uids = ['103321807540989012190', '111247877181174692442', '113768332375416603091', '103082307416188774280', '107215971786395261335', '109431546496288929715']
    for x in range(0, len(usernames)):
        user = User.objects.create_user(username=usernames[x], email=emails[x])
        user.first_name = nombres[x]
        user.last_name = apellidos[x]
        user.save()
        Perfil.objects.create(user=user, ci=cis[x], telefono=telefonos[x])
        sacc = SocialAccount(uid=uids[x], user=user, provider='google')
        sacc.save()
        user.user_permissions.add(perm)
    print('Usuarios creados')


def usuarioAproyecto(fases, proyecto):
    perfiles = Perfil.objects.filter(~Q(id=1))
    for perfil in perfiles:
        Miembro.objects.create(idProyecto=proyecto, idPerfil=perfil)
    #Creación de Fases
    for x in range(0, len(fases)):
        fase = Fase.objects.create(nombre=fases[x], posicion=x+1, proyecto=proyecto)
        fase.save()
        fases[x] = fase
    return fases


def crearTipos(tipos, proyecto, fases):
    listaTipos = []
    for key in tipos.keys():
        aux = tipos[key]
        tipo = TipoItem.objects.create(nombre=key, descripcion=aux[0], proyecto=proyecto, fase=fases[int(aux[1])-1])
        tipo.save()
        listaTipos.append(tipo)
        for x in range(2, len(aux), 2):
            if aux[x] == 'Numero':
                AdicionalNumero.objects.create(tipo=tipo, nombre=aux[x+1],opcional=True)
            elif aux[x] == 'Cadena':
                AdicionalCadena.objects.create(tipo=tipo, nombre=aux[x+1], opcional=True)
            elif aux[x] == 'Fecha':
                AdicionalFecha.objects.create(tipo=tipo, nombre=aux[x+1], opcional=True)
            elif aux[x] == 'Archivo':
                AdicionalArchivo.objects.create(tipo=tipo, nombre=aux[x+1], opcional=True)
    return listaTipos

def crearRoles(roles, fases, comite, proyecto):
    r = []
    for key in roles.keys():
        nombreRol = key
        nombreGrupo = '{}_{}'.format(nombreRol, proyecto.id)
        grupo = Group.objects.create(name =nombreGrupo)
        permisos = Permission.objects.filter(codename__in=roles[key])
        grupo.permissions.set(permisos)
        grupo.save()
        rol = Rol.objects.create(nombre=key, grupo=grupo, proyecto=proyecto)
        rol.save()
        rol.fase.set([fases[0], fases[1], fases[2]])
        rol.save()
        r.append(rol)
    #Asignación de Roles

    #Desarrolladores
    rol = Rol.objects.get(id=r[0].id)
    user = User.objects.get(email='ivamax14@gmail.com')
    rol.grupo.user_set.add(user)
    user = User.objects.get(email='fabricoscia@fpuna.edu.py')
    rol.grupo.user_set.add(user)
    user = User.objects.get(email='luisfercaballero27@fpuna.edu.py')
    rol.grupo.user_set.add(user)
    user = User.objects.get(email='ffcs98@fpuna.edu.py')
    rol.grupo.user_set.add(user)

    rol = Rol.objects.get(id=r[1].id)
    user = User.objects.get(email='anarivarola76@gmail.com')
    comite.user_set.add(user)
    rol.grupo.user_set.add(user)
    user = User.objects.get(email='hg4489702@gmail.com')
    comite.user_set.add(user)

    user = User.objects.get(email='fabricoscia@fpuna.edu.py')
    comite.user_set.add(user)
    return r


def crearItems(items, fases, listaTipos):
    listaItems = []
    for key in items.keys():
        aux = items[key]
        item = Item.objects.create(fase=fases[int(aux[0])-1], tipo=listaTipos[int(aux[1])-1], identificador=key, costo=int(aux[2]), descripcion=aux[3])
        item.save()
        listaItems.append(item)
        listaTipos[int(aux[1])-1].estado = 'Activo'
        listaTipos[int(aux[1])-1].numItems = listaTipos[int(aux[1])-1].numItems + 1
        listaTipos[int(aux[1])-1].save()
        fases[int(aux[0])-1].numItems = fases[int(aux[0])-1].numItems + 1
        fases[int(aux[0])-1].save()
        for x in range(4, len(aux), 3):
            if aux[x] == 'Numero':
                CampoNumero.objects.create(item=item, nombre=aux[x+1], numero=int(aux[x+2]), opcional=True)
            elif aux[x] == 'Cadena':
                CampoCadena.objects.create(item=item, nombre=aux[x+1], cadena=aux[x+2], opcional=True)
            elif aux[x] == 'Fecha':
                CampoFecha.objects.create(item=item, nombre=aux[x+1], fecha=datetime.now(), opcional=True)
            elif aux[x] == 'Archivo':
                CampoArchivo.objects.create(item=item, nombre=aux[x+1], archivo=aux[x+2], opcional=True)
        ver = Version()
        ver.versionar(item.id)
    return listaItems


def crearLBS(lbs, fases):
    listaLB = []
    for key in lbs.keys():
        aux = lbs[key]
        pos = aux[0]
        aux = aux[1]
        lb = LineaBase.objects.create(nombre=key, fechaCreacion=datetime.now(),
                                      estado='Cerrada', cantItems=len(aux),
                                      creador=User.objects.get(email='fabricoscia@fpuna.edu.py'),
                                      fase=fases[int(pos)-1])
        lb.save()
        lb.items.set(aux)
        lb.save()
        fases[int(pos)-1].numLB = fases[int(pos)-1].numLB + 1
        fases[int(pos)-1].save()
        listaLB.append(lb)
    return listaLB


#===Proyecto Listo para Finalizar===
def proyectoCompleto():
    #Creación del proyecto

    fases = ['Planificación', 'Desarrollo', 'Pruebas']
    usuario = User.objects.get(email='hg4489702@gmail.com')
    gerente = Perfil.objects.get(user=usuario)
    proyecto = Proyecto.objects.create(nombre='IS2_G04_IT5', descripcion='Proyecto para la entrega de la iteración 5',
                            numFases='3', gerente=gerente)
    proyecto.save()
    comite = Group.objects.create(name='comite%s' % proyecto.id)
    proyecto.comite = comite
    proyecto.save()

    #Añadir usuarios al proyecto
    fases = usuarioAproyecto(fases, proyecto)

    #Creación de Tipos de Ítem
    #Estructura del diccionario:
    #Clave ---> Nombre del tipo de ítem
    #Campos ---> Descripción, Posición de la fase, Campo adicional (si posee), nombre del campo
    tipos = {'RF': ['Requisito Funcional', '1', 'Cadena', 'Encargado'], 'RNF': ['Requisito No Funcional', '1'],
             'Código': ['Código Fuente', '2', 'Archivo', 'Código'], 'Ejecutable':['Software desarrollado', '2'],
             'Prueba':['Pruebas de Software', '3', 'Cadena', 'Tipo de Prueba', 'Fecha', 'Realizado el']}

    listaTipos = crearTipos(tipos, proyecto, fases)

    #Creación de roles
    #Estructura del diccionario:
    #Clave ---> Nombre del rol
    #Valor ---> Lista de permisos

    roles = {'Desarrollador': ['Crear item', 'Modificar item', 'Desactivar item', 'Restaurar item', 'Agregar ph', 'Agregar as', 'Eliminar relaciones'],
             'QA': ['Aprobar item', 'Desaprobar item', 'Crear lb']}

    r = crearRoles(roles, fases, comite, proyecto)

    #Iniciamos el proyecto
    proyecto.estado = 'Iniciado'
    proyecto.fechaInicio = datetime.now()
    proyecto.save()

    #Creamos ítems
    #Estructura del diccionario:
    #Clave ---> Identificador del ítem
    #Campos ---> Posición de la fase, tipo de ítem, costo, descripcion
    items = {'RF_1':['1', '1', '6', 'Archivos en la nube', 'Cadena', 'Encargado', 'Fernando Caballero'],
             'RF_2':['1', '1', '4', 'Notificaciones por correo', 'Cadena', 'Encargado', 'Fabrizio Coscia'],
             'RF_3':['1', '1', '8', 'Grafo de forma gráfica', 'Cadena', 'Encargado', 'Iván Duré'],
             'RNF_1':['1', '2', '10', 'Django'],
             'Código_1':['2', '3', '4', 'Módulo de Desarrollo'],
             'Código_2':['2', '3', '4', 'Módulo de Gestión'],
             'Código_3':['2', '3', '4', 'Módulo de Administración'],
             'Prueba_1':['3', '5', '2', 'Prueba del módulo de desarrollo', 'Cadena', 'Tipo de Prueba', 'Unitaria', 'Fecha', 'Realizado el', 'hoy'],
             'Prueba_2':['3', '5', '5', 'Prueba del módulo de gestión', 'Cadena', 'Tipo de Prueba','Unitaria', 'Fecha', 'Realizado el','hoy'],
             'Prueba_3':['3', '5', '4', 'Prueba del módulo de administración', 'Cadena', 'Tipo de Prueba','Unitaria', 'Fecha', 'Realizado el','hoy'],
             'Prueba_4':['3', '5', '10', 'Prueba general', 'Cadena', 'Tipo de Prueba','Integración','Fecha', 'Realizado el','hoy'],
    }
    listaItems = crearItems(items, fases, listaTipos)

    #Relacionamos los ítems
    #RNF_1 es padre de RF_1, RF_2, RF_3
    #RNF_1 es antecesor de Código_1
    #RF_2 es ancetesor de Código_2
    #Código_2 es padre de Código_3
    #Código_1 es antecesor de Prueba_1
    #Código_3 es antecesor de Prueba_3
    #Prueba_1 es padre de Prueba_2
    #Prueba_3 es padre de Prueba_4
    for x in range(0, 3):
        Relacion.objects.create(tipo=2, itemA=listaItems[3], itemB=listaItems[x])
        listaItems[x].version = listaItems[x].version + 1
        listaItems[x].save()
        ver = Version()
        ver.versionar(listaItems[x].id)
    Relacion.objects.create(tipo=1, itemA=listaItems[3], itemB=listaItems[4])
    listaItems[4].version = listaItems[4].version + 1
    listaItems[4].save()
    ver = Version()
    ver.versionar(listaItems[4].id)
    Relacion.objects.create(tipo=1, itemA=listaItems[1], itemB=listaItems[5])
    listaItems[5].version = listaItems[5].version + 1
    listaItems[5].save()
    ver = Version()
    ver.versionar(listaItems[5].id)
    Relacion.objects.create(tipo=2, itemA=listaItems[5], itemB=listaItems[6])
    listaItems[6].version = listaItems[6].version + 1
    listaItems[6].save()
    ver = Version()
    ver.versionar(listaItems[6].id)
    Relacion.objects.create(tipo=1, itemA=listaItems[4], itemB=listaItems[7])
    listaItems[7].version = listaItems[7].version + 1
    listaItems[7].save()
    ver = Version()
    ver.versionar(listaItems[7].id)
    Relacion.objects.create(tipo=1, itemA=listaItems[6], itemB=listaItems[9])
    listaItems[9].version = listaItems[9].version + 1
    listaItems[9].save()
    ver = Version()
    ver.versionar(listaItems[9].id)
    Relacion.objects.create(tipo=2, itemA=listaItems[7], itemB=listaItems[8])
    listaItems[8].version = listaItems[8].version + 1
    listaItems[8].save()
    ver = Version()
    ver.versionar(listaItems[8].id)
    Relacion.objects.create(tipo=2, itemA=listaItems[9], itemB=listaItems[10])
    listaItems[10].version = listaItems[10].version + 1
    listaItems[10].save()
    ver = Version()
    ver.versionar(listaItems[10].id)
    #Aprobación de ítems
    for item in listaItems:
        item.estado = 'Aprobado'
        item.save()

    #Creación de Líneas Base
    lbs = {'LB_Planificación_1':['1', [listaItems[0], listaItems[1], listaItems[2]]],
           'LB_Planificación_2':['1', [listaItems[3]]],
           'LB_Desarrollo_1':['2',[listaItems[4], listaItems[5],listaItems[6]]],
           'LB_Pruebas_1':['3',[listaItems[7], listaItems[8]]],
           'LB_pruebas_2':['3', [listaItems[9], listaItems[10]]]
    }

    listaLB = crearLBS(lbs, fases)

    for item in listaItems:
        item.estado = 'En LB'
        item.save()

    solicitud = SolicitudCambio.objects.create(nombre='SOLICITUD_IS2_G04_IT5_Planificación_LB_Planificación_1',
                                   responsable=proyecto.gerente.user,aFavor=0, enContra=3, motivo='Mala planificación',
                                               lb=listaLB[0], estado='Desaprobada')
    solicitud.save()
    solicitud.items.set([listaItems[0]])
    solicitud = SolicitudCambio.objects.create(nombre='SOLICITUD_IS2_G04_IT5_Planificación_LB_Planificación_1',
                                   responsable=proyecto.gerente.user,aFavor=2, enContra=1, motivo='Mala planificación',
                                               lb=listaLB[0], estado='Completada')
    solicitud.save()
    solicitud.items.set([listaItems[0]])
    solicitud.save


#Proyecto con ítems excepto en la última fase
def proyectoParcial():
    #Creación del proyecto

    fases = ['Administración', 'Gestión', 'Finalización']
    usuario = User.objects.get(email='hg4489702@gmail.com')
    gerente = Perfil.objects.get(user=usuario)
    proyecto = Proyecto.objects.create(nombre='Proyecto IT5', descripcion='Proyecto para la entrega de la iteración 5',
                            numFases='3', gerente=gerente)
    proyecto.save()
    comite = Group.objects.create(name='comite%s' % proyecto.id)
    proyecto.comite = comite
    proyecto.save()

    #Añadir usuarios al proyecto
    fases = usuarioAproyecto(fases, proyecto)

    #Creación de Tipos de Ítem
    #Estructura del diccionario:
    #Clave ---> Nombre del tipo de ítem
    #Campos ---> Descripción, Posición de la fase, Campo adicional (si posee), nombre del campo
    tipos = {'Memo': ['Memos Varios', '1', 'Cadena', 'Mensaje'],
             'Planilla': ['Planilla electrónica', '2', 'Archivo', 'Excel'], 'Recordatorio':['Recordar', '2', 'Cadena', 'Mensaje'],
             'Inventario':['Cierre de Inventario', '3', 'Numero', 'Saldo', 'Fecha', 'Realizado el']}

    listaTipos = crearTipos(tipos, proyecto, fases)

    #Creación de roles
    #Estructura del diccionario:
    #Clave ---> Nombre del rol
    #Valor ---> Lista de permisos

    roles = {'Desarrollador': ['Crear item', 'Modificar item', 'Desactivar item', 'Restaurar item', 'Agregar ph', 'Agregar as', 'Eliminar relaciones'],
             'QA': ['Aprobar item', 'Desaprobar item', 'Crear lb']}

    r = crearRoles(roles, fases, comite, proyecto)

    #Iniciamos el proyecto
    proyecto.estado = 'Iniciado'
    proyecto.fechaInicio = datetime.now()
    proyecto.save()

    #Creamos ítems
    #Estructura del diccionario:
    #Clave ---> Identificador del ítem
    #Campos ---> Posición de la fase, tipo de ítem, costo, descripcion
    items = {'Memo_1':['1', '1', '2', 'Resolución Exámenes', 'Cadena', 'Mensaje', 'Examenes de forma virtual'],
             'Memo_2':['1', '1', '4', 'Resolución Semestre', 'Cadena', 'Mensaje', 'Un solo semestre en el 2020'],
             'Memo_3':['1', '1', '6', 'Resolución Clases', 'Cadena', 'Mensajes', 'Clases por educa'],
             'Recordatorio_1':['2', '3', '1', 'IT5', 'Cadena', 'Mensaje', 'Entrega de la IT5 el 16/10'],
             'Recordatorio_2':['2', '3', '1', 'IT6', 'Cadena', 'Mensaje', 'Entrega en noviembre'],
             'Recordatorio_3':['2', '3', '6', 'Segundo parcial', 'Mensaje', 'SP empieza a las 14:00 hs.']
    }
    listaItems = crearItems(items, fases, listaTipos)
    Relacion.objects.create(tipo=2, itemA=listaItems[0], itemB=listaItems[1])
    ver = Version()
    ver.versionar(listaItems[1].id)
    Relacion.objects.create(tipo=2, itemA=listaItems[0], itemB=listaItems[2])
    ver = Version()
    ver.versionar(listaItems[2].id)
    Relacion.objects.create(tipo=1, itemA=listaItems[1], itemB=listaItems[4])
    ver = Version()
    ver.versionar(listaItems[4].id)
    Relacion.objects.create(tipo=2, itemA=listaItems[4], itemB=listaItems[3])
    ver = Version()
    ver.versionar(listaItems[3].id)
    Relacion.objects.create(tipo=2, itemA=listaItems[4], itemB=listaItems[5])
    ver = Version()
    ver.versionar(listaItems[5].id)
    #Aprobación de ítems
    for item in listaItems:
        item.estado = 'Aprobado'
        item.save()

    #Creación de Líneas Base
    lbs = {'LB_Administración_1':['1', [listaItems[0], listaItems[1], listaItems[2]]],
           'LB_Gestión_1':['2',[listaItems[3], listaItems[4],listaItems[5]]]
    }

    crearLBS(lbs, fases)

    for item in listaItems:
        item.estado = 'En LB'
        item.save()



def proyectoIniciado():
    fases = ['Inicio', 'Medio', 'Fin']
    usuario = User.objects.get(email='hg4489702@gmail.com')
    gerente = Perfil.objects.get(user=usuario)
    proyecto = Proyecto.objects.create(nombre='Proyecto Iniciado', descripcion='Proyecto en estado iniciado para la entrega de la iteración 5',
                            numFases='3', gerente=gerente)
    proyecto.save()
    comite = Group.objects.create(name='comite%s' % proyecto.id)
    proyecto.comite = comite
    proyecto.save()

    #Añadir usuarios al proyecto
    fases = usuarioAproyecto(fases, proyecto)

    #Creación de Tipos de Ítem
    #Estructura del diccionario:
    #Clave ---> Nombre del tipo de ítem
    #Campos ---> Descripción, Posición de la fase, Campo adicional (si posee), nombre del campo
    tipos = {'Resolución': ['Resoluciones Varias', '1', 'Cadena', 'Mensaje'],
             'Presupuesto': ['Planillas de presupuestos', '2', 'Archivo', 'Excel'], 'Materiales':['Lista de Materiales', '2', 'Cadena', 'Lista'],
             'Cierre':['Cierre de planillas', '3', 'Numero', 'Saldo', 'Fecha', 'Realizado el']}

    listaTipos = crearTipos(tipos, proyecto, fases)

    #Creación de roles
    #Estructura del diccionario:
    #Clave ---> Nombre del rol
    #Valor ---> Lista de permisos

    roles = {'Desarrollador': ['Crear item', 'Modificar item', 'Desactivar item', 'Restaurar item', 'Agregar ph', 'Agregar as', 'Eliminar relaciones'],
             'QA': ['Aprobar item', 'Desaprobar item', 'Crear lb']}

    r = crearRoles(roles, fases, comite, proyecto)

    #Iniciamos el proyecto
    proyecto.estado = 'Iniciado'
    proyecto.fechaInicio = datetime.now()
    proyecto.save()



def proyectoCreado():
    fases = ['Diseño', 'Maquetación', 'Construcción']
    usuario = User.objects.get(email='hg4489702@gmail.com')
    gerente = Perfil.objects.get(user=usuario)
    proyecto = Proyecto.objects.create(nombre='Proyecto Creado', descripcion='Proyecto en estado Pendiente para la entrega de la iteración 5',
                            numFases='3', gerente=gerente)
    proyecto.save()
    comite = Group.objects.create(name='comite%s' % proyecto.id)
    proyecto.comite = comite
    proyecto.save()

    #Añadir usuarios al proyecto
    fases = usuarioAproyecto(fases, proyecto)

    #Creación de Tipos de Ítem
    #Estructura del diccionario:
    #Clave ---> Nombre del tipo de ítem
    #Campos ---> Descripción, Posición de la fase, Campo adicional (si posee), nombre del campo
    tipos = {'Boceto': ['Diseño inicial', '1', 'Cadena', 'Autor','Archivo', 'Diseño'], 'CostoB':['Costo del boceto', '2', 'Numero', 'Costo'],
             'Maqueta':['Maquetación del diseño', '2', 'Cadena', 'Autor', ], 'CostoM':['Costo de la maqueta', '2', 'Numero', 'Costo'],
             'CostoC':['Costo de construcción', '3', 'Numero', 'Costo', 'Fecha', 'Inicia el:']}

    listaTipos = crearTipos(tipos, proyecto, fases)

    #Creación de roles
    #Estructura del diccionario:
    #Clave ---> Nombre del rol
    #Valor ---> Lista de permisos

    roles = {'Desarrollador': ['Crear item', 'Modificar item', 'Desactivar item', 'Restaurar item', 'Agregar ph', 'Agregar as', 'Eliminar relaciones'],
             'QA': ['Aprobar item', 'Desaprobar item', 'Crear lb']}
    r = crearRoles(roles, fases, comite, proyecto)


def start():
    crearUsuarios()
    print("===================================================")
    print('           POBLACIÓN de BASE DE DATOS              ')
    print("===================================================")
    print("Se crearán proyectos con las siguientes características:")
    print("1- Proyecto Creado sin iniciar")
    print("2- Proyecto iniciado sin ítems")
    print("3- Proyecto iniciado con ítems")
    print("4- Proyecto preparado para finalizar")
    print('Aguarde un momento')
    proyectoCompleto()
    proyectoParcial()
    proyectoIniciado()
    proyectoCreado()
