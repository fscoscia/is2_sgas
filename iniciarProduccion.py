#!/usr/bin/env python3
import os
import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sgas.settings.production")
import django
django.setup()
from allauth.socialaccount.models import SocialApp, SocialAccount
from django.contrib.auth.models import User, Permission, Group
from sgas.apps.usuarios.models import Perfil
from utils import start

# ===Script de Inicialización - Entorno de Desarrollo===

def iniciarSApp():
    '''
    Crea el vínculo entre el SSO y django, utilizando el modelo SocialApp de la librería allauth,
    dicho modelo recibe el nombre del proveedor, en este caso Google, y los tokens generedos por el
    mismo.
    '''
    sapp = SocialApp(provider='google', name='GL',
        client_id='340948295678-fmsmf4lhkgo79icmft0vr5h416e7k0s1.apps.googleusercontent.com',
        secret='j9-pjZFFAxlpcewPC8HxjNpH')
    sapp.save()
    sapp.sites.add(1)
    print('Vínculo con el SSO creado')

def crearRoles():
    '''
    Crea los roles iniciales del sistema, administrador y gerente, y asigna los permisos correspondientes.
    '''
    grupo = Group.objects.create(name ='administrador')
    permisos = Permission.objects.filter(codename__in=['acceso_usuario', 'autorizar_usuario'])
    grupo.permissions.set(permisos)
    grupo.save()
    print('Roles creados')


def iniciarAdmin():
    '''
    Crea el usuario administrador y le asigna el rol correspondiente.
    Luego de la creación del administrador se realiza la vinculación con su cuenta de google
    '''
    user = User.objects.create_user(username='admin', email='is2.sgas@gmail.com')
    user.first_name = 'Administrador'
    user.last_name = 'SGAS'
    user.save()
    Perfil.objects.create(user=user, ci='607481', telefono='0961548732')
    grupo = Group.objects.get(name ='administrador')
    grupo.user_set.add(user)
    sacc = SocialAccount(uid='103349591566677637955', user=user, provider='google')
    sacc.save()
    print('Usuario administrador creado')

iniciarSApp()
crearRoles()
iniciarAdmin()
if len(sys.argv) > 0:
    start()
