#!/bin/bash
cd /is2
apt-get install python3-pip
pip3 install virtualenv
virtualenv -p /usr/bin/python3.7 venv
source venv/bin/activate
cd /is2/is2_sgas/scripts
pip3 install -r requirements.txt

