import os
import django
import pytest

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sgas.settings.local")
django.setup()


@pytest.mark.django_db
def test_connect(client):
    response = client.get("http://127.0.0.1:8000")
    assert response.status_code == 200

# @pytest.mark.django_db
# def test_google(client):
#     response = client.get("http://127.0.0.1:8000/accounts/google/login/?process=&action=reauthenticate")
#     assert response.status_code == 302
