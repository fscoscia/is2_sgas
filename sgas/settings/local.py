# local.py
from .base import *
from google.oauth2 import service_account

DEBUG = True
ALLOWED_HOSTS = ['testserver','127.0.0.1','localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'desarrollo',
        'USER': 'usuario_desarrollo',
        'PASSWORD': 'desarrollo',
        'HOST': 'localhost',
        'PORT': '',
    }
}

STATIC_URL = '/static/'
#STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_DIRS = (os.path.join(BASE_DIR), 'sgas/static')

#Prueba fetch

#Configuracion Google Cloud Service
DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
GS_BUCKET_NAME = 'sgas-is2'
GS_DEFAULT_ACL = 'publicRead'
GS_FILE_OVERWRITE = 'False'
GS_CREDENTIALS = service_account.Credentials.from_service_account_file(
    "sgas/settings/GCServiceAccount-a3f791e00144.json"
)
