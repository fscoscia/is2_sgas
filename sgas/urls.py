
from django.contrib import admin
from django.urls import path, include

urlpatterns = [

    # En este apartado se listan las urls predefinidas para el
    # autenticador de Google y urls del SGAS.
    #
    # 1. **URL** admin
    # 2. **URL** cuentas
    # 3. **URL** de proyectos
    # 4. **URL** de usuarios
    # 5. **URL** inicio

    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('proyectos/', include(('sgas.apps.proyectos.urls', 'proyectos'), namespace='proyectos')),
    path('usuarios/', include(('sgas.apps.usuarios.urls', 'usuarios'), namespace='usuarios')),
    path('', include('sgas.apps.proyectos.urls')),
    path('proyectos/<int:idProyecto>/miembros/', include(('sgas.apps.miembros.urls', 'miembros'), namespace='miembros')),
    path('proyectos/<int:idProyecto>/tipoitem/', include(('sgas.apps.tipoitem.urls', 'tipoitem'), namespace='tipoitem')),
    path('proyectos/<int:idProyecto>/roles/', include(('sgas.apps.roles.urls', 'roles'), namespace='roles')),
    path('proyectos/<int:idProyecto>/fases/<int:idFase>/tipoitem/<int:idTipo>/item/', include(('sgas.apps.items.urls', 'items'), namespace='items')),
    path('proyectos/<int:idProyecto>/fases/<int:idFase>/tipoitem/<int:idTipo>/item/<int:idItem>/relaciones/', include(('sgas.apps.relaciones.urls', 'relaciones'), namespace='relaciones')),
]
