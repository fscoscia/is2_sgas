
const COLORES_NODOS = ["#26798e", "#63caa7", "#ffc172", "#ffe3b3"]
const COLORES_ARISTAS = ["#c9284f", "#ff6666"]
const WIDTH = $('.contenedor').width();
const HEIGHT = 500
const R = 10

// Separar items por fases
let fases = {}
for(let i = 0; i < jsonDict.nodos.length; i++){
    if(fases[jsonDict.nodos[i].faseid] === undefined){
        fases[jsonDict.nodos[i].faseid] = []
    }
    fases[jsonDict.nodos[i].faseid].push(jsonDict.nodos[i])
}

function separarItemsPorCapas(items, vertices){
    let grados = {}
    let niveles = {}
    let visitados = {}
    let maxCapas = 0
    for(let i of items){
        visitados[i.id] = false
        for(let h of i.hijos){
            grados[h.itemBid] = grados[h.itemBid] === undefined ? 1 : grados[h.itemBid]+1
            niveles[h.itemBid] = niveles[h.itemBid] === undefined ? 1 : niveles[h.itemBid]+1
            maxCapas = Math.max(maxCapas, niveles[h.itemBid])
        }
    }
    let queue = []
    for(i of items){
        if(grados[i.id] === undefined){
            queue.push(i)
        }
    }
    let capas = []
    let c = 0
    for(let i = 0; i < maxCapas+1; i++){
        capas.push([])
    }
    while(queue.length){
        let node = queue.shift()
        visitados[node.id] = true
        let nivel = niveles[node.id] === undefined ? 0 : niveles[node.id]
        capas[nivel].push(node)
        for(let h of node.hijos){
            if(!visitados[h.itemBid]){
                if(grados[h.itemBid]-1==0){
                    let next = items.find(i => i.id == h.itemBid)
                    grados[h.itemBid] = 0
                    queue.push(next)
                }else{
                    grados[h.itemBid] = grados[h.itemBid]-1
                }
            }
        }
    }
    return capas
}

function obtenerCoordenadas(capas, ite, pw, h){
    let coordenadas = {}

    for(let i=0; i<capas.length; i++){
        let n = capas.length
        for(let j=0; j<capas[i].length; j++){
            let m = capas[i].length
            let dx = pw / (n+1)
            let dy = h / (m+1)

            coordenadas[capas[i][j].id] = {id:capas[i][j].id, x:(dx*(i+1)+(pw*ite)), y:(dy*(j+1))}
        }
    }

    return coordenadas
}

function colocarNodos(svg, coordenadas, datos){
    const elem = svg.selectAll("g circle").data(coordenadas)
    const elemEnter = elem.enter().append("g")
    elemEnter.append("circle")
        .attr("id", d => d.id)
        .attr("cx", d => d.x)
        .attr("cy", d => d.y)
        .attr("r", R)
        .style("fill", function(d){ return COLORES_NODOS[datos[d.id].color]; })
        .attr('data-toggle', 'tooltip')
        .attr('data-html', 'true')
        .attr('data-placement', 'top')
        .attr('title', d => `<b>${datos[d.id].nombre}<br/></b>
                <span style="text-align: left;">Estado: ${datos[d.id].estado}<br/></span>
                <span style="text-align: left;">Costo: ${datos[d.id].costo}<br/></span>`)
    elemEnter.append("text")
        .attr("dx", function (d) {
            return d.x - 15
        })
        .attr("dy", function (d) {
            return d.y - 15
        })
        .text(function (d) {
            return datos[d.id].nombre
        })

}

function colocarAristas(svg, aristas){
    console.log(aristas)
    svg.selectAll(".line")
        .data(aristas)
        .enter()
        .append("line")
        .attr("x1", d => {console.log(d); return d.iniX + R})
        .attr("y1", d => d.iniY)
        .attr("x2", d => d.finX - R)
        .attr("y2", d => d.finY)
        .attr("stroke", d => COLORES_ARISTAS[d.tipo-1])
        .attr("stroke-width", 2)
        .attr("marker-end", "url(#arrow)");
}

function colocarBarrasVerticales(svg, coorV){
    svg.selectAll(".line")
        .data(coorV)
        .enter()
        .append("line")
        .attr("x1", d => d.x1)
        .attr("y1", d => d.y1)
        .attr("x2", d => d.x2)
        .attr("y2", d => d.y2)
        .attr("stroke", "#8c8c8c")
        .attr("stroke-width", 1);
}

function colocarBarrasHorizontales(svg, coorH){
    svg.selectAll(".line")
        .data(coorH)
        .enter()
        .append("line")
        .attr("x1", d => d.x1)
        .attr("y1", d => d.y1)
        .attr("x2", d => d.x2)
        .attr("y2", d => d.y2)
        .attr("stroke", "#8c8c8c")
        .attr("stroke-width", 1);
}

function colocarTitulosDeFases(svg, coorT){
    svg.selectAll(".title")
        .data(coorT)
        .enter()
        .append("text")
        .style("text-anchor", "middle")
        .attr("dx", d => d.x)
        .attr("dy", d => d.y)
        .text(function (d) {
            return d.nombre
        })
}

for(let f in fases){
    for(let i of fases[f]){
        i.hijos = jsonDict.vertices.filter(v => v.tipo == 2 && v.itemAid == i.id)
        i.sucesores = jsonDict.vertices.filter(v => v.tipo == 1 && v.itemAid == i.id)
    }
}

let svg = d3.select("svg")
            .style('width', WIDTH)
            .call(d3.zoom().on("zoom", () => {
                    svg.attr("transform", d3.event.transform)
                })).call(d3.zoom().on("zoom", () => {
                    svg.attr("transform", d3.event.transform)
                }))
            .append("g")

let f = Object.keys(fases)
let ite = -1
const PW = WIDTH/f.length

let coordenadasTotal = {}
for(let items in fases){
    ite++
    let capas = separarItemsPorCapas(fases[items], jsonDict.vertices)
    let coordenadas = obtenerCoordenadas(capas, ite, PW, HEIGHT)
    coordenadasTotal = {...coordenadasTotal, ...coordenadas}

}
let datos = {}
for(let i=0; i<jsonDict.nodos.length; i++){
    let color = 0
    let e = jsonDict.nodos[i].estado
    if(e == "En Desarrollo"){
        color = 0
    }else if(e == "Aprobado"){
        color = 1
    }else if(e == "En LB"){
        color = 2
    }else{
        color = 3
    }
    datos[jsonDict.nodos[i].id] = {id: jsonDict.nodos[i].id, nombre: jsonDict.nodos[i].identificador, costo: jsonDict.nodos[i].costo, estado: e, color:color}
}
let coor = Object.values(coordenadasTotal)
colocarNodos(svg, coor, datos)

let aristas = {}
for(let i=0; i<jsonDict.vertices.length; i++){
    let x1 = coordenadasTotal[jsonDict.vertices[i].itemAid].x
    let x2 = coordenadasTotal[jsonDict.vertices[i].itemBid].x
    let y1 = coordenadasTotal[jsonDict.vertices[i].itemAid].y
    let y2 = coordenadasTotal[jsonDict.vertices[i].itemBid].y
    let tipo  = jsonDict.vertices[i].tipo

    let dx = x1-x2
    let dy = y1-y2

    aristas[i] = {iniX: x1, iniY: y1, finX: x2, finY: y2, tipo: tipo}
}

colocarAristas(svg, Object.values(aristas))

let coorV = {}
for(let i=0; i<f.length+1; i++){
    coorV[i] = {x1:PW*i, x2:PW*i, y1:0, y2:HEIGHT}
}

colocarBarrasVerticales(svg, Object.values(coorV))

let coorH = {}
coorH[0] = {x1:0, x2:WIDTH, y1:0, y2:0}
coorH[1] = {x1:0, x2:WIDTH, y1:20, y2:20}
coorH[2] = {x1:0, x2:WIDTH, y1:HEIGHT, y2:HEIGHT}

colocarBarrasHorizontales(svg, Object.values(coorH))

let coorT = {}

for(let i=0; i<f.length; i++){
    coorT[f[i]] = {x:(PW*i)+(PW/2), y:15, nombre:fases[f[i]][0].fasenombre}
}

colocarTitulosDeFases(svg, Object.values(coorT))

$(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
