from django.contrib.auth.models import User
from sgas.apps.roles.models import Rol
from django.core.exceptions import NON_FIELD_ERRORS
from django.core.validators import MaxLengthValidator
from django.db import models


class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    ci = models.PositiveIntegerField(null = False, blank=False, unique=True, error_messages={"unique":"Ya existe una solicitud con esta CI"})
    telefono = models.PositiveIntegerField(blank=False, null= True)
    roles = models.ManyToManyField(Rol)

    class Meta:
         permissions = (("autorizar_usuario", "Permite autorizar el acceso a SGAS"),
                        ("acceso_usuario", "Permite el acceso a SGAS"),
                        ("edit_usuario", "Permite editar usuarios"),
                        ("elim_usuario", "Permite eliminar usuarios"),)

    def __str__(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)



