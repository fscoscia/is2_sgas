from django.urls import path, include

from sgas.apps.usuarios.views import home, CrearPerfil, ListarPerfil, editarPerfil, listaAcceso, \
    concederAcceso, eliminarPerfil, administrador, proyectos_usuario, bitacora

urlpatterns = [
    path(r'home/', home, name='home'),
    path(r'nuevo/', CrearPerfil.as_view(), name='crear_perfil'),
    path(r'listar/', ListarPerfil.as_view(), name='listar_perfiles'),
    path(r'acceso/', listaAcceso, name='lista_acceso'),
    path(r'administrador/<int:id_perfil>', concederAcceso, name='conceder_acceso'),
    path(r'editar/<int:id_perfil>/', editarPerfil, name='editar_perfil'),
    path(r'eliminar/<int:id_perfil>/', eliminarPerfil, name='eliminar_perfil'),
    path(r'administrador/', administrador, name='administrador'),
    path(r'<int:id_usuario>/', proyectos_usuario, name='proyectos_usuario'),
    path(r'<int:id_usuario>/bitacora/', bitacora, name='bitacora'),
]
