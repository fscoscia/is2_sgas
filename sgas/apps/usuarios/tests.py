from django.test import TestCase
import pytest
from django.contrib.auth.models import User
from sgas.apps.usuarios.models import Perfil
from sgas.apps.usuarios.forms import PerfilForm, UsuarioForm

@pytest.mark.django_db
def test_crearUsuario():
    #Verifica la creación de un usuario
    data = {
        'firs_name': 'Fabrizio',
        'last_name': 'Coscia',
        'email': 'fabricoscia@fpuna.edu.py',
    }
    form = UsuarioForm(data=data)
    assert form.is_valid() is True, form.errors


@pytest.mark.django_db
def test_eliminarUsuario():
    #Verifica la eliminación de un usuario
    data = {
        'firs_name': 'Fabrizio',
        'last_name': 'Coscia',
        'email': 'fabricoscia@fpuna.edu.py',
    }
    form = UsuarioForm(data=data)
    form.save()
    usuario = User.objects.get(email='fabricoscia@fpuna.edu.py')
    usuario.delete()
    assert not User.objects.filter(email='fabricoscia@fpuna.edu.py').exists()


@pytest.mark.django_db
def test_crearPerfil():
    #Verifica la creación de un perfil de usuario
    data = {
        'firs_name': 'Fabrizio',
        'last_name': 'Coscia',
        'email': 'fabricoscia@fpuna.edu.py',
    }
    form = UsuarioForm(data=data)
    form.save()
    usuario = User.objects.get(email='fabricoscia@fpuna.edu.py')
    data ={
        'user': usuario,
        'ci': 5173025,
        'telefono': '0984289180',
    }
    perfilForm = PerfilForm(data=data)
    assert perfilForm.is_valid() is True, perfilForm.errors

@pytest.mark.django_db
def test_crearPerfil2():
    #Verifica que no se pueden crear dos perfiles con el mismo ci
    data = {
        'firs_name': 'Fabrizio',
        'last_name': 'Coscia',
        'email': 'fabricoscia@fpuna.edu.py',
    }
    form = UsuarioForm(data=data)
    form.save()
    usuario = User.objects.get(email='fabricoscia@fpuna.edu.py')
    data ={
        'user': usuario,
        'ci': 5173025,
        'telefono': '0984289180',
    }
    perfilForm = PerfilForm(data=data)
    if perfilForm.is_valid():
        perfilForm.save(ci=5173025, usuario=usuario, telefono='0984289180')
    perfilForm = PerfilForm(data=data)
    if perfilForm.is_valid():
        perfilForm.save(ci=5173025, usuario=usuario, telefono='0984289180')
    assert perfilForm.is_valid() is True, perfilForm.errors


@pytest.mark.django_db
def test_eliminarPerfil():
    #Verifica la eliminación de un perfil de usuario
    data = {
        'firs_name': 'Fabrizio',
        'last_name': 'Coscia',
        'email': 'fabricoscia@fpuna.edu.py',
    }
    form = UsuarioForm(data=data)
    form.save()
    usuario = User.objects.get(email='fabricoscia@fpuna.edu.py')
    data ={
        'user': usuario,
        'ci': 5173025,
        'telefono': '0984289180',
    }
    perfilForm = PerfilForm(data=data)
    if perfilForm.is_valid():
        perfilForm.save(ci=5173025, usuario=usuario, telefono='0984289180')
    perfil = Perfil.objects.get(ci=5173025)
    perfil.delete()
    assert not Perfil.objects.filter(ci=5173025).exists()


@pytest.mark.django_db
def test_view_UsuariosHome(client):
   response = client.get('http://127.0.0.1:8000/usuarios/home/')
   assert response.status_code == 200
