from django.contrib.auth.models import User, Permission
from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView
from sgas.apps.usuarios.models import Perfil
from sgas.apps.usuarios.forms import PerfilForm, UsuarioForm
from sgas.apps.proyectos.models import Proyecto
from sgas.apps.miembros.models import Miembro
from sgas.apps.roles.models import Rol
from django.contrib.auth.decorators import login_required, permission_required
from django.core.mail import send_mail
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

def home(request):
    # Enlace a pagina inicial 127.0.0.1/8000
    return render(request, 'home.html')

# === Crear Perfil ===
class CrearPerfil(LoginRequiredMixin, CreateView):
    """
    Clase para crear un perfil de usuario
    Hereda de la clase genérica CreateView
    Requiere inicio de sesión
    """
    model = Perfil
    form_class = PerfilForm
    template_name = 'usuarios/usuario_form.html'
    success_url = reverse_lazy('usuarios:home')

    def form_valid(self, form):
        user = User.objects.get(username=self.request.user)
        Perfil.objects.create(ci=self.request.POST['ci'], telefono=self.request.POST['telefono'], user=user)
        return redirect(self.success_url)


# === Listar Perfiles ===
class ListarPerfil(LoginRequiredMixin, ListView):
    """
    Clase que se utiliza para listar los perfiles de usuario existentes
    Hereda de la clase genérica ListView
    Requiere inicio de sesión
    """
    redirect_field_name = 'redirect_to'
    model = Perfil
    template_name = 'usuarios/listar_perfiles.html'


# === Editar Perfil ===
@login_required
def editarPerfil(request, id_perfil):
    """
    Vista para editar la información de un usuario (el email no puede ser modificado)
    Recibe el request HTTP y el id del perfil de usuario
    Retorna la renderización de la información del usuario en el template especificado
    Requiere inicio de sesión
    """
    perfil = Perfil.objects.get(id=id_perfil)
    usuario = User.objects.get(id=perfil.user.id)
    if request.method == 'GET':
        formP = PerfilForm(instance=perfil)
        formU = UsuarioForm(instance=usuario)
    else:
        formP = PerfilForm(request.POST, instance=perfil)
        formU = UsuarioForm(request.POST, instance=usuario)
        if all([formP.is_valid(), formU.is_valid()]):
            formP.save()
            formU.save()
        return redirect('usuarios:listar_perfiles')
    return render(request, 'usuarios/editar_perfil.html', {'formP': formP, 'formU': formU})


# === Listar Solicitudes de Acceso ===
@login_required
@permission_required('usuarios.autorizar_usuario', login_url='usuarios:home')
def listaAcceso(request):
    """
    Lista las solicitudes de acceso al sistema
    Recibe el request HTTP
    Retorna la renderización de la información solicitada en el template especificado
    Requiere una sesión iniciada y permisos de administrador
    """
    perm = Permission.objects.get(codename='acceso_usuario')
    usuario = User.objects.filter(~Q(user_permissions=perm))
    perfiles = Perfil.objects.filter(Q(user=usuario[0]))
    for x in range(1, len(usuario)):
        perfiles |= Perfil.objects.filter(Q(user=usuario[x]))
    contexto = {'perfiles': perfiles}
    return render(request, 'usuarios/usuario_acceso.html', contexto)


# === Conceder Acceso al Sistema ===
@login_required
@permission_required('usuarios.autorizar_usuario', login_url='usuarios:home')
def concederAcceso(request, id_perfil):
    """
    Da permiso de acceso al sistema y envía un email notificando de lo sucedido al usuario
    Recibe el request HTTP y el id del perfil de usuario
    El retorno el una redirección a la página de administración
    Requiere una sesión iniciada y permisos de administrador
    """
    perfil = Perfil.objects.get(id=id_perfil)
    usuario = User.objects.get(id=perfil.user.id)
    perm = Permission.objects.get(codename='acceso_usuario')
    usuario.user_permissions.add(perm)
    send_mail('Acceso a SGAS', 'Su solicitud de acceso al sistema fue aceptada.', 'is2.sgas@gmail.com', [usuario.email],
              fail_silently=False, )
    return redirect('usuarios:administrador')


# === Eliminar Perfil ===
@login_required
def eliminarPerfil(request, id_perfil):
    """
    Elimina el perfil del usuario solicitado
    Recibe el request HTTP y el id del perfil de usuario
    Retorna la renderización en el template especificado, en donde solicita confirmación y luego redirige a la lista de perfiles
    Requiere inicio de sesión
    """
    perfil = Perfil.objects.get(id=id_perfil)
    miembro = Miembro.objects.filter(idPerfil=perfil.id)
    if not miembro:
        if request.method == 'POST':
            usuario = User.objects.get(id=perfil.user.id)
            usuario.delete()
            perfil.delete()
            return redirect('usuarios:listar_perfiles')
        return render(request, 'usuarios/eliminar_perfil.html', {'perfil': perfil})
    else:
        messages.add_message(request, messages.ERROR, 'No se puede eliminar al usuario: %s  porque forma parte de un proyecto' % perfil.user.first_name)
        return redirect('usuarios:listar_perfiles')


# === Administrador ===
@login_required
@permission_required('usuarios.autorizar_usuario', login_url='usuarios:home')
def administrador(request):
    """
    Obtiene información pertinente acerca de los proyectos existentes y las solicitudes de acceso al sistema
    Recibe el request HTTP
    Retorna la renderización de la información ya mencionada en el template especificado
    Requiere una sesión iniciada y permisos de administrador de sistema
    """
    proyectos = Proyecto.objects.all()
    perm = Permission.objects.get(codename='acceso_usuario')
    usuario = User.objects.filter(~Q(user_permissions=perm), ~Q(id=1))
    perfilesAcceso = Perfil.objects.none()
    if len(usuario) > 0:
        perfilesAcceso = Perfil.objects.filter(Q(user=usuario[0]))
        for x in range(1, len(usuario)):
            perfilesAcceso |= Perfil.objects.filter(Q(user=usuario[x]))
    return render(request, 'usuarios/administrador.html', {'perfilesAcceso': perfilesAcceso, 'proyectos': proyectos})


# === Proyectos de Usuario ===
@login_required
def proyectos_usuario(request, id_usuario):
    """
    Función que permite visualizar los proyectos de un determinado usuario
    Recibe el request HTTP y el id del usuario
    Retorna la renderización de los proyetos asociados en el template detallado
    Requiere tener una sesión iniciada
    """
    usuario = User.objects.get(id=id_usuario)
    perfil = Perfil.objects.get(user=usuario)
    miembro = Miembro.objects.filter(idPerfil=perfil.id)
    return render(request, 'usuarios/proyectos.html', {'miembros': miembro})


#===Bitácora de Usuario===
@login_required
def bitacora(request, id_usuario):
    """
    Permite visualizar información sobre la participación de un usuario en distintos proyectos.
    Recibe la petición http y el id del usuario.
    """
    user = User.objects.get(id=id_usuario)
    perfil = Perfil.objects.get(user=user)
    miembros = Miembro.objects.filter(idPerfil=perfil)
    dictU = {}
    for miembro in miembros:
        proyecto = Proyecto.objects.get(id=miembro.idProyecto.id)
        rolesP = Rol.objects.filter(proyecto=proyecto)
        listaRoles = []
        for rol in rolesP:
            if user.groups.filter(id=rol.grupo.id).exists():
                listaRoles.append(rol)
        dictU[proyecto] = listaRoles
    return render(request, 'usuarios/bitacora.html', {'dict': dictU})
