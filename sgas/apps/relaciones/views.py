import ast

from django.shortcuts import render, redirect
from sgas.apps.proyectos.models import Proyecto, Historial
from sgas.apps.usuarios.models import Perfil
from django.contrib.auth.models import User
from sgas.apps.relaciones.models import Relacion
from sgas.apps.items.models import Item, Version
from sgas.apps.relaciones.forms import RelacionesASForm, RelacionesPHForm

import json

#===Relacion Antecesor-Sucesor===
from sgas.apps.relaciones.utils import bfsAtras, bfsAdelante


def crearRelacionAS(request, idProyecto, idFase, idTipo, idItem):
    """
    Vista basada en funcion para crear relaciones del tipo Antecesor/Sucesor.
    Como parametros recibe una peticion http y el id del proyecto, fase, tipo de item e item en cuestion.
    Al crear la relacion exitosamente vuelve a la vista de listado de items.
    """

    if request.method == 'POST':
        form = RelacionesASForm(request.POST, idProyecto=idProyecto, idFase=idFase, idTipo=idTipo, idItem=idItem)

        if form.is_valid():
            relacion = form.save(commit=False)
            relacion.itemB = Item.objects.get(id=idItem)
            relacion.tipo = 1
            relacion.itemB.version = relacion.itemB.version+1
            if relacion.itemB.version == 2:
                relacion.itemB.identificador = relacion.itemB.identificador + "v" + str(relacion.itemB.version)
            else:
                n = len(relacion.itemB.identificador)
                relacion.itemB.identificador = relacion.itemB.identificador[:n - 1] + str(relacion.itemB.version)
            relacion.itemB.save()
            relacion.save()
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Crear relacion antecesor-sucesor entre {} y {}'.format(relacion.itemA.identificador, relacion.itemB.identificador), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Items")
            ver = Version()
            ver.versionar(idItem)


        return redirect('items:listar_items', idProyecto=idProyecto, idFase=idFase, idTipo=idTipo)

    else:
        form = RelacionesASForm(request.POST or None, idProyecto=idProyecto, idFase=idFase, idTipo=idTipo, idItem=idItem)

    return render(request, 'relaciones/nuevoRelacion.html', {'form':form,
                                                             'idProyecto':idProyecto,
                                                             'idFase':idFase,
                                                             'idTipo':idTipo})

#===Relacion Padre-Hijo===
def crearRelacionPH(request, idProyecto, idFase, idTipo, idItem):
    """
    Vista basada en funcion para crear relaciones del tipo Padre/Hijo.
    Como parametros recibe una peticion http y el id del proyecto, fase, tipo de item e item en cuestion.
    Al crear la relacion exitosamente vuelve a la vista de listado de items.
    """
    if request.method == 'POST':
        form = RelacionesPHForm(request.POST, idProyecto=idProyecto, idFase=idFase, idTipo=idTipo, idItem=idItem)

        if form.is_valid():
            relacion = form.save(commit=False)
            relacion.itemB = Item.objects.get(id=idItem)
            relacion.tipo = 2
            relacion.itemB.version = relacion.itemB.version+1
            if relacion.itemB.version == 2:
                relacion.itemB.identificador = relacion.itemB.identificador + "v" + str(relacion.itemB.version)
            else:
                n = len(relacion.itemB.identificador)
                relacion.itemB.identificador = relacion.itemB.identificador[:n - 1] + str(relacion.itemB.version)
            relacion.itemB.save()
            relacion.save()
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Crear relacion padre-hijo entre {} y {}'.format(relacion.itemA.identificador, relacion.itemB.identificador), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Items")
            ver = Version()
            ver.versionar(idItem)
        else:
            return render(request, 'relaciones/nuevoRelacion.html', {'form':form,
                                                                     'idProyecto':idProyecto,
                                                                     'idFase':idFase,
                                                                     'idTipo':idTipo})

        return redirect('items:listar_items', idProyecto=idProyecto, idFase=idFase, idTipo=idTipo)

    else:
        form = RelacionesPHForm(request.POST or None, idProyecto=idProyecto, idFase=idFase, idTipo=idTipo, idItem=idItem)

    return render(request, 'relaciones/nuevoRelacion.html', {'form':form,
                                                             'idProyecto':idProyecto,
                                                             'idFase':idFase,
                                                             'idTipo':idTipo})

#===Eliminar Relacion===
def eliminarRelacion(request, idProyecto, idFase, idTipo, idItem, idRelacion):
    """
    Vista basada en funcion para eliminar relaciones de cualquier tipo.
    Como parametros recibe una peticion http y el id del proyecto, fase, tipo de item, item y relacion.
    Al eliminar la relacion vuelve a la vista de listado de items.
    """
    relacion = Relacion.objects.get(id=idRelacion)

    if request.method == 'POST':
        nombre1 = relacion.itemA.identificador
        nombre2 = relacion.itemB.identificador
        relacion.itemB.version = relacion.itemB.version + 1
        if relacion.itemB.version == 2:
            relacion.itemB.identificador = relacion.itemB.identificador + "v" + str(relacion.itemB.version)
        else:
            n = len(relacion.itemB.identificador)
            relacion.itemB.identificador = relacion.itemB.identificador[:n - 1] + str(relacion.itemB.version)
        relacion.itemB.save()
        relacion.delete()
        ver = Version()
        ver.versionar(idItem)
        user = User.objects.get(username=request.user)
        perfil = Perfil.objects.get(user=user)
        Historial.objects.create(operacion='Eliminar relacion entre {} y {}'.format(nombre1, nombre2), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Items")
        return redirect('items:listar_items', idProyecto=idProyecto, idFase=idFase, idTipo=idTipo)

    return render(request, 'relaciones/eliminarRelacion.html', {'relacion':relacion,
                                                                'idProyecto':idProyecto,
                                                                'idFase':idFase,
                                                                'idTipo':idTipo})

#===Calculo de Impacto===
def calcularImpacto(request, idProyecto, idFase, idTipo, idItem):
    """
    Vista basada en funciones para observar el impacto que tendria cambiar un item, tanto hacia atras como hacia adelante en el proyecto.
    Recibe como parametros un request GET, los identificadores de Proyecto, Fase, Tipo e Item.
    """
    costoB = 0
    costoF = 0

    # Hacia atras
    visitedB = set()
    relacionesB = set()
    bfsAtras(visitedB, idItem, relacionesB)
    bVisited = list(visitedB)
    for i in bVisited:
        it = Item.objects.get(id=i)
        if it.estado != 'Desactivado':
            costoB = costoB + it.costo

    # Hacia adelante
    visitedF = set()
    relacionesF = set()
    bfsAdelante(visitedF, idItem, relacionesF)
    fVisited = list(visitedF)
    for i in fVisited:
        it = Item.objects.get(id=i)
        if it.estado != 'Desactivado':
            costoF = costoF + it.costo

    it = Item.objects.get(id=idItem)

    if it.estado != 'Desactivado':
        costoB = costoB - it.costo
        costoF = costoF - it.costo
        costoT = costoB + costoF + it.costo
    else:
        costoT = costoB + costoF

    nodoTrazable = visitedB.union(visitedF)
    nodoTlist = list(nodoTrazable)
    relacionesTrazable = relacionesB.union(relacionesF)
    relacionesTlist = list(relacionesTrazable)

    dictT = {}
    itemsT = []
    for id in nodoTlist:
        it = Item.objects.get(id=id)
        itemsT.append({"id":it.id, "faseid":it.fase.id, "fasenombre":it.fase.nombre, "identificador":it.identificador, "costo":it.costo, "estado":it.estado})

    relacionesT = []
    for id in relacionesTlist:
        r = Relacion.objects.get(id=id)
        relacionesT.append({"id":r.id, "itemAid":r.itemA.id, "itemAnombre":r.itemA.identificador, "itemBid":r.itemB.id, "itemBnombre":r.itemB.identificador, "tipo":r.tipo})

    dictT["nodos"] = itemsT
    dictT["vertices"] = relacionesT

    jsonDict = json.dumps(dictT)

    return render(request, 'relaciones/costoImpacto.html', {'costoB':costoB,
                                                            'costoF':costoF,
                                                            'costoT':costoT,
                                                            'idProyecto':idProyecto,
                                                            'idFase':idFase,
                                                            'idTipo':idTipo,
                                                            'jsonDict':jsonDict})

def trazarItem(request, idProyecto, idFase, idTipo, idItem):
    """
    Vista basada en funciones para observar la trazabilidad de un item, tanto vertical como horizontalmente en el proyecto.
    Recibe como parametros un request GET, los identificadores de Proyecto, Fase, Tipo e Item.
    """

    item = Item.objects.get(id=idItem)
    versiones = Version.objects.filter(itemBase=item)

    # Hacia atras
    visitedB = set()
    relacionesB = set()
    bfsAtras(visitedB, idItem, relacionesB)

    # Hacia adelante
    visitedF = set()
    relacionesF = set()
    bfsAdelante(visitedF, idItem, relacionesF)

    nodoTrazable = visitedB.union(visitedF)
    nodoTlist = list(nodoTrazable)
    relacionesTrazable = relacionesB.union(relacionesF)
    relacionesTlist = list(relacionesTrazable)

    dictT = {}
    itemsT = []
    for id in nodoTlist:
        it = Item.objects.get(id=id)
        itemsT.append({"id":it.id, "faseid":it.fase.id, "fasenombre":it.fase.nombre, "identificador":it.identificador, "costo":it.costo, "estado":it.estado})

    relacionesT = []
    for id in relacionesTlist:
        r = Relacion.objects.get(id=id)
        relacionesT.append({"id":r.id, "itemAid":r.itemA.id, "itemAnombre":r.itemA.identificador, "itemBid":r.itemB.id, "itemBnombre":r.itemB.identificador, "tipo":r.tipo})

    dictT["nodos"] = itemsT
    dictT["vertices"] = relacionesT

    jsonDict = json.dumps(dictT)

    return render(request, 'relaciones/trazabilidad.html', {'idProyecto':idProyecto,
                                                            'idFase':idFase,
                                                            'idTipo':idTipo,
                                                            'versiones':versiones,
                                                            'jsonDict':jsonDict})
