from collections import defaultdict
from django import forms
from django.db.models import Q

from sgas.apps.relaciones.models import Relacion
from sgas.apps.relaciones.utils import dfs
from sgas.apps.proyectos.models import Proyecto, Fase
from sgas.apps.tipoitem.models import TipoItem
from sgas.apps.items.models import Item

class RelacionesASForm(forms.ModelForm):

    class Meta:

        model = Relacion

        fields = [
            'itemA',
        ]

        labels = {
            'itemA' : 'Seleccionar antecesor:',
        }

        widgets = {
            'itemA' : forms.Select(attrs={'class':'form-control custom-select'}),
        }

    def __init__(self, *args, **kwargs):
        idProyecto = kwargs.pop('idProyecto')
        idFase = kwargs.pop('idFase')
        idTipo = kwargs.pop('idTipo')
        idItem = kwargs.pop('idItem')
        super(RelacionesASForm, self).__init__(*args, **kwargs)

        proyecto = Proyecto.objects.get(id=idProyecto)
        fase = Fase.objects.get(id=idFase)

        # Desde itemB hasta itemA
        if idFase > 1:
            item = Item.objects.get(id=idItem)
            relaciones = item.itemB.all()
            noIncluir = []
            for r in relaciones:
                noIncluir.append(r.itemA.id)

            fasesProyecto = proyecto.fase_set.all()
            faseAnt = fasesProyecto.get(posicion=fase.posicion-1)

            itemsQuery = faseAnt.item_set.exclude(id__in=noIncluir).exclude(estado='Desactivado')

            self.fields['itemA'].queryset = itemsQuery
        else:
            itemsQuery = Item.objects.filter(id=-1)
            self.fields['itemA'].queryset = itemsQuery


class RelacionesPHForm(forms.ModelForm):

    class Meta:

        model = Relacion

        fields = [
            'itemA',
        ]

        labels = {
            'itemA' : 'Seleccionar padre:',
        }

        widgets = {
            'itemA' : forms.Select(attrs={'class':'form-control custom-select'}),
        }

    def __init__(self, *args, **kwargs):

        # Desde itemB a itemA
        idProyecto = kwargs.pop('idProyecto')
        idFase = kwargs.pop('idFase')
        idTipo = kwargs.pop('idTipo')
        idItem = kwargs.pop('idItem')
        super(RelacionesPHForm, self).__init__(*args, **kwargs)

        self.destino = Item.objects.get(id=idItem)
        self.fase = Fase.objects.get(id=idFase)

        item = Item.objects.get(id=idItem)
        relaciones = item.itemB.all()
        noIncluir = []
        for r in relaciones:
            noIncluir.append(r.itemA.id)

        fase = Fase.objects.get(id=idFase)
        itemsEnFase = fase.item_set.exclude(id=idItem).exclude(estado='Desactivado')

        itemsQuery = itemsEnFase.exclude(id__in = noIncluir)

        self.fields['itemA'].queryset = itemsQuery


    def clean_itemA(self):

        valor = self.cleaned_data['itemA']

        itemsEnFase = self.fase.item_set.all()

        setRelacionesUsadas = set()

        for it in itemsEnFase:
            relacionesAux = it.itemA.all()

            for r in relacionesAux:
                setRelacionesUsadas.add(r.id)

        grafo = defaultdict(list)
        listRelacionesUsadas = list(setRelacionesUsadas)

        for ruId in listRelacionesUsadas:
            aux = Relacion.objects.get(id=ruId)
            grafo[aux.itemA.id].append(aux.itemB.id)

        visitado = set()
        dfs(visitado, grafo, self.destino.id)

        if valor.id in visitado:
            raise forms.ValidationError("Esta relación forma una dependencia cíclica.")
        else:
            return valor
