from django.db import models
from sgas.apps.items.models import Item

class Relacion(models.Model):
    itemA = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="itemA")
    itemB = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="itemB")
    tipo = models.IntegerField()

    def __str__(self):
        return str(self.itemA) + '->' + str(self.itemB)
