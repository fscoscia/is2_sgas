import pytest
from collections import defaultdict

from sgas.apps.tipoitem.models import TipoItem, AdicionalNumero, AdicionalCadena, AdicionalFecha, AdicionalArchivo
from sgas.apps.items.models import Item, CampoNumero, CampoCadena, CampoFecha, CampoArchivo
from sgas.apps.proyectos.models import Proyecto, Fase
from django.contrib.auth.models import User
from sgas.apps.usuarios.models import Perfil
from sgas.apps.relaciones.models import Relacion
from sgas.apps.relaciones.utils import dfs
from sgas.apps.relaciones.forms import RelacionesASForm

# Create your tests here.

@pytest.mark.django_db
def test_CrearRelacionAS():
    usuario = User.objects.create_user('huga', 'huga@fleitas.com', 'hugafleitas')
    perfil = Perfil.objects.create(ci=1155111, telefono=121312, user=usuario)
    proyecto = Proyecto.objects.create(nombre='prueba_relacion', descripcion='pruebarelacion', fechaCreacion='2020-03-20', numFases=6, estado='Iniciado', gerente=perfil)
    fase1 = Fase.objects.create(nombre='fase1', posicion=1, proyecto=proyecto)
    fase2 = Fase.objects.create(nombre='fase2', posicion=2, proyecto=proyecto)
    fase3 = Fase.objects.create(nombre='fase3', posicion=3, proyecto=proyecto)
    tipo1 = TipoItem.objects.create(nombre='tipo1', descripcion='prueba', proyecto=proyecto, fase=fase1)
    tipo2 = TipoItem.objects.create(nombre='tipo2', descripcion='prueba', proyecto=proyecto, fase=fase2)
    item1 = Item.objects.create(fase=fase1, identificador='tipo11', tipo=tipo1, costo=5, descripcion='prueba')
    item2 = Item.objects.create(fase=fase2, identificador='tipo21', tipo=tipo2, costo=5, descripcion='prueba')

    data = {
        'itemA': item1,
        'itemB': item2,
        'tipo': 1
    }
    form = RelacionesASForm(idProyecto=proyecto.id, idFase=fase2.id, idTipo=tipo2.id, idItem=item2.id, data=data)
    assert form.is_valid() is True, form.errors

@pytest.mark.django_db
def test_dfs():
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto = Proyecto.objects.create(nombre='prueba', descripcion='pruebamiembros', fechaCreacion='2020-03-20', numFases=3, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='fase1', posicion=1, proyecto=proyecto)
    tipo1 = TipoItem.objects.create(nombre='tipo1', descripcion='prueba', proyecto=proyecto, fase=fase1)
    item1 = Item.objects.create(fase=fase1, identificador='tipo11', tipo=tipo1, costo=5, descripcion='prueba')
    item2 = Item.objects.create(fase=fase1, identificador='tipo12', tipo=tipo1, costo=5, descripcion='prueba')
    item3 = Item.objects.create(fase=fase1, identificador='tipo13', tipo=tipo1, costo=5, descripcion='prueba')


    Relacion.objects.create(itemA=item1, itemB=item2, tipo=2)
    Relacion.objects.create(itemA=item2, itemB=item3, tipo=2)

    graph = defaultdict(list)
    graph[item1.id].append(item2.id)
    graph[item2.id].append(item3.id)
    visited = set()

    dfs(visited, graph, item1.id)

    assert item3.id in visited, 'No detecta correctamente un ciclo'

@pytest.mark.django_db
def test_EliminarRelacion():
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto = Proyecto.objects.create(nombre='prueba', descripcion='pruebamiembros', fechaCreacion='2020-03-20', numFases=3, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='fase1', posicion=1, proyecto=proyecto)
    fase2 = Fase.objects.create(nombre='fase2', posicion=2, proyecto=proyecto)
    tipo1 = TipoItem.objects.create(nombre='tipo1', descripcion='prueba', proyecto=proyecto, fase=fase1)
    tipo2 = TipoItem.objects.create(nombre='tipo2', descripcion='prueba', proyecto=proyecto, fase=fase2)
    item1 = Item.objects.create(fase=fase1, identificador='tipo11', tipo=tipo1, costo=5, descripcion='prueba')
    item2 = Item.objects.create(fase=fase2, identificador='tipo21', tipo=tipo2, costo=5, descripcion='prueba')

    relacion = Relacion.objects.create(itemA=item1, itemB=item2, tipo=1)
    relacion.delete()

    assert Relacion.objects.count() == 0
