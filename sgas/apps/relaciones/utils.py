from collections import defaultdict

from sgas.apps.items.models import Item
from sgas.apps.relaciones.models import Relacion


def dfs(visited, graph, node):
    if node not in visited:
        visited.add(node)
        for neighbour in graph[node]:
            dfs(visited, graph, neighbour)

def bfsAtras(visited, node, relaciones):
    queue = []
    visited.add(node)
    queue.append(node)

    while queue:
        aux = queue.pop(0)

        item = Item.objects.get(id=aux)
        re = item.itemB.all()

        for r in re:
            padre = r.itemA.id
            relaciones.add(r.id)
            if padre not in visited:
                visited.add(padre)
                queue.append(padre)


def bfsAdelante(visited, node, relaciones):
    queue = []
    visited.add(node)
    queue.append(node)

    while queue:
        aux = queue.pop(0)

        item = Item.objects.get(id=aux)
        re = item.itemA.all()

        for r in re:
            hijo = r.itemB.id
            relaciones.add(r.id)
            if hijo not in visited:
                visited.add(hijo)
                queue.append(hijo)
