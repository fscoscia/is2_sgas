from django.conf.urls import url
from django.urls import path

from sgas.apps.relaciones.views import crearRelacionAS, crearRelacionPH, eliminarRelacion, calcularImpacto, trazarItem

urlpatterns = [

    #url(r'lista/', listaRelaciones, name='lista'),
    url(r'nuevoAS/', crearRelacionAS, name='nuevoAS'),
    url(r'nuevoPH/', crearRelacionPH, name='nuevoPH'),
    url(r'impacto/', calcularImpacto, name='impacto'),
    url(r'trazabilidad/', trazarItem, name='trazabilidad'),
    path('eliminar/<int:idRelacion>/', eliminarRelacion, name='eliminar'),

]
