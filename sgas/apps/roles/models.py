from django.db import models
from django.contrib.auth.models import Group

class Rol(models.Model):
    grupo = models.OneToOneField(Group, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=80)
    proyecto = models.ForeignKey(to='proyectos.Proyecto', on_delete=models.CASCADE, null=True, blank=True)
    fase = models.ManyToManyField(to='proyectos.Fase', blank=True)

    class Meta:

        unique_together = ['nombre', 'proyecto']

        permissions = (("Crear item", "Permite crear items"),
                       ("Modificar item", "Permite modificar atributos de items"),
                       ("Restaurar item", "Permite restaurar items a versiones anteriores"),
                       ("Desactivar item", "Permite desactivar items"),
                       ("Agregar ph", "Permite agregar relaciones padre-hijo"),
                       ("Agregar as", "Permite agregar relaciones antecesor-sucesor"),
                       ("Eliminar relaciones", "Permite eliminar relaciones de los items"),
                       ("Aprobar item", "Permite aprobar items"),
                       ("Desaprobar item", "Permite desaprobar items"),
                       ("Calcular impacto", "Permite calcular el impacto de un item sobre el proyecto"),
                       ("Crear lb", "Permite crear lineas base"),
                       ("Cerrar lb", "Permite cerrar lineas base"),
                       ("Romper lb", "Permite romper lineas base"),)

'''
("Crear proyecto", "Permite la creacion de proyectos"),
("Asignar gerente", "Permite asignar al gerente del proyecto"),
("Desasignar gerente", "Permite desasignar al gerente del proyecto"),
("Crear usuario", "Permite la creacion de usuarios"),
("Modificar usuario", "Permite la modificacion de atributos de usuarios"),
("Eliminar usuario", "Permite la eliminacion de usuarios"),
("Modificar proyecto", "Permite la modificacion de atributos de proyectos"),
("Cancelar proyecto", "Permite cancelar un proyecto"),
("Iniciar proyecto", "Permite iniciar un proyecto"),
("Finalizar proyecto", "Permite finalizar un proyecto"),
("Agregar a comite", "Permite asignar usuarios al comite de cambios"),
("Crear rol", "Permite crear roles"),
("Modificar rol", "Permite modificar atributos de roles"),
("Eliminar rol", "Permite eliminar roles"),
("Asignar rol", "Permite asignar roles a usuarios"),
("Desasignar rol", "Permite desasignar roles a usuarios"),
("Crear fase", "Permite crear fases del proyecto"),
("Finalizar fase", "Permite finalizar fases del proyecto"),
("Crear tipo item", "Permite definir nuevos tipos de item"),
("Importar tipo item", "Permite importar tipos de item de otros proyectos"),
("Eliminar tipo item", "Permite eliminar tipos de item"),
'''
