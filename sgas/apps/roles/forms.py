from django import forms
from sgas.apps.roles.models import Rol
from django.contrib.admin import widgets
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission, Group
from sgas.apps.proyectos.models import Proyecto, Fase


class RolForm(forms.ModelForm):

    permissions = ['Crear item','Modificar item','Restaurar item', 'Desactivar item',
    'Agregar ph', 'Agregar as', 'Eliminar relaciones', 'Aprobar item', 'Desaprobar item',
    'Calcular impacto','Crear lb', 'Romper lb']
    permisos = Permission.objects.filter(codename__in=permissions).values_list('name', 'codename')
    select = forms.MultipleChoiceField(choices=permisos,widget=forms.CheckboxSelectMultiple())

    def __init__(self, *args, **kwargs):
        id = kwargs.pop('idProyecto')
        super(RolForm, self).__init__(*args, **kwargs)
        proyecto = Proyecto.objects.get(id=id)
        self.fields['fase'].queryset = Fase.objects.filter(proyecto=proyecto).order_by('id')
        if kwargs.get('instance'):
            instance = kwargs.get('instance')
            lista = instance.grupo.permissions.all().values_list('name', 'codename')
            self.fields['select'].initial = [x[0] for x in lista]

    class Meta:
        model = Rol
        fields = [
            'nombre',
            'fase'
        ]
        labels = {
            'nombre': 'Nombre',
            'fase': 'Fase'
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'fase': forms.CheckboxSelectMultiple(),
        }



