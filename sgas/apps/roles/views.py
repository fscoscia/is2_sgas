from django.contrib.auth.models import Group, Permission
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView
from django.urls.base import reverse
from sgas.apps.proyectos.models import Proyecto, Historial
from sgas.apps.roles.models import Rol
from sgas.apps.roles.forms import RolForm
from sgas.apps.miembros.models import Miembro
from sgas.apps.usuarios.models import Perfil
from django.contrib.auth.models import User

# ===Vista CrearRol===
class CrearRol(LoginRequiredMixin, CreateView):
    """
    Vista basada en modelos. Permite crear un rol y el grupo asociado con los permisos correspondientes.
    La validacion se redefine para permitir la creacion del grupo y asociar con los permisos correspondientes.
    """
    redirect_field_name = 'redirect_to'
    model = Rol
    form_class = RolForm
    template_name = 'roles/rol_form.html'

    def get_success_url(self):
        return reverse('roles:listar_roles', args=(self.kwargs['idProyecto'],))

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super(CrearRol, self).get_form_kwargs(**kwargs)
        form_kwargs['idProyecto'] = self.kwargs['idProyecto']
        return form_kwargs

    def form_valid(self, form):
        proyecto = Proyecto.objects.get(id=self.kwargs['idProyecto'])
        form.instance.proyecto = proyecto
        nombreRol = form.cleaned_data['nombre']
        nombreGrupo = '{}_{}'.format(nombreRol, proyecto.id)
        grupo = Group.objects.create(name = nombreGrupo)
        form.instance.grupo = grupo
        permisos = Permission.objects.filter(name__in=form.cleaned_data['select'])
        grupo.permissions.set(permisos)
        user = User.objects.get(username=self.request.user)
        perfil = Perfil.objects.get(user=user)
        Historial.objects.create(operacion='Crear Rol {}'.format(form.instance.nombre), autor=perfil.__str__(), proyecto=proyecto, categoria="Miembros")
        return super(CrearRol, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CrearRol, self).get_context_data()
        context['idProyecto'] = self.kwargs['idProyecto']
        return context

# ===Vista ListarRol===
class ListarRol(LoginRequiredMixin, ListView):
    """
    Vista basada en modelos. Permite listar todos los roles creados.
    """
    redirect_field_name = 'redirect_to'
    model = Rol
    template_name = 'roles/listar_roles.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ListarRol, self).get_context_data()
        context['idProyecto'] = self.kwargs['idProyecto']
        return context

    def get_queryset(self):
        return Rol.objects.filter(proyecto=self.kwargs['idProyecto']).order_by('id')

# ===Vista EliminarRol===
@login_required
def EliminarRol(request, idProyecto, id_rol):
    """
    Vista basada en funciones. Permite eliminar un rol seleccionado y su grupo asociado.
    Recibe el request y el id del rol a eliminar.
    """
    rol = Rol.objects.get(id = id_rol)
    if request.method == 'POST':
        grupo = Group.objects.get(id = rol.grupo.id)
        grupo.delete()
        nombre = rol.nombre
        rol.delete()
        user = User.objects.get(username=request.user)
        perfil = Perfil.objects.get(user=user)
        Historial.objects.create(operacion='Eliminar Rol {}'.format(nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Miembros")
        return redirect('roles:listar_roles', idProyecto=idProyecto)
    return render(request, 'roles/eliminar_rol.html', {'rol':rol,
                                                       'idProyecto': idProyecto})

# ===Vista EditarRol===
@login_required
def EditarRol(request, idProyecto, id_rol):
    """
    Vista basada en funciones. Permite editar un rol seleccionado y su grupo asociado.
    Recibe el request y el id del rol a editar.
    """
    rol = Rol.objects.get(id=id_rol)
    grupo = Group.objects.get(id=rol.grupo.id)
    if request.method == 'GET':
        form = RolForm(instance=rol, idProyecto=idProyecto)
    else:
        form = RolForm(request.POST, instance=rol, idProyecto=idProyecto)
        if form.is_valid():
            permisos = Permission.objects.filter(name__in=form.cleaned_data['select'])
            grupo.permissions.set(permisos)
            form.save()
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Editar Rol {}'.format(rol.nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Miembros")
        return redirect('roles:listar_roles', idProyecto=idProyecto)
    return render(request, 'roles/editar_rol.html', {'form': form,
                                                     'idProyecto': idProyecto})

@login_required
def asignarRol(request, idProyecto, idMiembro, idRol):

    miembro = Miembro.objects.get(id=idMiembro)
    user = miembro.idPerfil.user
    rol = Rol.objects.get(id=idRol)
    rol.grupo.user_set.add(user)
    perfil = Perfil.objects.get(user=user)
    nombre = perfil.__str__()
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    Historial.objects.create(operacion='Asignar Rol {} a {}'.format(rol.nombre, nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Miembros")
    return redirect('miembros:ver_roles', idProyecto=idProyecto, idMiembro=idMiembro)

@login_required
def desasignarRol(request, idProyecto, idMiembro, idRol):

    miembro = Miembro.objects.get(id=idMiembro)
    user = miembro.idPerfil.user
    rol = Rol.objects.get(id=idRol)
    rol.grupo.user_set.remove(user)
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    Historial.objects.create(operacion='Desasignar Rol {} a {}'.format(rol.nombre, perfil.__str__()), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Miembros")
    return redirect('miembros:ver_roles', idProyecto=idProyecto, idMiembro=idMiembro)

@login_required
def verRoles(request, idProyecto, idMiembro):
    """
    Vista basada en modelos. Permite listar todos los roles creados.
    """
    miembro = Miembro.objects.get(id=idMiembro)
    user = miembro.idPerfil.user
    list = []
    for x in user.groups.all():
        list.append(x.name)
    #print(list)
    for x in range(0, len(list)):
        n = list[x].split('_')
        list[x] = n[0]
    roles_asignados = Rol.objects.distinct('nombre').filter(nombre__in=list, proyecto=idProyecto)
    roles_sinasignar = Rol.objects.distinct('nombre').exclude(nombre__in=list).exclude(fase=None).filter(proyecto=idProyecto)
    return render(request, 'miembros/verRoles.html', {'roles_a': roles_asignados, 'roles_sa': roles_sinasignar, 'idProyecto':idProyecto, 'idMiembro':idMiembro})

