import pytest
import datetime

from django.contrib.contenttypes.models import ContentType
from sgas.apps.proyectos.models import Proyecto
from sgas.apps.usuarios.models import User, Perfil
from django.contrib.auth.models import Group, Permission
from sgas.apps.roles.models import Rol
from django.urls import reverse

@pytest.mark.django_db
def test_crearGrupo():
    #Verifica la creación de un grupo
    Group.objects.create(name='Grupo')
    assert Group.objects.count() == 1

@pytest.mark.django_db
def test_eliminarGrupo():
    #Verifica la eliminación de un grupo
    grupo = Group.objects.create(name='Grupo')
    grupo.delete()
    assert Group.objects.count() == 0

@pytest.mark.django_db
def test_eliminarRol():
    #Verifica la eliminación de un rol
    grupo = Group.objects.create(name='Grupo')
    rol = Rol.objects.create(nombre='Rol', grupo=grupo)
    rol.delete()
    assert Rol.objects.count() == 0

@pytest.mark.django_db
def test_eliminarRolError():
    #Verifica que no se puede eliminar un rol que no existe
    grupo = Group.objects.create(name='Grupo')
    rol = Rol.objects.create(nombre='Rol', grupo=grupo)
    try:
        rol.delete()
        rol.delete()
    except:
        assert False, 'No se puede eliminar un rol que no existe'

@pytest.mark.django_db
def test_crearRol():
    #Verifica la creación de un rol
    grupo = Group.objects.create(name='Grupo')
    Rol.objects.create(nombre='Rol', grupo=grupo)
    assert Rol.objects.count() == 1


@pytest.mark.django_db
def test_crearRolError():
    #Verifica que no se puedan crear roles con el mismo nombre
    grupo1 = Group.objects.create(name='Grupo1')
    grupo2 = Group.objects.create(name='Grupo2')
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(), numFases=2, estado='Pendiente', gerente=perfil)
    try:
        Rol.objects.create(nombre='Rol', grupo=grupo1, proyecto=proyecto1)
        Rol.objects.create(nombre='Rol', grupo=grupo2, proyecto=proyecto1)
    except:
        assert False, 'No se permiten roles con el mismo nombre en un mismo proyecto'


# @pytest.mark.django_db
# def test_view_Roles_Listar(client):
#     #Verifica que la vista responda correctamente a un pedido de GET
#     response = client.get('http://127.0.0.1:8000/roles/listar/')
#     assert response.status_code == 200
#
# @pytest.mark.django_db
# def test_view_Crear_Rol(client):
#     #Verifica que la vista responda correctamente a un pedido de POST
#     permisos = Permission.objects.create(name='Permiso',codename='Permiso',content_type=ContentType.objects.get_for_model(Rol))
#     response = client.post('http://127.0.0.1:8000/roles/nuevo/',{'nombre': 'Rol', 'select': (permisos)})
#     assert response.status_code == 200
