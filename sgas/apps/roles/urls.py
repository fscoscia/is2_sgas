from django.urls import path, include
from sgas.apps.roles.views import ListarRol, CrearRol, EliminarRol, EditarRol

urlpatterns = [
    path(r'listar/', ListarRol.as_view(), name='listar_roles'),
    path(r'nuevo/', CrearRol.as_view(), name='crear_rol'),
    path(r'eliminar/<int:id_rol>/', EliminarRol, name='eliminar_rol'),
    path(r'editar/<int:id_rol>/', EditarRol, name='editar_rol'),
]