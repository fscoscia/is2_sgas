from django.shortcuts import render, redirect
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages import get_messages
from django.forms.models import modelformset_factory
from sgas.apps.proyectos.models import Proyecto, Fase, Historial, LineaBase
from sgas.apps.roles.models import Rol
from sgas.apps.usuarios.models import Perfil
from sgas.apps.items.models import Item, CampoNumero, CampoCadena, CampoFecha, CampoArchivo
from sgas.apps.items.models import Item, CampoNumero, CampoCadena, CampoFecha, CampoArchivo, Version
from sgas.apps.items.forms import ItemForm, BaseCampoNumeroForm, BaseCampoCadenaForm, BaseCampoFechaForm, BaseCampoArchivoForm, mensajeCorreo
from sgas.apps.tipoitem.models import TipoItem, AdicionalNumero, AdicionalFecha, AdicionalCadena, AdicionalArchivo
from sgas.apps.relaciones.models import Relacion
from django.core.mail import send_mail
from django.contrib.auth.models import User, Group
from collections import defaultdict
from sgas.apps.relaciones.utils import dfs
from django.contrib import messages
from datetime import datetime
import ast

# ===Crear Ítem===
@login_required
def CrearItem(request, idProyecto, idFase, idTipo):
    """
    Vista basada en funciones. Permite crear un item con todos los atributos adicionales que posee el tipo de item.
    Recibe el id del proyecto y la fase al que va a pertenecer el item y el tipo de item a crear.
    """
    tipo = TipoItem.objects.get(id=idTipo)
    fase = Fase.objects.get(id=idFase)
    i = Item.objects.filter(fase=fase, tipo=tipo).count() + 1
    nombre = "%s_%d" % (tipo.nombre, i)
    nombre = nombre.replace(" ", "")
    queryset1 = AdicionalNumero.objects.filter(tipo=idTipo)
    queryset2 = AdicionalCadena.objects.filter(tipo=idTipo)
    queryset3 = AdicionalFecha.objects.filter(tipo=idTipo)
    queryset4 = AdicionalArchivo.objects.filter(tipo=idTipo)
    CampoNumeroFormset = modelformset_factory(
        CampoNumero,
        fields=('nombre', 'opcional', 'numero'),
        form=BaseCampoNumeroForm,
        extra=len(queryset1)
    )

    CampoCadenaFormset = modelformset_factory(
        CampoCadena,
        fields=('nombre', 'opcional', 'cadena'),
        form=BaseCampoCadenaForm,
        extra=len(queryset2)
    )

    CampoFechaFormset = modelformset_factory(
        CampoFecha,
        fields=('nombre', 'opcional', 'fecha'),
        form=BaseCampoFechaForm,
        extra=len(queryset3)
    )

    CampoArchivoFormset = modelformset_factory(
        CampoArchivo,
        fields=('nombre', 'opcional', 'archivo'),
        form=BaseCampoArchivoForm,
        extra=len(queryset4)
    )
    template_name = 'items/item_form.html'
    if request.method == 'GET':
        itemform = ItemForm(nombre=nombre)
        dict1 = queryset1.values('nombre', 'opcional')
        dict2 = queryset2.values('nombre', 'opcional')
        dict3 = queryset3.values('nombre', 'opcional')
        dict4 = queryset4.values('nombre', 'opcional')
        formset1 = CampoNumeroFormset(initial=[{'nombre': x['nombre'], 'opcional': x['opcional']} for x in dict1], queryset=CampoNumero.objects.none(), prefix='form1')
        formset2 = CampoCadenaFormset(initial=[{'nombre': x['nombre'], 'opcional': x['opcional']} for x in dict2], queryset=CampoCadena.objects.none(), prefix='form2')
        formset3 = CampoFechaFormset(initial=[{'nombre': x['nombre'], 'opcional': x['opcional']} for x in dict3], queryset=CampoFecha.objects.none(), prefix='form3')
        formset4 = CampoArchivoFormset(initial=[{'nombre': x['nombre'], 'opcional': x['opcional']} for x in dict4], queryset=CampoArchivo.objects.none(), prefix='form4')
    elif request.method == 'POST':
        itemform = ItemForm(request.POST, nombre=nombre)
        formset1 = CampoNumeroFormset(request.POST, prefix='form1')
        formset2 = CampoCadenaFormset(request.POST, prefix='form2')
        formset3 = CampoFechaFormset(request.POST, prefix='form3')
        formset4 = CampoArchivoFormset(request.POST, request.FILES, prefix='form4')
        if itemform.is_valid() and (formset1.is_valid() or not queryset1) and (formset2.is_valid() or not queryset2) and (formset3.is_valid() or not queryset3) and (formset4.is_valid() or not queryset4):
            item = itemform.save(commit=False, nombre=nombre, fase=fase, tipo=tipo)
            numeros = []
            cadenas = []
            fechas = []
            archivos = []
            for x in range(0, len(queryset1)):
                adicional = formset1[x].save(commit=False)
                adicional.item = item
                adicional.nombre = queryset1[x].nombre
                adicional.opcional = queryset1[x].opcional
                numeros.append(adicional)
                if adicional.opcional and not adicional.numero:
                    mensaje = "El atributo {} es obligatorio".format(adicional.nombre)
                    messages.add_message(request, messages.ERROR, mensaje)
            for x in range(0, len(queryset2)):
                adicional = formset2[x].save(commit=False)
                adicional.item = item
                adicional.nombre = queryset2[x].nombre
                adicional.opcional = queryset2[x].opcional
                cadenas.append(adicional)
                if adicional.opcional and not adicional.cadena:
                    mensaje = "El atributo {} es obligatorio".format(adicional.nombre)
                    messages.add_message(request, messages.ERROR, mensaje)
            for x in range(0, len(queryset3)):
                adicional = formset3[x].save(commit=False)
                adicional.item = item
                adicional.nombre = queryset3[x].nombre
                adicional.opcional = queryset3[x].opcional
                fechas.append(adicional)
                if adicional.opcional and not adicional.fecha:
                    mensaje = "El atributo {} es obligatorio".format(adicional.nombre)
                    messages.add_message(request, messages.ERROR, mensaje)
            for x in range(0, len(queryset4)):
                adicional = formset4[x].save(commit=False)
                adicional.item = item
                adicional.nombre = queryset4[x].nombre
                adicional.opcional = queryset4[x].opcional
                archivos.append(adicional)
                if adicional.opcional and not adicional.archivo:
                    mensaje = "El atributo {} es obligatorio".format(adicional.nombre)
                    messages.add_message(request, messages.ERROR, mensaje)
            error = False
            mensajes = get_messages(request)
            for mensaje in mensajes:
                if 'error' in mensaje.tags:
                    error = True
            if not error:
                item.save()
                for x in numeros:
                    x.save()
                for x in cadenas:
                    x.save()
                for x in fechas:
                    x.save()
                for x in archivos:
                    x.save()
                tipo.numItems = tipo.numItems + 1
                tipo.save()
                fase.numItems = fase.numItems + 1
                fase.save()
                user = User.objects.get(username=request.user)
                perfil = Perfil.objects.get(user=user)
                Historial.objects.create(operacion='Crear Item {}'.format(item.identificador), autor=perfil.__str__(), proyecto=item.fase.proyecto, categoria="Items")
                ver = Version()
                ver.versionar(item.id)
                if fase.numItems == 1 and fase.posicion > 1:
                    return redirect('relaciones:nuevoAS', idProyecto= idProyecto, idFase=idFase, idTipo=idTipo, idItem=item.id)
                return redirect('items:listar_items', idProyecto=idProyecto, idFase=idFase, idTipo=idTipo)
            dict1 = queryset1.values('nombre', 'opcional')
            dict2 = queryset2.values('nombre', 'opcional')
            dict3 = queryset3.values('nombre', 'opcional')
            dict4 = queryset4.values('nombre', 'opcional')
            formset1 = CampoNumeroFormset(initial=[{'nombre': x['nombre'], 'opcional': x['opcional']} for x in dict1],
                                          queryset=CampoNumero.objects.none(), prefix='form1')
            formset2 = CampoCadenaFormset(initial=[{'nombre': x['nombre'], 'opcional': x['opcional']} for x in dict2],
                                          queryset=CampoCadena.objects.none(), prefix='form2')
            formset3 = CampoFechaFormset(initial=[{'nombre': x['nombre'], 'opcional': x['opcional']} for x in dict3],
                                         queryset=CampoFecha.objects.none(), prefix='form3')
            formset4 = CampoArchivoFormset(initial=[{'nombre': x['nombre'], 'opcional': x['opcional']} for x in dict4],
                                           queryset=CampoArchivo.objects.none(), prefix='form4')

    return render(request, template_name,
                  {'itemform': itemform,
                   'formset1': formset1,
                   'formset2': formset2,
                   'formset3': formset3,
                   'formset4': formset4,
                   'idProyecto': idProyecto,
                   'idFase': idFase,
                   'idTipo': idTipo}
                   )


class ListarItem(LoginRequiredMixin, ListView):

    redirect_field_name = 'redirect_to'
    model = Item
    template_name = 'items/listar_item.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ListarItem, self).get_context_data()
        context['idProyecto'] = self.kwargs['idProyecto']
        context['idFase'] = self.kwargs['idFase']
        context['idTipo'] = self.kwargs['idTipo']
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['idProyecto'])
        context['faseActual'] = Fase.objects.get(id=self.kwargs['idFase'])
        if context['faseActual'].posicion == 1:
            context['faseAnt'] = None
        else:
            context['faseAnt'] = Fase.objects.get(id= (self.kwargs['idFase'] - 1))
        return context

    def get_queryset(self):
        return Item.objects.filter(fase=self.kwargs['idFase'], tipo=self.kwargs['idTipo']).order_by('id')


# ===Interfaz de Ítem===
def verItem(request, idProyecto, idFase, idTipo, idItem):
    """
    Función para visualizar toda la información y las acciones que pueden realizarse
    sobre un ítem.
    """
    fase = Fase.objects.get(id=idFase)
    if request.method == 'GET':
        item = Item.objects.get(id=idItem)
        relacionA = item.itemA.all()
        relacionB = item.itemB.all()
        return render(request, 'items/ver_item.html', {'item': item,
                                                       'idProyecto': idProyecto,
                                                       'idFase': idFase,
                                                       'idTipo': idTipo,
                                                       'idItem': idItem,
                                                       'relacionA':relacionA,
                                                       'relacionB':relacionB,
                                                       'fase':fase})


# ===Solicitar Aprobación para Ítem===
def preAprobar(request, idProyecto, idFase, idTipo, idItem):
    """
    Cambia el estado de un ítem de En Desarrollo a Para Aprobar.
    Recibe la petición http, el id del proyecto, el id de la fase, el id del tipo de ítem
    y el id del ítem.
    Redirige a la interfaz de ítem.
    """
    item = Item.objects.get(id=idItem)
    item.estado = 'Para aprobar'
    item.save()
    return redirect('items:ver_item', idProyecto, idFase, idTipo, idItem)


# ===Aprobar Ítem===
def aprobarItem(request, idProyecto, idFase, idTipo, idItem):
    """
    Cambia el estado de un ítem de Para Aprobar a Aprobado.
    Recibe la petición http, el id del proyecto, el id de la fase, el id del tipo de ítem
    y el id del ítem.
    Redirige a la interfaz de ítem.

    ACTUALIZACIÓN ITERACIÓN 4:

    Se añade una comprobación de manera a asegurar la consistencia del flujo del proyecto.
    No permite aprobación de ítems que no estén relacionados de forma directa o indirecta con la fase anterior,
    a excepción de la fase 1.
    Dicho proceso se realiza cargando los ítems de la fase en un grafo dirigido y luego se utiliza dfs.
    """
    fase = Fase.objects.get(id = idFase)
    item = Item.objects.get(id=idItem)
    if fase.posicion == 1:
        item.estado = 'Aprobado'
        item.save()
        user = User.objects.get(username=request.user)
        perfil = Perfil.objects.get(user=user)
        Historial.objects.create(operacion='Aprobar Item {}'.format(item.identificador), autor=perfil.__str__(), proyecto=item.fase.proyecto, categoria="Items")
    else:
        aprobar = False
        itemsEnFase = fase.item_set.all()
        setRelacionesUsadas = set()
        for it in itemsEnFase:
            relacionesAux = it.itemA.all()
            for r in relacionesAux:
                setRelacionesUsadas.add(r.id)
        grafo = defaultdict(list)
        listRelacionesUsadas = list(setRelacionesUsadas)
        for ruId in listRelacionesUsadas:
            aux = Relacion.objects.get(id=ruId)
            grafo[aux.itemB.id].append(aux.itemA.id)

        visitado = set()
        dfs(visitado, grafo, item.id)
        sucesores = []
        for i in itemsEnFase:
            rs = Relacion.objects.filter(tipo=1, itemB=i)
            if rs:
                sucesores.append(i.id)
        for s in sucesores:
            if s in visitado:
                aprobar = True
                break
        if aprobar:
            item.estado = 'Aprobado'
            item.save()
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Aprobar Item {}'.format(item.identificador), autor=perfil.__str__(), proyecto=item.fase.proyecto, categoria="Items")
        else:
            messages.add_message(request, messages.ERROR, 'No se puede aprobar el ítem seleccionado porque no es trazable a la fase anterior, verifique las relaciones ')
    return redirect('items:ver_item', idProyecto, idFase, idTipo, idItem)


# ===Desaprobar Ítem===
def desaprobarItem(request, idProyecto, idFase, idTipo, idItem):
    """
    Cambia el estado del ítem a "En Desarrollo"
    Recibe la petición http, el id del proyecto, el id de la fase, el id del tipo de ítem
    y el id del ítem.
    Redirige a la interfaz de ítem.
    """
    item = Item.objects.get(id=idItem)
    proyecto = Proyecto.objects.get(id=idProyecto)
    fase = Fase.objects.get(id=idFase)
    roles = Rol.objects.filter(fase=idFase)
    miembros= User.objects.none()
    for x in range(0, len(roles)):
        grupo = Group.objects.get(id=roles[x].grupo.id)
        miembros |= grupo.user_set.all()
    usuario = request.user
    miembros = miembros.exclude(id=usuario.id)
    if request.method == 'POST':
        mensajeForm = mensajeCorreo(request.POST)
        if mensajeForm.is_valid():
            mensaje = mensajeForm.cleaned_data.get('mensaje')
            for x in range(0, len(miembros)):
                send_mail('Desaprobación de Ítem', "El item '{0}' de la fase '{1}', del proyecto '{2}' fue desaprobado por el siguiente motivo '{3}'.".format(item.identificador,fase.nombre, proyecto.nombre ,mensaje),
                    'is2.sgas@gmail.com', [miembros[x].email], fail_silently=False)
            item.estado = 'En Desarrollo'
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Desaprobar Item {}'.format(item.identificador), autor=perfil.__str__(), proyecto=item.fase.proyecto, categoria="Items")
            item.save()
            return redirect('items:listar_items', idProyecto=idProyecto, idFase=idFase, idTipo=idTipo)
    else:
        mensajeForm = mensajeCorreo()
        return render(request, 'items/desaprobar_item.html', {'mensaje':mensajeForm, 'item': item, 'idProyecto': idProyecto, 'idFase': idFase, 'idTipo': idTipo})


#===Modificar Item===
def ModificarItem(request, idProyecto, idFase, idTipo, idItem):
    """
    Vista basada en funciones para actualizar los campos de un ítem existente.
    Recibe como parámetros los identificadores del Proyecto, Fase, Tipo e Ítem correspondientes.
    Guarda la nueva información y suma 1 (uno) a la versión.
    Se agrega la version al identificador del Ítem cuando es 2 o mayor.
    Al finalizar redirige a la lista de Ítems del Tipo de Ítem en cuestión.
    """

    item = Item.objects.get(id=idItem)
    tipo = TipoItem.objects.get(id=idTipo)
    fase = Fase.objects.get(id=idFase)
    version = Item.objects.get(id=idItem).version

    queryset1 = CampoNumero.objects.filter(item=idItem)
    queryset2 = CampoCadena.objects.filter(item=idItem)
    queryset3 = CampoFecha.objects.filter(item=idItem)
    queryset4 = CampoArchivo.objects.filter(item=idItem)
    CampoNumeroFormset = modelformset_factory(
        CampoNumero,
        fields=('nombre', 'opcional', 'numero'),
        form=BaseCampoNumeroForm,
        extra=len(queryset1)
    )

    CampoCadenaFormset = modelformset_factory(
        CampoCadena,
        fields=('nombre', 'opcional', 'cadena'),
        form=BaseCampoCadenaForm,
        extra=len(queryset2)
    )

    CampoFechaFormset = modelformset_factory(
        CampoFecha,
        fields=('nombre', 'opcional', 'fecha'),
        form=BaseCampoFechaForm,
        extra=len(queryset3)
    )

    CampoArchivoFormset = modelformset_factory(
        CampoArchivo,
        fields=('nombre', 'opcional', 'archivo'),
        form=BaseCampoArchivoForm,
        extra=len(queryset4)
    )
    template_name = 'items/item_form.html'
    if request.method == 'GET':
        itemform = ItemForm(nombre=item.identificador, instance=item)
        formset1 = CampoNumeroFormset(queryset=queryset1, prefix='form1')
        formset2 = CampoCadenaFormset(queryset=queryset2, prefix='form2')
        formset3 = CampoFechaFormset(queryset=queryset3, prefix='form3')
        formset4 = CampoArchivoFormset(queryset=queryset4, prefix='form4')
        return render(request, template_name,
                  {'itemform': itemform,
                   'formset1': formset1,
                   'formset2': formset2,
                   'formset3': formset3,
                   'formset4': formset4,
                   'idProyecto': idProyecto,
                   'idFase': idFase,
                   'idTipo': idTipo}
                   )
    else:
        itemform = ItemForm(request.POST, nombre=item.identificador, instance=item)
        formset1 = CampoNumeroFormset(request.POST, prefix='form1')
        formset2 = CampoCadenaFormset(request.POST, prefix='form2')
        formset3 = CampoFechaFormset(request.POST, prefix='form3')
        formset4 = CampoArchivoFormset(request.POST, request.FILES, prefix='form4')
        if itemform.is_valid() and (formset1.is_valid() or not queryset1) and (formset2.is_valid() or not queryset2) and (formset3.is_valid() or not queryset3) and (formset4.is_valid() or not queryset4):
            item = itemform.update(nombre=item.identificador, fase=fase, tipo=tipo, version=version)
            for x in range(0, len(queryset1)):
                adicional = formset1[x].save(commit=False)
                adicional.item = item
                adicional.nombre = queryset1[x].nombre
                adicional.opcional = queryset1[x].opcional
                adicional.save()
            for x in range(0, len(queryset2)):
                adicional = formset2[x].save(commit=False)
                adicional.item = item
                adicional.nombre = queryset2[x].nombre
                adicional.opcional = queryset2[x].opcional
                adicional.save()
            for x in range(0, len(queryset3)):
                adicional = formset3[x].save(commit=False)
                adicional.item = item
                adicional.nombre = queryset3[x].nombre
                adicional.opcional = queryset3[x].opcional
                adicional.save()
            for x in range(0, len(queryset4)):
                adicional = formset4[x].save(commit=False)
                adicional.item = item
                adicional.nombre = queryset4[x].nombre
                adicional.opcional = queryset4[x].opcional
                adicional.save()
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Modificar Item {}'.format(item.identificador), autor=perfil.__str__(), proyecto=item.fase.proyecto, categoria="Items")
            ver = Version()
            ver.versionar(idItem)
            return redirect('items:listar_items', idProyecto=idProyecto, idFase=idFase, idTipo=idTipo)


#===Eliminar Item===
def EliminarItem(request, idProyecto, idFase, idTipo, idItem):
    """
    Vista para eliminar (sólo de manera logica) un ítem existente.
    Recibe como parametros por url los identificadores del Proyecto, Fase, Tipo e Ítem correspondientes.
    Pasa el estado del Item a "Desactivado" y luego redirige a la lista de Ítems del Tipo de Ítem en cuestión.
    """

    item = Item.objects.get(id=idItem)
    item.estado = 'Desactivado'
    item.save()
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    Historial.objects.create(operacion='Eliminar Item {}'.format(item.identificador), autor=perfil.__str__(), proyecto=item.fase.proyecto, categoria="Items")

    return redirect('items:listar_items', idProyecto=idProyecto, idFase=idFase, idTipo=idTipo)

#===Reversionar Item===
def ReversionarItem(request, idProyecto, idFase, idTipo, idItem, idVersion):
    """
    Vista para copiar todos los datos de una version anterior a la version mas reciente del item.
    Recibe como parametros el request POST, los identificadores de Proyecto, Fase, Tipo de Item, Item y Version.
    Al finalizar, vuelve a guardar la version mas reciente del item.
    """
    ver = Version.objects.get(id=idVersion)

    if request.method == 'POST':
        it = Item.objects.get(id=idItem)


        n = len(it.identificador)
        it.identificador = it.identificador[:n - 1] + str(it.version + 1)
        it.estado = 'En Desarrollo'
        it.version = it.version + 1
        it.descripcion = ver.descripcion
        it.costo = ver.costo
        it.estado = "En Desarrollo"

        dictN = ast.literal_eval(ver.ccNumero)
        dictC = ast.literal_eval(ver.ccCadena)
        dictF = ast.literal_eval(ver.ccFecha)
        dictA = ast.literal_eval(ver.ccArchivo)
        dictR = ast.literal_eval(ver.ccRelaciones)

        queryset1 = CampoNumero.objects.filter(item=idItem)
        queryset2 = CampoCadena.objects.filter(item=idItem)
        queryset3 = CampoFecha.objects.filter(item=idItem)
        queryset4 = CampoArchivo.objects.filter(item=idItem)

        for x in range(len(queryset1)):
            boolean = False
            queryset1[x].nombre = dictN[str(x)]['nombre']
            queryset1[x].numero = float(dictN[str(x)]['numero'])
            if dictN[str(x)]['opcional'] == 'False' :
                boolean = False
            else:
                boolean = True
            queryset1[x].opcional = boolean
            queryset1[x].save()

        for x in range(len(queryset2)):
            boolean = False
            queryset2[x].nombre = dictC[str(x)]['nombre']
            queryset2[x].cadena = dictC[str(x)]['cadena']
            if dictC[str(x)]['opcional'] == 'False' :
                boolean = False
            else:
                boolean = True
            queryset2[x].opcional = boolean
            queryset2[x].save()

        for x in range(len(queryset3)):
            boolean = False
            queryset3[x].nombre = dictF[str(x)]['nombre']
            queryset3[x].fecha = datetime.strptime(dictF[str(x)]['fecha'], '%Y-%m-%d').date()
            if dictF[str(x)]['opcional'] == 'False' :
                boolean = False
            else:
                boolean = True
            queryset3[x].opcional = boolean
            queryset3[x].save()

        for x in range(len(queryset4)):
            boolean = False
            queryset4[x].nombre = dictA[str(x)]['nombre']
            queryset4[x].archivo = dictA[str(x)]['archivo']
            if dictA[str(x)]['opcional'] == 'False' :
                boolean = False
            else:
                boolean = True
            queryset4[x].opcional = boolean
            queryset4[x].save()

        it.save()
        nuevaVersion = Version()
        nuevaVersion.versionar(it.id)
        return redirect('items:listar_items', idProyecto=idProyecto, idFase=idFase, idTipo=idTipo)

    return render(request, 'items/restaurar_version.html', {'ver':ver,
                                                            'idProyecto':idProyecto,
                                                            'idFase':idFase,
                                                            'idTipo':idTipo,
                                                            'idItem':idItem,
                                                            'idVersion':idVersion})

#===Ver Versiones===
def verVersiones(request, idProyecto, idFase, idTipo, idItem):
    """
    Vista basada en funcion para listar todas las versiones anteriores a un item en particular.
    Recibe como parametros un request GET, los identificadores de Proyecto, Fase, Tipo de Item e Item.
    """
    it = Item.objects.get(id=idItem)
    versionesDeItem = Version.objects.filter(itemBase=it)

    return render(request, 'items/ver_versiones.html',  {'versiones':versionesDeItem,
                                                        'cantidad':versionesDeItem.count()>0,
                                                        'idProyecto':idProyecto,
                                                        'idFase':idFase,
                                                        'idTipo':idTipo})

#===Ver Versiones (Mas Detalles)===
def versionesDetalles(request, idProyecto, idFase, idTipo, idItem, idVersion):
    """
    Vista basada en funcion para ver los atributos adicionales que posee una version anterior en particular de un item.
    Recibe como parametros un request GET, los identificadores de Proyecto, Fase, Tipo de Item, Item y Version
    """
    ver = Version.objects.get(id=idVersion)
    item = Item.objects.get(id=idItem)

    dictN = ast.literal_eval(ver.ccNumero)
    dictC = ast.literal_eval(ver.ccCadena)
    dictF = ast.literal_eval(ver.ccFecha)
    dictA = ast.literal_eval(ver.ccArchivo)
    dictR = ast.literal_eval(ver.ccRelaciones)

    return render(request, 'items/versiones_detalles.html', {'version':ver,
                                                             'dictN':dictN,
                                                             'dictC':dictC,
                                                             'dictF':dictF,
                                                             'dictA':dictA,
                                                             'dictR':dictR,
                                                             'idProyecto':idProyecto,
                                                             'idFase':idFase,
                                                             'idTipo':idTipo,
                                                             'idItem':idItem,
                                                             'item':item})
