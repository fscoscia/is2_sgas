from django import forms
from django.forms import modelformset_factory
from sgas.apps.items.models import Item, CampoNumero, CampoCadena, CampoFecha, CampoArchivo
from sgas.apps.tipoitem.models import TipoItem

class ItemForm(forms.ModelForm):

    identif = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control", "readonly": "readonly"}))

    def __init__(self, *args, **kwargs):
        nombre = kwargs.pop('nombre')
        super(ItemForm, self).__init__(*args, **kwargs)
        self.fields['identif'].initial = nombre
        self.fields['costo'].widget.attrs['min'] = 1

    def save(self, commit=True, *args, **kwargs):
        fase = kwargs.get('fase')
        nombre = kwargs.get('nombre')
        tipo = kwargs.get('tipo')
        instance = super(ItemForm, self).save(commit=False)
        instance.fase = fase
        instance.identificador = nombre
        instance.tipo = tipo
        if commit:
            instance.save()
        return instance

    #def update(self, commit=True, *args, **kwargs):
    #    fase = kwargs.get('fase')
    #    nombre = kwargs.get('nombre')
    #    tipo = kwargs.get('tipo')
    #    version = kwargs.get('version')
    #    instance = super(ItemForm, self).save(commit=False)
    #    instance.fase = fase
    #    instance.identificador = nombre + "v" + str(version)
    #    instance.tipo = tipo
    #    instance.version = version
    #    if commit:
    #        instance.save()
    #    return instance

    def update(self, commit=True, *args, **kwargs):
        fase = kwargs.get('fase')
        nombre = kwargs.get('nombre')
        tipo = kwargs.get('tipo')
        version = kwargs.get('version')
        instance = super(ItemForm, self).save(commit=False)
        instance.fase = fase
        if version == 1:
            instance.identificador = nombre + "v" + str(version+1)
        else:
            n = len(nombre)
            instance.identificador = nombre[:n-1] + str(version+1)
        instance.tipo = tipo
        instance.version = version+1
        if commit:
            instance.save()
        return instance

    class Meta:

        model = Item

        fields = [
            'descripcion',
            'costo',
        ]
        labels = {
            'descripcion': 'Descripcion',
            'costo': 'Costo',
        }
        widgets = {
            'descripcion': forms.TextInput(attrs={'class': 'form-control'}),
            'costo': forms.NumberInput(attrs={'initial': 0, 'min': 1, 'max': 10})
        }

class BaseCampoNumeroForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BaseCampoNumeroForm, self).__init__(*args, **kwargs)

    class Meta:

        model = CampoNumero

        fields = [
            'nombre',
            'opcional',
            'numero'
        ]

class BaseCampoCadenaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BaseCampoCadenaForm, self).__init__(*args, **kwargs)
        self.fields['cadena'].required = self.fields['opcional'].initial

    class Meta:
        model = CampoCadena

        fields = [
            'nombre',
            'opcional',
            'cadena'
        ]


class BaseCampoFechaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BaseCampoFechaForm, self).__init__(*args, **kwargs)
        self.fields['fecha'].required = self.fields['opcional'].initial

    class Meta:
        model = CampoFecha

        fields = [
            'nombre',
            'opcional',
            'fecha'
        ]
        widgets={
            'fecha': forms.DateInput(attrs={'class':'form-control col-4','type':'date'})
        }
class BaseCampoArchivoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BaseCampoArchivoForm, self).__init__(*args, **kwargs)
        self.fields['archivo'].required = self.fields['opcional'].initial

    class Meta:
        model = CampoArchivo

        fields = [
            'nombre',
            'opcional',
            'archivo'
        ]
        widgets = {
            'archivo': forms.FileInput(attrs={'class': 'custom-file'})
        }

class mensajeCorreo(forms.Form):
    mensaje = forms.CharField(widget=forms.Textarea(attrs={"class": "form-control", "rows":"5"}))
