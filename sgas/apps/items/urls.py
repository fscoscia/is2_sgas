from django.urls import path
from sgas.apps.items.views import CrearItem, ListarItem, ModificarItem, EliminarItem, verItem\
    , preAprobar, aprobarItem, desaprobarItem, verVersiones, versionesDetalles, ReversionarItem

urlpatterns = [
    path(r'nuevo/', CrearItem, name='crear_item'),
    path(r'listar/', ListarItem.as_view(), name='listar_items'),
    path(r'modificar/<int:idItem>/', ModificarItem, name='modificar_item'),
    path(r'eliminar/<int:idItem>/', EliminarItem, name='eliminar_item'),
    path(r'<int:idItem>/', verItem, name='ver_item'),
    path(r'<int:idItem>/versiones/', verVersiones, name='ver_versiones'),
    path(r'<int:idItem>/versiones/<int:idVersion>/', versionesDetalles, name='versiones_detalles'),
    path(r'<int:idItem>/versiones/<int:idVersion>/eliminar', ReversionarItem, name='restaurar_version'),
    path(r'preAprobar/<int:idItem>/', preAprobar, name='solicitar_aprobacion'),
    path(r'aprobar/<int:idItem>/', aprobarItem, name='aprobar_item'),
    path(r'desaprobar/<int:idItem>/', desaprobarItem, name='desaprobar_item'),
]
