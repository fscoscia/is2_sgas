from django import template
from django.contrib.auth.models import User, Group, Permission
from sgas.apps.roles.models import Rol
from sgas.apps.proyectos.models import Fase

register = template.Library()

@register.filter
def crear_item(user, idFase):
    fase = Fase.objects.get(id=idFase)
    rol = Rol.objects.filter(fase=fase)
    perm = Permission.objects.get(codename='Crear item')
    if len(rol) > 0:
        for x in range(0, len(rol)):
            if user.groups.filter(id=rol[x].grupo.id).exists() and \
                    perm in rol[x].grupo.permissions.all():
                    return True
    return False


@register.filter
def modificar_item(user, idFase):
    fase = Fase.objects.get(id=idFase)
    rol = Rol.objects.filter(fase=fase)
    perm = Permission.objects.get(codename='Modificar item')
    if len(rol) > 0:
        for x in range(0, len(rol)):
            if user.groups.filter(id=rol[x].grupo.id).exists() and \
                    perm in rol[x].grupo.permissions.all():
                    return True
    return False


@register.filter
def aprobar_item(user, idFase):
    fase = Fase.objects.get(id=idFase)
    rol = Rol.objects.filter(fase=fase)
    perm = Permission.objects.get(codename='Aprobar item')
    if len(rol) > 0:
        for x in range(0, len(rol)):
            if user.groups.filter(id=rol[x].grupo.id).exists() and \
                    perm in rol[x].grupo.permissions.all():
                    return True
    return False


@register.filter
def desaprobar_item(user, idFase):
    fase = Fase.objects.get(id=idFase)
    rol = Rol.objects.filter(fase=fase)
    perm = Permission.objects.get(codename='Desaprobar item')
    if len(rol) > 0:
        for x in range(0, len(rol)):
            if user.groups.filter(id=rol[x].grupo.id).exists() and \
                    perm in rol[x].grupo.permissions.all():
                    return True
    return False


@register.filter
def relacionar_AS(user, idFase):
    fase = Fase.objects.get(id=idFase)
    rol = Rol.objects.filter(fase=fase)
    perm = Permission.objects.get(codename='Agregar as')
    if len(rol) > 0:
        for x in range(0, len(rol)):
            if user.groups.filter(id=rol[x].grupo.id).exists() and \
                    perm in rol[x].grupo.permissions.all():
                    return True
    return False


@register.filter
def relacionar_PH(user, idFase):
    fase = Fase.objects.get(id=idFase)
    rol = Rol.objects.filter(fase=fase)
    perm = Permission.objects.get(codename='Agregar ph')
    if len(rol) > 0:
        for x in range(0, len(rol)):
            if user.groups.filter(id=rol[x].grupo.id).exists() and \
                    perm in rol[x].grupo.permissions.all():
                    return True
    return False


@register.filter
def desactivar_item(user, idFase):
    fase = Fase.objects.get(id=idFase)
    rol = Rol.objects.filter(fase=fase)
    perm = Permission.objects.get(codename='Desactivar item')
    if len(rol) > 0:
        for x in range(0, len(rol)):
            if user.groups.filter(id=rol[x].grupo.id).exists() and \
                    perm in rol[x].grupo.permissions.all():
                    return True
    return False


@register.filter
def eliminar_relacion(user, idFase):
    fase = Fase.objects.get(id=idFase)
    rol = Rol.objects.filter(fase=fase)
    perm = Permission.objects.get(codename='Eliminar relaciones')
    if len(rol) > 0:
        for x in range(0, len(rol)):
            if user.groups.filter(id=rol[x].grupo.id).exists() and \
                    perm in rol[x].grupo.permissions.all():
                    return True
    return False
