from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

ESTADO_CHOICES = [
    ("En Desarrollo", "En Desarrollo"),
    ("Aprobado", "Aprobado"),
    ("En LB", "En LB"),
    ("En Revision", "En Revision"),
    ("Desactivado", "Desactivado"),
    ("Para aprobar", "Para aprobar")
]
class Item(models.Model):
    fase = models.ForeignKey(to='proyectos.Fase', on_delete=models.CASCADE, null=True)
    identificador = models.CharField(max_length=80, null=True)
    tipo = models.ForeignKey(to='tipoitem.TipoItem', on_delete=models.CASCADE, null=True)
    version = models.PositiveIntegerField(default=1)
    costo = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    estado = models.CharField(max_length=80, choices=ESTADO_CHOICES, default="En Desarrollo")
    descripcion = models.CharField(max_length=80)

    class Meta:

        unique_together = ['identificador', 'fase']

    def __str__(self):
        return '{}'.format(self.identificador)

class CampoNumero(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=80, null=True, blank=True)
    numero = models.FloatField(null=True, blank=True)
    opcional = models.BooleanField(null=True, blank=True)

class CampoCadena(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=80, null=True, blank=True)
    cadena = models.CharField(max_length=80)
    opcional = models.BooleanField(null=True, blank=True)

class CampoFecha(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=80, null=True, blank=True)
    fecha = models.DateField(null=True, blank=True)
    opcional = models.BooleanField(null=True, blank=True)

class CampoArchivo(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=80, null=True, blank=True)
    archivo =  models.FileField(upload_to='adjuntos/', null=True, blank=True)
    opcional = models.BooleanField(null=True, blank=True)

class Version(models.Model):
    itemBase = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="itemBase")

    fase = models.ForeignKey(to='proyectos.Fase', on_delete=models.CASCADE, null=True)
    identificador = models.CharField(max_length=80, null=True)
    tipo = models.ForeignKey(to='tipoitem.TipoItem', on_delete=models.CASCADE, null=True)
    version = models.PositiveIntegerField(default=1)
    costo = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    estado = models.CharField(max_length=80, choices=ESTADO_CHOICES, default="En Desarrollo")
    descripcion = models.CharField(max_length=80)

    ccNumero = models.TextField()
    ccCadena = models.TextField()
    ccFecha = models.TextField()
    ccArchivo = models.TextField()

    ccRelaciones = models.TextField()

    def versionar(self, itemId):
        it = Item.objects.get(id=itemId)

        # Copia de atributos base del item
        self.itemBase = it
        self.fase = it.fase
        self.identificador = it.identificador
        self.tipo = it.tipo
        self.version = it.version
        self.costo = it.costo
        self.estado = it.estado
        self.descripcion = it.descripcion

        # Copia de atributos adicionales
        numeros = CampoNumero.objects.filter(item=it)
        cadenas = CampoCadena.objects.filter(item=it)
        fechas = CampoFecha.objects.filter(item=it)
        archivos = CampoArchivo.objects.filter(item=it)

        dictN = {}
        dictC = {}
        dictF = {}
        dictA = {}

        for i in range(len(numeros)):
            dictAux = {}
            dictAux['nombre'] = numeros[i].nombre
            dictAux['numero'] = str(numeros[i].numero)
            dictAux['opcional'] = str(numeros[i].opcional)

            dictN[str(i)] = dictAux

        for i in range(len(cadenas)):
            dictAux = {}
            dictAux['nombre'] = cadenas[i].nombre
            dictAux['cadena'] = cadenas[i].cadena
            dictAux['opcional'] = str(cadenas[i].opcional)

            dictC[str(i)] = dictAux

        for i in range(len(fechas)):
            dictAux = {}
            dictAux['nombre'] = fechas[i].nombre
            dictAux['fecha'] = str(fechas[i].fecha)
            dictAux['opcional'] = str(fechas[i].opcional)

            dictF[str(i)] = dictAux

        for i in range(len(archivos)):
            dictAux = {}
            dictAux['nombre'] = archivos[i].nombre
            dictAux['archivo'] = str(archivos[i].archivo)
            dictAux['opcional'] = str(archivos[i].opcional)

            dictA[str(i)] = dictAux

        self.ccNumero = dictN
        self.ccCadena = dictC
        self.ccFecha = dictF
        self.ccArchivo = dictA

        # Copia de relaciones
        relacionesA = it.itemA.all()
        relacionesB = it.itemB.all()

        dictR = {}

        dictRA = {}
        dictRB = {}

        for i in range(len(relacionesA)):
            dictAux = {}
            dictAux['id'] = str(relacionesA[i].id)
            dictAux['tipo'] = str(relacionesA[i].tipo)
            dictAux['itemA'] = str(relacionesA[i].itemA.id)
            dictAux['itemAnombre'] = relacionesA[i].itemA.identificador
            dictAux['itemB'] = str(relacionesA[i].itemB.id)
            dictAux['itemBnombre'] = relacionesA[i].itemB.identificador

            dictRA[str(i)] = dictAux

        for i in range(len(relacionesB)):
            dictAux = {}
            dictAux['id'] = str(relacionesB[i].id)
            dictAux['tipo'] = str(relacionesB[i].tipo)
            dictAux['itemA'] = str(relacionesB[i].itemA.id)
            dictAux['itemAnombre'] = relacionesB[i].itemA.identificador
            dictAux['itemB'] = str(relacionesB[i].itemB.id)
            dictAux['itemBnombre'] = relacionesB[i].itemB.identificador

            dictRB[str(i)] = dictAux

        dictR['0'] = dictRA
        dictR['1'] = dictRB

        self.ccRelaciones = dictR

        self.save()
