import pytest
import datetime

from sgas.apps.proyectos.models import Proyecto, Fase
from sgas.apps.usuarios.models import User, Perfil
from sgas.apps.items.models import Item, CampoNumero, CampoCadena, CampoArchivo, CampoFecha
from sgas.apps.tipoitem.models import TipoItem
from sgas.apps.items.forms import ItemForm

@pytest.mark.django_db
def test_crearItem():
    # Verifica la creación de un item
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(), numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    tipoitem1 = TipoItem.objects.create(nombre='Test', descripcion='Descripcion', proyecto=proyecto1, fase=fase1)
    data = {
        'identif': "%s_1" % (tipoitem1.nombre),
        'descripcion': 'Descripcion',
        'costo': 5,
    }
    form = ItemForm(nombre="%s_1" % (tipoitem1.nombre), data=data)
    assert form.is_valid() is True, form.errors

@pytest.mark.django_db
def test_crearCampoNumero():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    CampoNumero.objects.create(item=item, nombre='Numero1', numero=1, opcional=True)
    assert CampoNumero.objects.count() == 1


@pytest.mark.django_db
def test_crearCampoCadena():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    CampoCadena.objects.create(item=item, nombre='Numero1', cadena=1, opcional=True)
    assert CampoCadena.objects.count() == 1


@pytest.mark.django_db
def test_crearCampoFecha():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    CampoFecha.objects.create(item=item, nombre='Numero1', opcional=True)
    assert CampoFecha.objects.count() == 1


@pytest.mark.django_db
def test_crearCampoArchivo():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    CampoArchivo.objects.create(item=item, nombre='Numero1', opcional=True)
    assert CampoArchivo.objects.count() == 1


@pytest.mark.django_db
def test_eliminarCampoNumero():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    campo = CampoNumero.objects.create(item=item, nombre='Numero1', numero=1, opcional=True)
    campo.delete()
    assert CampoNumero.objects.count() == 0


@pytest.mark.django_db
def test_eliminarCampoCadena():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    campo = CampoCadena.objects.create(item=item, nombre='Numero1', cadena=1, opcional=True)
    campo.delete()
    assert CampoCadena.objects.count() == 0


@pytest.mark.django_db
def test_eliminarCampoFecha():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    campo = CampoFecha.objects.create(item=item, nombre='Numero1', opcional=True)
    campo.delete()
    assert CampoNumero.objects.count() == 0


@pytest.mark.django_db
def test_eliminarCampoArchivo():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    campo = CampoArchivo.objects.create(item=item, nombre='Numero1', opcional=True)
    campo.delete()
    assert CampoNumero.objects.count() == 0


"""
# PARA CORREGIR (FER)

@pytest.mark.django_db
def test_modificarItem():
    #Verifica la modificación de un atributo de un ítem
    Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    assert Item.objects.count() == 1

@pytest.mark.django_db
def test_liminarItem():
    #Verifica la eliminación lógica de un ítem
    Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    item = Item.objects.get(identificador='Item1')
    item.estado = 'Desactivado'
    item.save()
    assert Item.objects.get(identificador='Item1').estado == 'Desactivado'
"""
