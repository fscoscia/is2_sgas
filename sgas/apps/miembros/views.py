from django.shortcuts import render, redirect
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from sgas.apps.usuarios.models import Perfil
from sgas.apps.proyectos.models import Proyecto, Historial
from sgas.apps.miembros.models import Miembro
from sgas.apps.miembros.forms import MiembrosForm
from django.contrib.auth.models import User

# === Crear Miembro ===
@login_required
def miembroCrear(request, idProyecto):
    """
    Vista basada en funcion para crear miembros.
    Recibe un 'request' y la id de un Proyecto como parametros.
    Una vez haya terminado la peticion se retorna a la vista de Lista de Miembros.
    """
    if request.method == 'POST':
        form = MiembrosForm(request.POST, idProyecto=idProyecto)

        if form.is_valid():
            miembro = form.save(commit=False)
            miembro.idProyecto = Proyecto.objects.get(id=idProyecto)
            miembro.save()
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Agregar a {} al proyecto'.format(miembro.idPerfil.__str__()), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Miembros")

        return redirect('miembros:lista', idProyecto=idProyecto)

    else:
        form = MiembrosForm(request.POST or None, idProyecto=idProyecto)

    return render(request, 'miembros/nuevoMiembro.html', {'form': form, 'proyecto':idProyecto})

# === Eliminar Miembro ===
@login_required
def miembroEliminar(request, idProyecto, idMiembro):
    """
    Vista basada en funcion para eliminar miembros.
    Recibe un 'request', el id de un proyecto y el id de un miembro como parametros.
    Una vez haya terminado la peticion se retorna a la vista de Lista de Miembros.
    """
    miembro = Miembro.objects.get(id=idMiembro)
    if request.method == 'POST':
        miembro.delete()
        user = User.objects.get(username=request.user)
        perfil = Perfil.objects.get(user=user)
        Historial.objects.create(operacion='Eliminar a {} del proyecto'.format(miembro.idPerfil.__str__()), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Miembros")
        return redirect('miembros:lista', idProyecto=idProyecto)
    return render(request, 'miembros/eliminarMiembro.html', {'miembros':miembro, 'idProyecto':idProyecto})

# === Listar Miembros ===
@login_required
def verMiembrosEnProyecto(request, idProyecto):
    """
    Vista basada en funcion para listar miembros dentro de un proyecto.
    Recibe un 'request' y el id de un proyecto como parametros.
    """
    proyecto = Proyecto.objects.get(id=idProyecto)
    miembro = proyecto.miembro_set.all()
    perfil = Perfil.objects.all()
    valid_id = []
    for p in perfil:
        if not Miembro.objects.filter(idProyecto=proyecto).filter(idPerfil=p).exists():
            if not p.id == 1:
                valid_id.append(p.id)
    perfiles = Perfil.objects.filter(id__in = valid_id)
    #print(perfiles)
    miembro = Miembro.objects.filter(idProyecto=idProyecto)
    g = proyecto.gerente
    return render(request, 'miembros/verMiembrosEnProyecto.html', {'miembros':miembro, 'gerente':g, 'idProyecto':idProyecto, 'participantes_para_agregar':perfiles.all().count()>0})
