from django.conf.urls import url
from django.urls import path
from sgas.apps.roles.views import verRoles, asignarRol, desasignarRol
from sgas.apps.miembros.views import miembroCrear, verMiembrosEnProyecto, miembroEliminar

urlpatterns = [

    url(r'lista/', verMiembrosEnProyecto, name='lista'),
    url(r'nuevo/', miembroCrear, name='nuevo'),
    path('<int:idMiembro>/eliminar/', miembroEliminar, name='eliminar'),
    path('<int:idMiembro>/roles/', verRoles, name='ver_roles'),
    path('<int:idMiembro>/roles/<int:idRol>/asignar/', asignarRol, name='asignar_rol'),
    path('<int:idMiembro>/roles/<int:idRol>/desasignar/', desasignarRol, name='desasignar_rol'),
]
