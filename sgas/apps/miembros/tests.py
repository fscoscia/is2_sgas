
import pytest
from django.contrib.auth.models import User
from sgas.apps.miembros.models import Miembro
from sgas.apps.usuarios.models import Perfil
from sgas.apps.proyectos.models import Proyecto

# === Test Crear Miembro ===
@pytest.mark.django_db
def testCrearMiembroV():
    # Este test verifica la creacion de un objeto miembro en la Base de Datos
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto = Proyecto.objects.create(nombre='prueba', descripcion='pruebamiembros', fechaCreacion='2020-03-20', numFases=3, estado='Pendiente', gerente=perfil)
    Miembro.objects.create(idPerfil=perfil, idProyecto=proyecto)

    assert Miembro.objects.count() == 1

# === Test Eliminar Miembro ===
@pytest.mark.django_db
def testEliminarMiembroV():
    # Este test verifica el borrado de la instancia de un objeto Miembro
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto = Proyecto.objects.create(nombre='prueba', descripcion='pruebamiembros', fechaCreacion='2020-03-20', numFases=3, estado='Pendiente', gerente=perfil)
    miembro = Miembro.objects.create(idPerfil=perfil, idProyecto=proyecto)
    miembro.delete()

    assert Miembro.objects.count() == 0

# === Test Eliminar Miembro ===
@pytest.mark.django_db
def testEliminarMiembroF():
    # Este test verifica el borrado de la instancia de un objeto Miembro
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto = Proyecto.objects.create(nombre='prueba', descripcion='pruebamiembros', fechaCreacion='2020-03-20', numFases=3, estado='Pendiente', gerente=perfil)
    miembro = Miembro.objects.create(idPerfil=perfil, idProyecto=proyecto)
    try:
        miembro.delete()
        miembro.delete()
    except:
        assert False, 'El objeto que intenta eliminar no existe'
