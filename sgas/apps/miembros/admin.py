from django.contrib import admin

from sgas.apps.miembros.models import Miembro

# Register your models here.
admin.site.register(Miembro)
