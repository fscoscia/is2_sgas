from django.db import models
from sgas.apps.proyectos.models import Proyecto
from sgas.apps.usuarios.models import Perfil

# Create your models here.
class Miembro(models.Model):
    idPerfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    idProyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
