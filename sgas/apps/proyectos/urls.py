from django.urls import path, include
from .views import ProyectoList, ProyectoCreate, ProyectoUpdate, index, createFases, updateFases, proyectoGeneral, fasesProyecto,\
    iniciarProyecto, cancelarProyecto, verFase, finalizarFase,verHistorial, asignarTI, comite, asignarComite, desasignarComite, verificarComite, finalizarProyecto, \
    crearLB, verLB, cerrarLB, crerSolicitud, interfazVotacion, votoAceptar, votoRechazar, verificarCambios, mantenerItem, cambiarItem, solicitudesProyecto, \
    reporteSolicitudes, reportes, reporteLB


urlpatterns = [
    # Enlace al menu principal de Proyectos
    path(r'', index, name='index'),
    path(r'listar/', ProyectoList.as_view(), name='listar_proyectos'),
    path(r'nuevo/', ProyectoCreate.as_view(), name='nuevo_proyecto'),
    path(r'<pk>/modificar/', ProyectoUpdate.as_view(), name='modificar_proyecto'),
    path(r'<int:id_proyecto>/', proyectoGeneral, name='general'),
    path(r'<int:id_proyecto>/iniciar/', iniciarProyecto, name='iniciar'),
    path(r'<int:id_proyecto>/finalizar/', finalizarProyecto, name='finalizar'),
    path(r'<int:id_proyecto>/cancelar/', cancelarProyecto, name='cancelar'),
    path(r'<int:id_proyecto>/fases', createFases, name='nuevas_fases'),
    path(r'<int:id_proyecto>/fases/modificar', updateFases, name='modificar_fases'),
    path(r'<int:id_proyecto>/fases/listar', fasesProyecto, name='listar_fases'),
    path(r'<int:id_proyecto>/fases/<int:id_fase>/', verFase, name='ver_fase'),
    path(r'<int:id_proyecto>/fases/<int:id_fase>/cerrar/', finalizarFase, name='cerrar_fase'),
    path(r'<int:id_proyecto>/fases/<int:id_fase>/<int:id_lb>/', verLB, name='ver_lb'),
    path(r'<int:id_proyecto>/fases/<int:id_fase>/<int:id_lb>/crearSolicitud/', crerSolicitud, name='crear_solicitud'),
    path(r'<int:id_proyecto>/fases/<int:id_fase>/crearLB/', crearLB, name='crear_LB'),
    path(r'<int:id_proyecto>/fases/<int:id_fase>/<int:id_lb>/cerrar/', cerrarLB, name='cerrar_LB'),
    path(r'<int:id_proyecto>/fases/<int:id_fase>/tipoItem/<int:id_tipo>/', asignarTI, name='add_tipoItem'),
    path(r'<int:id_proyecto>/comite/', comite, name='comite'),
    path(r'<int:id_proyecto>/comite/nuevo/<int:id_usuario>', asignarComite, name='asignar_comite'),
    path(r'<int:id_proyecto>/comite/eliminar/<int:id_usuario>', desasignarComite, name='desasignar_comite'),
    path(r'<int:id_proyecto>/comite/verificar/', verificarComite, name='verificar_comite'),
    path(r'<int:id_proyecto>/solicitudes/', interfazVotacion, name='solicitudes_votacion'),
    path(r'<int:id_proyecto>/solicitudes/lista/', solicitudesProyecto, name='lista_solicitudes'),
    path(r'<int:id_proyecto>/solicitudes/<int:id_solicitud>/aceptar/', votoAceptar, name='solicitudes_aceptar'),
    path(r'<int:id_proyecto>/solicitudes/<int:id_solicitud>/rechazar/', votoRechazar, name='solicitudes_rechazar'),
    path(r'<int:id_proyecto>/solicitudes/<int:id_solicitud>/cambios/', verificarCambios, name='verificar_cambios'),
    path(r'<int:id_proyecto>/solicitudes/<int:id_solicitud>/mantener/<int:id_item>/', mantenerItem, name='cambios_mantener'),
    path(r'<int:id_proyecto>/solicitudes/<int:id_solicitud>/cambiar/<int:id_item>/', cambiarItem, name='cambios_cambiar'),
    path(r'<int:id_proyecto>/historial/', verHistorial, name='ver_historial'),
    path(r'<int:id_proyecto>/reportes/', reportes, name='reportes'),
    path(r'<int:id_proyecto>/fases/<int:id_fase>/reporteLB/', reporteLB, name='reporte_LB'),
]
