from django import template
from sgas.apps.proyectos.models import Fase
from django.contrib.auth.models import Permission
from sgas.apps.roles.models import Rol
register = template.Library()

@register.filter(name='pertenece_grupo')
def pertenece_grupo(user, group_name):
    group_name = "comite" + str(group_name)
    return user.groups.filter(name=group_name).exists()

@register.filter
def crear_lb(user, idFase):
    fase = Fase.objects.get(id=idFase)
    rol = Rol.objects.filter(fase=fase)
    perm = Permission.objects.get(codename='Crear lb')
    if len(rol) > 0:
        for x in range(0, len(rol)):
            if user.groups.filter(id=rol[x].grupo.id).exists() and \
                    perm in rol[x].grupo.permissions.all():
                    return True
    return False

@register.filter
def separacion(x, y):
    if x % int(y) == 0:
        return True
    else:
        return False
