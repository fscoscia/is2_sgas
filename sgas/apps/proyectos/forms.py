from django import forms

from sgas.apps.proyectos.models import Proyecto, Fase, LineaBase, SolicitudCambio, VotoSolicitud
from sgas.apps.usuarios.models import Perfil
from django.db.models import Q
from sgas.apps.items.models import Item

class ProyectoAdminForm(forms.ModelForm):

    class Meta:
        model = Proyecto
        fields = [
            'nombre',
            'descripcion',
            'gerente',
        ]
        labels = {
            'nombre': 'Nombre',
            'descripcion': 'Descripcion',
            'gerente': 'Gerente',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'descripcion': forms.TextInput(attrs={'class': 'form-control'}),
            'gerente': forms.Select(attrs={'class': 'form-control'}),
        }

    def __init__(self,*args, **kwargs):
        super(ProyectoAdminForm, self).__init__(*args, **kwargs)
        self.fields['gerente'].queryset = Perfil.objects.filter(~Q(id=1))

class ProyectoGerenteForm(forms.ModelForm):

    class Meta:
        model = Proyecto
        fields = [
            'nombre',
            'descripcion',
            'numFases',
        ]
        labels = {
            'nombre': 'Nombre',
            'descripcion': 'Descripcion',
            'numFases': 'Numero de Fases',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'descripcion': forms.TextInput(attrs={'class': 'form-control'}),
            'numFases': forms.NumberInput(attrs={'class': 'form-control'}),
        }


class FasesForm(forms.ModelForm):

    class Meta:
        model = Fase
        fields = [
            'nombre',
            'posicion',
        ]
        labels = {
            'nombre': 'Nombre',
            'posicion': 'Posicion',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'posicion': forms.NumberInput(),
        }

class LBForm(forms.ModelForm):
    nombre =  forms.CharField(widget=forms.TextInput(attrs={"class": "form-control", "readonly": "readonly"}))

    def __init__(self, *args, **kwargs):
        nombre = kwargs.pop('nombre')
        fase = kwargs.pop('fase')
        super(LBForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].initial = nombre
        self.fields['items'] = forms.ModelMultipleChoiceField(queryset=Item.objects.filter(estado='Aprobado', fase = fase).order_by('id', 'tipo'), widget=forms.CheckboxSelectMultiple())

    def save(self, commit=True, *args, **kwargs):
        if not commit:
            raise NotImplementedError("No se pudo crear la LB")
        fase = kwargs.get('fase')
        nombre = kwargs.get('nombre')
        items = kwargs.get('items')
        cantItems = kwargs.get('cantItems')
        usuario = kwargs.get('creador')
        instance = super(LBForm, self).save(commit=False)
        instance.fase = fase
        instance.nombre = nombre
        instance.creador = usuario
        instance.save()
        instance.items.set(items)
        instance.cantItems = cantItems
        instance.save()
        for item in items:
            item.estado = 'En LB'
            item.save()
        return instance

    class Meta:
        model = LineaBase
        fields = [
            'items',
        ]
        labels = {
            'items': 'Items'
        }

class SolicitudForm(forms.ModelForm):
    nombre =  forms.CharField(widget=forms.TextInput(attrs={"class": "form-control", "readonly": "readonly"}))

    def __init__(self, *args, **kwargs):
        nombre = kwargs.pop('nombre')
        lb = kwargs.pop('lb')
        super(SolicitudForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].initial = nombre
        self.fields['items'] = forms.ModelMultipleChoiceField(queryset=lb.items.all(), widget=forms.CheckboxSelectMultiple())

    def save(self, commit=True, *args, **kwargs):
        if not commit:
            raise NotImplementedError("No se pudo crear la solicitud de cambios")
        nombre = kwargs.get('nombre')
        items = kwargs.get('items')
        responsable = kwargs.get('responsable')
        lb = kwargs.get('lb')
        instance = super(SolicitudForm, self).save(commit=False)
        instance.nombre = nombre
        instance.lb = lb
        instance.responsable = responsable
        instance.save()
        instance.items.set(items)
        instance.save()
        return instance

    class Meta:
        model = SolicitudCambio
        fields = [
            'items',
            'motivo',
        ]
        labels = {
            'items': 'Items',
            'motivo': 'Motivo'
        }

        widgets = {
            'motivo': forms.Textarea(attrs={"class": "form-control", "rows":"5"})
        }

class ReporteSolicitudesForm(forms.Form):
    fechaInicio = forms.DateField(widget=forms.DateInput(attrs={'class':'form-control','type':'date'}))
    fechaFin = forms.DateField(widget=forms.DateInput(attrs={'class':'form-control','type':'date'}))
    horaInicio = forms.TimeField(widget=forms.TimeInput(attrs={'class':'form-control','type':'time'}))
    horaFin = forms.TimeField(widget=forms.TimeInput(attrs={'class':'form-control','type':'time'}))



class ReporteItemsFase(forms.Form):
    fase = forms.ModelChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), queryset=Fase.objects.all())

    def __init__(self,*args, **kwargs):
        proyecto = Proyecto.objects.get(id=kwargs.pop('proyecto'))
        super(ReporteItemsFase, self).__init__(*args, **kwargs)
        self.fields['fase'].queryset = Fase.objects.filter(proyecto=proyecto)


class ReporteItems(forms.Form):
    OPTIONS = (
        ("En Desarrollo", "En Desarrollo"),
        ("Aprobado", "Aprobado"),
        ("En LB", "En LB"),
        ("Desactivado", "Desactivado"),
        ("Para Aprobar", "Para Aprobar"),
    )
    estados = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(attrs={'class':'form-check-input','type':'checkbox'}), choices=OPTIONS)
