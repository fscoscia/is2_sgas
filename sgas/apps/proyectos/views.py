from django.shortcuts import render, redirect
from django.forms import inlineformset_factory
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView
from django.urls.base import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .models import Proyecto, Fase, LineaBase, SolicitudCambio, VotoSolicitud, Historial
from .forms import ProyectoAdminForm, ProyectoGerenteForm, LBForm, SolicitudForm, ReporteSolicitudesForm, ReporteItemsFase, ReporteItems
from sgas.apps.miembros.models import Miembro
from sgas.apps.usuarios.models import Perfil
from sgas.apps.items.models import Item
from sgas.apps.tipoitem.models import TipoItem
from sgas.apps.roles.models import Rol
from sgas.apps.relaciones.models import Relacion
from sgas.apps.relaciones.utils import bfsAdelante
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from datetime import datetime
from django.core.mail import send_mail
from django.db.models import Q
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.lib.styles import getSampleStyleSheet, TA_CENTER
from reportlab.lib import colors
from reportlab.platypus.tables import Table, TableStyle
from reportlab.platypus.paragraph import Paragraph

from django.http import HttpResponse

#===Mostrar Inicio===
def index(request):
    """
    Vista basada en funcion para mostrar la pagina de inicio del sistema.
    Recibe un 'request' como parametro.
    Muestra el template de inicio 'index.html'
    """
    return render(request, 'index.html')

#===Listar Proyectos===
class ProyectoList(LoginRequiredMixin, ListView):
    """
    Vista basada en la clase ListView para listar los Proyectos.
    No recibe parametros.
    Muestra la lista de los proyectos asociados en forma de tabla.
    """
    redirect_field_name = 'redirect_to'
    model = Proyecto
    template_name = 'listaProyectos.html'
    ordering = ['id']

# Verificar permisos: Admin crea proyecto con nombre, descripcion y gerente.
# Gerente define nro fases, nombres fases y orden fases
#===Crear Proyecto===
class ProyectoCreate(LoginRequiredMixin, CreateView):
    """
    Vista basada en la clase CreateView para crear un nuevo Proyecto.
    No recibe parametros.
    Una vez completados los campos del formulario, guarda la informacion
    y redirige a la lista de los proyectos asociados.
    """
    redirect_field_name = 'redirect_to'
    model = Proyecto
    form_class = ProyectoAdminForm
    template_name = 'nuevoProyecto.html'

    def get_success_url(self):
        gerente = Perfil.objects.get(id = self.request.POST['gerente'])
        proyecto = Proyecto.objects.get(id=self.object.pk)
        Miembro.objects.create(idPerfil= gerente, idProyecto=proyecto)
        comite = Group.objects.create(name='comite%s' % self.object.pk)
        proyecto.comite = comite
        proyecto.save()
        user = User.objects.get(username=self.request.user)
        perfil = Perfil.objects.get(user=user)
        Historial.objects.create(operacion='Proyecto creado', autor=perfil.__str__(), proyecto=proyecto, categoria="Proyecto")
        return reverse_lazy('usuarios:administrador')

#===Modificar Proyecto===
class ProyectoUpdate(LoginRequiredMixin, UpdateView):
    """
    Vista basada en la clase UpdateView para actualizar un Proyecto existente.
    Recibe como parametro el Primary Key (pk) por url.
    Una vez completados los cambios los campos del formulario, guarda la informacion
    y redirige a la lista de los proyectos asociados.
    """
    redirect_field_name = 'redirect_to'
    model = Proyecto
    #fields = ['nombre', 'descripcion', 'numFases']
    form_class = ProyectoGerenteForm
    template_name = 'nuevoProyecto.html'

    def get_success_url(self):
        return reverse_lazy('proyectos:nuevas_fases', kwargs={'id_proyecto': self.object.pk})

#===Crear Fases===
@login_required
def createFases(request, id_proyecto):
    """
    Vista basada en funcion para crear las Fases de un nuevo Proyecto.
    Recibe un 'request' y el 'id' del Proyecto correspondiente como parametros.
    Una vez completados los campos del formulario, guarda la informacion y redirige
    a la lista de los proyectos asociados.
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    FasesFormSet = inlineformset_factory(Proyecto, Fase, fields=('nombre', 'posicion',), can_delete=False,
                                         extra=proyecto.numFases, max_num=proyecto.numFases)

    if request.method == 'POST':
        formset = FasesFormSet(request.POST, instance=proyecto)
        if formset.is_valid():
            for x in range(0, len(formset)):
                if formset[x].cleaned_data.get('posicion') > proyecto.numFases:
                    messages.add_message(request, messages.ERROR, 'Posición de la fase %s no compatible' % formset[x].cleaned_data.get('nombre'))
                    return redirect('proyectos:modificar_fases', id_proyecto)
                for z in range(x+1, len(formset)):
                     if formset[x].cleaned_data.get('posicion') == formset[z].cleaned_data.get('posicion'):
                        messages.add_message(request, messages.ERROR, "Las fases '{0}' y '{1} tienen posiciones repetidas".format(formset[x].cleaned_data.get('nombre'), formset[z].cleaned_data.get('nombre')))
                        return redirect('proyectos:modificar_fases', id_proyecto)
            formset.save()

        return redirect('proyectos:general', id_proyecto)
    else:
        formset = FasesFormSet(instance=proyecto)

    return render(request, 'proyectos/nuevasFases.html', {'formset': formset})


#===Modificar Fases===
@login_required
def updateFases(request, id_proyecto):
    """
    Vista basada en funcion para modificar las Fases de un Proyecto existente.
    Recibe un 'request' y el 'id' del Proyecto correspondiente como parametros.
    Una vez completados los cambios en los campos del formulario, guarda la informacion
    actualizada y redirige a la lista de los proyectos asociados.
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    FasesFormSet = inlineformset_factory(Proyecto, Fase, fields=('nombre', 'posicion',), can_delete=False,
                                         extra=proyecto.numFases, max_num=proyecto.numFases)

    if request.method == 'GET':
        formset = FasesFormSet(instance=proyecto)
    else:
        formset = FasesFormSet(request.POST, instance=proyecto)
        if formset.is_valid():
            for x in range(0, len(formset)):
                if formset[x].cleaned_data.get('posicion') > proyecto.numFases:
                    messages.add_message(request, messages.ERROR, 'Posición de la fase %s no compatible' % formset[x].cleaned_data.get('nombre'))
                    return redirect('proyectos:modificar_fases', id_proyecto)
                for z in range(x+1, len(formset)):
                     if formset[x].cleaned_data.get('posicion') == formset[z].cleaned_data.get('posicion'):
                        messages.add_message(request, messages.ERROR, "Las fases '{0}' y '{1} tienen posiciones repetidas".format(formset[x].cleaned_data.get('nombre'), formset[z].cleaned_data.get('nombre')))
                        return redirect('proyectos:modificar_fases', id_proyecto)
            formset.save()
        return redirect('proyectos:general', id_proyecto)

    return render(request, 'proyectos/nuevasFases.html', {'formset': formset})


#===Ver Proyecto===
@login_required
def proyectoGeneral(request, id_proyecto):
    """
    Vista basada en funcion para mostrar el menu de un Proyecto dado
    Recibe un 'request' y el 'id' del Proyecto correspondiente como parametros.
    Muestra el Proyecto, la lista de los usuarios asociados, y las funciones
    correspondientes al mismo.
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    fases = Fase.objects.filter(proyecto=id_proyecto)
    miembros = Miembro.objects.filter(idProyecto=id_proyecto)
    solicitudes = SolicitudCambio.objects.none()
    for fase in fases:
        lbs = LineaBase.objects.filter(fase=fase)
        for lb in lbs:
            solicitudes |= SolicitudCambio.objects.filter(estado='Aprobada', lb=lb)
    return render(request, 'proyectos/proyectoGeneral.html', {'miembros': miembros, 'proyecto': proyecto, 'fases':fases, 'solicitudes':solicitudes})


def solicitudesProyecto(request, id_proyecto):
    proyecto = Proyecto.objects.get(id=id_proyecto)
    fases = Fase.objects.filter(proyecto=id_proyecto)
    solicitudes = SolicitudCambio.objects.none()
    for fase in fases:
        lbs = LineaBase.objects.filter(fase=fase)
        for lb in lbs:
            solicitudes |= SolicitudCambio.objects.filter(estado='Aprobada', lb=lb)
    #print(solicitudes)
    return render(request, 'proyectos/solicitudesUsuario.html', {'proyecto': proyecto, 'solicitudes':solicitudes})


#===Ver Fases de un Proyecto===
@login_required
def fasesProyecto(request, id_proyecto):
    """
    Vista basada en función para mostrar las fases de un proyecto específico
    Recibe la petición http y el id del proyecto en cuestión
    Muestra el nombre, número de ítems, número de LB de cada fase
    """
    fases = Fase.objects.filter(proyecto=id_proyecto).order_by('posicion')
    return render(request, 'proyectos/fasesProyecto.html', {'fases': fases, 'proyecto':id_proyecto})


#===Iniciar Proyecto===
@login_required
def iniciarProyecto(request, id_proyecto):
    """
    Función para cambiar el estado de un proyecto de 'Pendiente' a 'Iniciado'
    Recibe la petición http y el id del proyecto que se desea iniciar
    Previo al cambio de estado hace las comprobaciones correspondientes
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    fases = Fase.objects.filter(proyecto=id_proyecto)
    comite = Group.objects.get(name='comite%s' % id_proyecto)
    i = 0
    if len(fases) == proyecto.numFases:
        i = i + 1
    else:
        messages.error(request, 'No se han creado todas las fases')
    if len(fases) > 0:
        for x in range(0, len(fases)):
            if len(TipoItem.objects.filter(fase=fases[x])) > 0:
                i = i + 1
                #print(i)
            else:
                messages.add_message(request, messages.ERROR, 'La fase %s aún no posee un tipo de ítem asociado' % fases[x].nombre)

    if len(comite.user_set.all()) > 1 and len(comite.user_set.all()) % 2 != 0:
        i = i + 1
    else:
        messages.add_message(request, messages.ERROR, 'Comité de control de cambios mal configurado')
    if i == len(fases) + 2:
        proyecto.estado = 'Iniciado'
        proyecto.fechaInicio = datetime.now()
        proyecto.save()
        miembros = Miembro.objects.filter(idProyecto=proyecto)
        correos = []
        for miembro in miembros:
            correos.append(miembro.idPerfil.user.email)
        send_mail('Proyecto Iniciado',
                      "Usted es miembro del proyecto '{0}' y el mismo acaba de iniciar, ingrese a la plataforma para realizar sus tareas.".format(proyecto.nombre),
                        'is2.sgas@gmail.com', correos)
        user = User.objects.get(username=request.user)
        perfil = Perfil.objects.get(user=user)
        Historial.objects.create(operacion='Iniciar Proyecto', autor=perfil.__str__(), proyecto=proyecto, categoria="Proyecto")
    return redirect('proyectos:general', id_proyecto)


#===Cancelar Proyecto===
@login_required
def cancelarProyecto(request, id_proyecto):
    """
    Función para cambiar el estado de un proyecto a 'Cancelado'
    Recibe la petición http y el id del proeycto que se desea cancelar
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    gerente = proyecto.gerente
    fases = Fase.objects.filter(proyecto=id_proyecto)
    for fase in fases:
        fase.estado = 'Cerrada'
        fase.save()
    proyecto.estado = 'Cancelado'
    proyecto.fechaFin = datetime.now()
    proyecto.save()
    miembros = Miembro.objects.filter(idProyecto=proyecto)
    correos = []
    for miembro in miembros:
        correos.append(miembro.idPerfil.user.email)
        send_mail('Proyecto Cancelado',
                      "Usted es miembro del proyecto '{0}' y el mismo acaba de ser CANCELADO.".format(proyecto.nombre),
                        'is2.sgas@gmail.com', correos)
    return redirect('proyectos:general', id_proyecto)


#===Ver Fase de Proyecto===
@login_required
def verFase(request, id_proyecto, id_fase):
    """
    Función para ver los datos de una fase específica de un proyecto
    Recibe la petición http, el id del proyecto y el id de la fase en cuestión
    Muestra los datos específicos de la fase, tipos de ítems asociados y funciones disponibles
    """
    fase = Fase.objects.get(id=id_fase)
    tipos = TipoItem.objects.filter(fase=id_fase).order_by('id')
    roles = Rol.objects.filter(fase=id_fase)
    lbs = LineaBase.objects.filter(fase=id_fase).order_by('id')
    miembros= User.objects.none()
    for x in range(0, len(roles)):
        grupo = Group.objects.get(id=roles[x].grupo.id)
        miembros |= grupo.user_set.all()
    tiposSF = TipoItem.objects.filter(proyecto=id_proyecto, fase=None)
    return render(request, 'proyectos/verFase.html', {'fase': fase, 'tipos': tipos, 'tiposSF': tiposSF, 'miembros': miembros, 'lbs':lbs})


#===Finalizar Fase===
#@login_required
def finalizarFase(request, id_proyecto, id_fase):
    """
    Función para finalizar una determinada Fase del Proyecto.
    Recibe los identificadores del Proyecto y de la Fase en cuestión.
    Comprueba que todos sus Ítems estén en Líneas Base cerradas y que la Fase anterior esté cerrada.
    Cambia el estado de la Fase a "Cerrada".
    """
    fase = Fase.objects.get(id=id_fase)
    items = Item.objects.filter(fase=fase)
    items = items.exclude(estado='Desactivado')
    lbs = LineaBase.objects.filter(fase=fase)
    lbs = lbs.exclude(estado='Rota')
    puede_cerrar = True

    #Si la fase no tiene Ítems no se puede finalizar
    if len(items) == 0:
        messages.add_message(request, messages.ERROR,
                             'La fase debe tener al menos un Ítem')
        puede_cerrar = False

    #Ciclo para recorrer Ítems
    for item in items:
        # Para Relaciones en Fase 1
        if fase.posicion == 1:
            relacion = Relacion.objects.filter((Q(tipo=1)|Q(tipo=2))&(Q(itemA=item)|Q(itemB=item)))
            if not relacion:
                messages.add_message(request, messages.ERROR,
                                     "El Ítem {} no tiene relaciones <a href='tipoitem/{}/item/{}/relaciones/nuevoPH/'>Seleccionar Padre</a>".format(item.identificador, item.tipo.id, item.id))
                puede_cerrar = False

        # Para las demás Fases
        if item.estado != "En LB":
            messages.add_message(request, messages.ERROR,
                                 'El Ítem {} no está en una Línea Base'.format(item.identificador))
            puede_cerrar = False

    #Ciclo para recorrer Líneas Base
    for lb in lbs:
        if lb.estado != "Cerrada":
            messages.add_message(request, messages.ERROR,
                                 'La Línea Base {} no se encuentra Cerrada'.format(lb.nombre))
            puede_cerrar = False

    #Fases mayores a 1
    if fase.posicion > 1:
        fase_anterior = Fase.objects.get(id=fase.id-1)
        if fase_anterior.estado != "Cerrada":
            messages.add_message(request, messages.ERROR,
                                 'La Fase {} no está cerrada aún'.format(fase_anterior.nombre))
            puede_cerrar = False

    #Cierre de Fase
    if puede_cerrar:
        fase.estado = "Cerrada"
        fase.save()
        user = User.objects.get(username=request.user)
        perfil = Perfil.objects.get(user=user)
        Historial.objects.create(operacion='Finalizar fase {}'.format(fase.nombre), autor=perfil.__str__(), proyecto=fase.proyecto, categoria="Proyecto")

    return redirect("proyectos:ver_fase", id_proyecto, id_fase)

#===Asignar Tipo de Ítem a Fase===
@login_required
def asignarTI(request, id_proyecto, id_fase, id_tipo):
    """
    Asigna un tipo de ítem a una fase
    Recibe la petición http, el id del proyecto, el id de la fase y el id del tipo de ítem
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    tipo = TipoItem.objects.get(id=id_tipo)
    fase = Fase.objects.get(id= id_fase)
    tipo.fase = fase
    tipo.save()
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    Historial.objects.create(operacion='Asignar {} a {}'.format(tipo.nombre, fase.nombre), autor=perfil.__str__(), proyecto=proyecto, categoria="Items")
    return redirect('proyectos:ver_fase', id_proyecto, id_fase)


# ===Vista Comité de Cambios===
def comite(request, id_proyecto):
    """
    Muestra los miembros del proyecto divididos en quienes forman parte del comité de cambios
    y quienes no. Ofrece las opciones de asignación y desasignación al mismo.
    Recibe la petición http y el id del proyecto
    """
    miembros = Miembro.objects.filter(idProyecto=id_proyecto)
    miembrosNo = miembros
    for x in range(0, len(miembros)):
        if miembros[x].idPerfil.user.groups.filter(name='comite%s' % id_proyecto).exists():
            miembrosNo = miembrosNo.exclude(idPerfil=miembros[x].idPerfil)
    comite = Group.objects.get(name='comite%s' % id_proyecto)
    comite = comite.user_set.all()
    return render(request, 'proyectos/comite.html', {'miembros':miembrosNo, 'comite':comite, 'idProyecto':id_proyecto})

# ===Asignar a Comité===
@login_required()
def asignarComite(request, id_proyecto, id_usuario):
    """
    Agrega un miembro del proyecto al comité de control de cambios de dicho proyecto
    Recibe la petición http, el id del proyecto y el id del usuario
    Redirige al control de requerimientos del comité
    """
    comite = Group.objects.get(name='comite%s' % id_proyecto)
    usuario = User.objects.get(id=id_usuario)
    comite.user_set.add(usuario)
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    perfil2 = Perfil.objects.get(user=usuario)
    Historial.objects.create(operacion='Asignar {} a Comite de Cambios'.format(perfil2.__str__()), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=id_proyecto), categoria="Proyecto")
    return redirect('proyectos:verificar_comite', id_proyecto)

# ===Eliminar de Comité"
@login_required()
def desasignarComite(request, id_proyecto, id_usuario):
    """
    Elimina a un miembro del proyecto del comité de control de cambios
    Recibe la petición http, el id del proyecto y el id del usuario
    Redirige al control de requerimientos del comité
    """
    comite = Group.objects.get(name='comite%s' % id_proyecto)
    usuario = User.objects.get(id=id_usuario)
    comite.user_set.remove(usuario)
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    perfil2 = Perfil.objects.get(user=usuario)
    proyecto = Proyecto.objects.get(id=id_proyecto)
    fases = Fase.objects.filter(proyecto=proyecto)
    for fase in fases:
        lbs = LineaBase.objects.filter(fase=fase)
        for lb in lbs:
            solicitudes = SolicitudCambio.objects.filter(lb=lb, estado='Pendiente')
            for solicitud in solicitudes:
                voto = VotoSolicitud.objects.filter(usuario=usuario, solicitud=solicitud)
                for v in voto:
                    if v.aFavor:
                        solicitud.aFavor = solicitud.aFavor - 1
                    else:
                        solicitud.enContra = solicitud.enContra - 1
                    solicitud.save()

    Historial.objects.create(operacion='Desasignar {} de Comite de Cambios'.format(perfil2.__str__()), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=id_proyecto), categoria="Proyecto")
    return redirect('proyectos:verificar_comite', id_proyecto)

# ===Control Comité===
@login_required()
def verificarComite(request, id_proyecto):
    """
    Función de verificación de requisitos del comité de control de cambios.
    Recibe la petición http y el id del proyecto.
    Redirige a la interfaz del comité añadiendo mensajes de error si éstos fueran
    necesarios.
    """
    comite = Group.objects.get(name='comite%s' % id_proyecto)
    if len(comite.user_set.all()) <= 1:
        messages.add_message(request, messages.ERROR, 'La cantidad de miembros del comité debe mayor a 1')
    elif len(comite.user_set.all()) % 2 == 0:
        messages.add_message(request, messages.ERROR, 'La cantidad de miembros del comité debe ser impar')
    return redirect('proyectos:comite', id_proyecto)


# === Finalizar Proyecto ===
@login_required()
def finalizarProyecto(request, id_proyecto):
    """
    Finaliza un proyecto si cumple con las condiciones (todas las fases finalizadas)
    Recibe el id del proyecto
    Despliega mensajes de error en caso de que existan fases abiertas
    """
    proyecto = Proyecto.objects.get(id= id_proyecto)
    fases = Fase.objects.filter(proyecto=id_proyecto)
    i = len(fases)
    for x in range(0, len(fases)):
        if fases[x].estado != 'Cerrada':
            messages.add_message(request, messages.ERROR, 'Fase %s sin finalizar' % fases[x].nombre)
            i = i - 1
    if i == len(fases):
        proyecto.estado = 'Finalizado'
        proyecto.fechaFin = datetime.now()
        proyecto.save()
        miembros = Miembro.objects.filter(idProyecto=proyecto)
        correos = []
        for miembro in miembros:
            correos.append(miembro.idPerfil.user.email)
        send_mail('Proyecto Finalizado', "Usted es miembro del proyecto '{0}' y el mismo acaba de finalizar.".format(proyecto.nombre),
                        'is2.sgas@gmail.com', correos)
        user = User.objects.get(username=request.user)
        perfil = Perfil.objects.get(user=user)
        Historial.objects.create(operacion='Finalizar proyecto', autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=id_proyecto), categoria="Proyecto")
    return redirect('proyectos:general', id_proyecto)

# ===Crear Línea Base===
@login_required()
def crearLB(request, id_proyecto, id_fase):
    """
    Crea una LB sobre el conjunto de ítems seleccionados por el usuario.
    Recibe el id del proyecto y el id de la fase.
    Actualiza la cantidad de LB de la fase y el estado de los ítems.
    """
    fase = Fase.objects.get(id=id_fase)
    nombre = "LB_{}_{}".format(fase.nombre, fase.numLB + 1)
    if request.method == 'POST':
        lbForm = LBForm(request.POST, nombre=nombre, fase=fase)
        if lbForm.is_valid():
            items = lbForm.cleaned_data['items']
            l = lbForm.save(nombre= nombre, fase=fase, cantItems= len(items), creador= request.user, items=items)
            fase.numLB = fase.numLB + 1
            fase.save()
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Crear LB en {}'.format(fase.nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=id_proyecto), categoria="LB")
            cerrarLB(request, id_proyecto, id_fase, l.id)
            return redirect('proyectos:ver_fase', id_proyecto=id_proyecto, id_fase=id_fase)
    else:
        lbForm = LBForm(nombre=nombre, fase=fase)

    return render(request, 'proyectos/lb_form.html', {"form": lbForm, "fase": fase})


#===Ver LB===
@login_required()
def verLB(request, id_proyecto, id_fase, id_lb):
    """
    Interfaz de Línea Base. Muestra información sobre una línea base específica.
    Recibe el id del proyecto, de la fase y de la línea base a mostrar.
    """
    lb = LineaBase.objects.get(id=id_lb)
    solicitudes = SolicitudCambio.objects.filter(Q(lb=lb) & (Q(estado='Pendiente') | Q(estado='Aprobada') ))
    return render(request, 'proyectos/ver_lb.html', {'lb': lb, 'idProyecto': id_proyecto, 'idFase': id_fase,
                                                     'solicitudes': solicitudes})

#===Cerrar Línea Base===
#@login.required
@login_required()
def cerrarLB(request, id_proyecto, id_fase, id_lb):
    """
    Función que cierra la Línea Base seleccionada.
    Recibe los identificadores de Proyecto, Fase y Línea Base correspondientes.
    Cambia el estado de la Línea Base a "Cerrada".
    """
    lb = LineaBase.objects.get(id=id_lb)
    lb.estado = "Cerrada"
    lb.save()
    messages.add_message(request, messages.SUCCESS, 'Línea Base {} cerrada correctamente'.format(lb.nombre))
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    Historial.objects.create(operacion='Cerrar linea base {}'.format(lb.nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=id_proyecto), categoria="Proyecto")
    return redirect('proyectos:ver_lb', id_proyecto, id_fase, id_lb)


#===Crear Solicitud de Cambio===
@login_required()
def crerSolicitud(request, id_proyecto, id_fase, id_lb):
    """
    Crea una solicitud de cambio sobre los ítems seleccionados y
    notifica a los miembros del comité acerca de la nueva solicitud.
    Recibe el id del proyecto, de la fase y la línea base.
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    fase = Fase.objects.get(id=id_fase)
    lb = LineaBase.objects.get(id=id_lb)
    nombre = "SOLICITUD_{}_{}_{}".format(proyecto.nombre, fase.nombre, lb.nombre)
    if request.method == 'GET':
        solicitudForm = SolicitudForm(nombre= nombre, lb= lb)
    else:
        solicitudForm = SolicitudForm(request.POST, nombre= nombre, lb= lb)
        if solicitudForm.is_valid():
            items = solicitudForm.cleaned_data['items']
            solicitudForm.save(nombre= nombre, lb= lb, items= items, responsable= request.user)
            comite = Group.objects.get(name='comite%s' % id_proyecto)
            comite = comite.user_set.all()
            correos = []
            for x in range(0, len(comite)):
                correos.append(comite[x].email)
            send_mail('Solicitud de Cambio Pendiente',
                      "Se ha creado una solicitud de cambio que afecta a la línea base '{0}' del proyecto '{1}', emita su voto al respecto. Otros detalles serán proporcionados en la plataforma.".format(lb.nombre, proyecto.nombre),
                        'is2.sgas@gmail.com', correos)
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Solicitud de cambio en LB {}'.format(lb.__str__()), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=id_proyecto), categoria="LB")
            return redirect('proyectos:ver_fase', id_proyecto=id_proyecto, id_fase=id_fase)

    return render(request, 'proyectos/crear_solicitud.html', {'form': solicitudForm, 'lb': lb})

#===Interfaz de Votación de Solicitudes de Cambio"
@login_required()
def interfazVotacion(request, id_proyecto):
    """
    Muestra todas las solicitudes de cambio pendientes.
    Recibe el id del proyecto.
    La cantidad de solicitudes varía de acuerdo a cada miembro del comitè
    ya que solo se muestran aquellas en las que no se ha emitido el voto.
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    fases = Fase.objects.filter(proyecto=proyecto)
    lbs = LineaBase.objects.none()
    for fase in fases:
        lbs |= LineaBase.objects.filter(fase= fase)
    solicitudes = SolicitudCambio.objects.none()
    for lb in lbs:
        solicitudes |= SolicitudCambio.objects.filter(estado='Pendiente', lb= lb)
    solicitudes2 = solicitudes
    for solicitud in solicitudes2:
        voto = VotoSolicitud.objects.filter(solicitud=solicitud, usuario=request.user)
        if voto:
            solicitudes = solicitudes.exclude(id=solicitud.id)
    return render(request, 'proyectos/solicitudes.html', {'solicitudes': solicitudes, 'idProyecto':id_proyecto})

#===Voto a Favor===
@login_required()
def votoAceptar(request, id_proyecto, id_solicitud):
    """
    Procesa un voto a favor del cambio según una solicitud.
    Recibe el id del proyecto y el id de la solicitud de cambio.
    Guarda el voto y el usuario que lo realizó, y actualiza la cantidad de votos de la solicitud.
    """
    comite = Group.objects.get(name='comite{}'.format(id_proyecto))
    comite = comite.user_set.all()
    solicitud = SolicitudCambio.objects.get(id=id_solicitud)
    VotoSolicitud.objects.create(solicitud=solicitud, aFavor=True, usuario=request.user)
    solicitud.aFavor = solicitud.aFavor + 1
    if solicitud.aFavor + solicitud.enContra == len(comite):
        if solicitud.aFavor > solicitud.enContra:
            solicitud.estado = 'Aprobada'
            romperLb(solicitud)
        else:
            solicitud.estado = 'Desaprobada'
        notificarResultado(solicitud, id_proyecto)
    solicitud.save()
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    Historial.objects.create(operacion='Voto a favor de la solicitud  {}'.format(solicitud.nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=id_proyecto), categoria="LB")
    return redirect('proyectos:solicitudes_votacion', id_proyecto)


#===Voto  En Contra===
@login_required()
def votoRechazar(request, id_proyecto, id_solicitud):
    """
    Procesa un voto en contra del cambio según una solicitud.
    Recibe el id del proyecto y el id de la solicitud de cambio.
    Guarda el voto y el usuario que lo realizó, y actualiza la cantidad de votos de la solicitud.
    """
    comite = Group.objects.get(name='comite{}'.format(id_proyecto))
    comite = comite.user_set.all()
    solicitud = SolicitudCambio.objects.get(id=id_solicitud)
    VotoSolicitud.objects.create(solicitud=solicitud, enContra=True, usuario=request.user)
    solicitud.enContra = solicitud.enContra + 1
    if solicitud.aFavor + solicitud.enContra == len(comite):
        if solicitud.aFavor > solicitud.enContra:
            solicitud.estado = 'Aprobada'
            romperLb(solicitud)
        else:
            solicitud.estado = 'Desaprobada'
        notificarResultado(solicitud, id_proyecto)
    solicitud.save()
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    Historial.objects.create(operacion='Voto en contra de solicitud {}'.format(solicitud.nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=id_proyecto), categoria="LB")
    return redirect('proyectos:solicitudes_votacion', id_proyecto)


#=== Romper Línea Base===
def romperLb(solicitud):
    """
    Realiza el proceso de romper una línea base.
    Recibe la solicitud de cambio.
    Cambia el estado de una lb a 'Rota' y pone en revisión los ítems afectados por el ítem a cambiar,
    asimismo pone en estado 'aprobado' los ítems de la lb que no son afectados.
    """
    solicitud.lb.estado = 'Rota'
    solicitud.lb.save()
    hijoSucesor = verificarHS(solicitud.id, 1)
    for hs in hijoSucesor:
        hs.estado = 'En Revision'
        hs.save()
    for item in solicitud.lb.items.all():
        item.estado = 'Aprobado'
        item.save()
    for item in solicitud.items.all():
        item.estado ='En Desarrollo'
        item.save()

#===Notificar Resultado===
def notificarResultado(solicitud, id_proyecto):
    """
    Procedimiento auxiliar
    Notifica por correo a los miembros del comité de control de cambios el resultado de una votación.
    Recibe el id del proyecto y la solicitud en cuestión.
    """
    if solicitud.aFavor > solicitud.enContra:
        mensaje = 'Aprobada'
    else:
        mensaje = 'Rechazada'
    dif = abs(solicitud.aFavor - solicitud.enContra)
    comite = Group.objects.get(name='comite%s' % id_proyecto)
    comite = comite.user_set.all()
    correos = []
    for x in range(0, len(comite)):
        correos.append(comite[x].email)
    send_mail('Solicitud de Cambio {}'.format(mensaje),
              'La solicitud {0} fue {1} por mayoría, con una diferencia de {2} voto/s'.format(solicitud.nombre, mensaje, dif),
              'is2.sgas@gmail.com', correos)

#===Ver Historial===
@login_required()
def verHistorial(request, id_proyecto):
    """
    Muestra el historial de cambios de todo el proyecto.
    Recibe el id del proyecto.
    Procede a mostrar todos los mensajes guardados en el historial del proyecto desde que se creo.
    """
    historiales = Historial.objects.filter(proyecto=id_proyecto)
    mensajes = []
    for x in range(0, len(historiales)):
        mensajes.append(historiales[x].__str__())
    return render(request, 'proyectos/ver_historial.html', {'mensajes':mensajes, 'idProyecto': id_proyecto})


#===Verificar Hijos/Sucesores===
def verificarHS(id_solicitud, est):
    """
    Procedimiento auxiliar.
    Recupera los hijos y sucesores de un ítem según parámetro que indica si el estado de los ítems
    buscados es 'Aprobado o en LB' ó 'En Revisión'.
    """
    solicitud = SolicitudCambio.objects.get(id=id_solicitud)
    lb = LineaBase.objects.get(id=solicitud.lb.id)
    hs = Item.objects.none()
    it = Item.objects.filter
    for item in lb.items.all():
        visitedF = set()
        r = set()
        bfsAdelante(visitedF, item.id, r)
        fVisited = list(visitedF)
        for i in fVisited:
            if(est == 1):
                it = Item.objects.filter(Q(id=i) & (Q(estado='Aprobado') | Q(estado='En LB')))
            else:
                it = Item.objects.filter(Q(id=i) & Q(estado='En Revision'))
            hs |= it
    for item in lb.items.all():
        hs = hs.exclude(id=item.id)
    return hs


#===Verificar Cambios===
@login_required()
def verificarCambios(request, id_proyecto, id_solicitud):
    """
    Muestra al usuario los ítems afectados por el rompimiento de una línea base.
    Recibe la petición http, el id del proyecto y el id de la solicitud de cambio.
    Renderiza la lista de ítems afectados-
    """
    solicitud = SolicitudCambio.objects.get(id=id_solicitud)
    hs = verificarHS(id_solicitud, 2)
    if hs:
        return render(request, 'proyectos/verificar_cambios.html', {'hs':hs, 'idProyecto':id_proyecto, 'idSolicitud':id_solicitud})
    else:
        solicitud.estado = 'Completada'
        solicitud.save()
        messages.add_message(request, messages.INFO,'Los ítems pueden ser modificados')
        return redirect('proyectos:general', id_proyecto)


@login_required()
#===Mantener Ítem===
def mantenerItem(request, id_proyecto, id_solicitud, id_item):
    """
    Deja un ítem afectado por el rompimiento de una lb sin modificaciones.
    Recibe la petición http, el id del proyecto, de la solicitud y del ítem.
    Redirige a la lista de ítems afectados.
    """
    item = Item.objects.get(id=id_item)
    fase = Fase.objects.get(id= item.fase.id)
    lbs = LineaBase.objects.filter(fase=fase)
    estado = 'Aprobado'
    for lb in lbs:
        if item in lb.items.all():
            estado = 'En LB'
            break
    item.estado = estado
    item.save()
    return redirect('proyectos:verificar_cambios', id_proyecto, id_solicitud)


@login_required()
#===Cambiar ítem===
def cambiarItem(request, id_proyecto, id_solicitud, id_item):
    """
    Cambia el estado de un ítem a 'En desarrollo' si es posible, luego de que éste ítem se viera afetado
    por el rompimiento de una línea base.
    Recibe la petición http, el id del proyecto, de la solicitud y del ítem.
    Redirige a la lista de ítems afectados.
    """
    item = Item.objects.get(id=id_item)
    fase = Fase.objects.get(id= item.fase.id)
    lbs = LineaBase.objects.filter(Q(fase=fase) & (Q(estado='Cerrada') | Q(estado='Abierta')))
    romper = False
    for lb in lbs:
        if item in lb.items.all():
            romper = True
            break
    if romper:
        item.estado = 'En LB'
        item.save()
        messages.add_message(request, messages.INFO,
                                     'El ítem {} pertenece a una línea base, debe generar una solicitud de cambio'.format(item.identificador))
    else:
        item.estado = 'En Desarrollo'
        item.save()
        messages.add_message(request, messages.INFO,
                                     'El ítem {} ya puede ser modificado'.format(item.identificador))

    return redirect('proyectos:verificar_cambios', id_proyecto, id_solicitud)

#===Reporte de Solicitudes de Cambio===
def reporteSolicitudes(reporteForm, proyecto):
    """
    Genera un archivo pdf con la información solicitada.
    Recibe el formulario en donde se encuentran los parámetros de búsqueda y el el proyecto objetivo.
    Dada una fecha y hora de inicio y fin, retorna un archivo pdf que contiene las solicitudes de cambio
    realizadas en el periodo indicado.
    """
    if reporteForm.is_valid():
        fechaInicio = reporteForm.cleaned_data['fechaInicio']
        fechaFin = reporteForm.cleaned_data['fechaFin']
        horaInicio = reporteForm.cleaned_data['horaInicio']
        horaFin = reporteForm.cleaned_data['horaFin']
        fases = Fase.objects.filter(proyecto=proyecto)
        lbs = LineaBase.objects.none()
        for fase in fases:
            lbs |= LineaBase.objects.filter(fase=fase)
        solicitudes = SolicitudCambio.objects.none()
        for lb in lbs:
            solicitudes |= SolicitudCambio.objects.filter(fecha__range=[fechaInicio, fechaFin], lb= lb)
        listaSolicitudes = []
        for solicitud in solicitudes:
            hora = solicitud.hora
            if solicitud.fecha == fechaInicio and hora >= horaInicio:
                listaSolicitudes.append(solicitud)
            elif solicitud.fecha == fechaFin and hora <= horaFin:
                listaSolicitudes.append(solicitud)
            elif solicitud.fecha != fechaInicio and solicitud.fecha != fechaFin:
                listaSolicitudes.append(solicitud)

        #Convierte el response en un archivo pdf en lugar de una dirección http
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename={}_Reporte_Solicitudes'.format(proyecto.nombre)
        buffer = BytesIO()
        #Cabecera del Archivo
        c = canvas.Canvas(buffer, pagesize=A4)
        c.setLineWidth(.3)
        c.setFont('Helvetica', 22)
        c.drawString(30, 750, proyecto.nombre)
        c.setFont('Helvetica', 12)
        c.drawString(30, 735, 'Reporte de Solicitudes de Cambio')
        c.setFont('Helvetica', 10)
        c.drawString(30, 720, 'Periodo: {} del {} al {} del {}'.format(horaInicio, fechaInicio.__format__('%d/%m/%Y') , horaFin, fechaFin.__format__('%d/%m/%Y')))
        c.drawString(30, 705, 'Cantidad de Solicitudes: {}'.format(len(listaSolicitudes)))
        c.line(30, 690, 560, 690)
        c.setFont('Helvetica-Bold', 12)
        c.drawString(480, 750, str(datetime.now().date().__format__('%d/%m/%Y')))
        c.line(460, 747, 560, 747)
        #Cabecera de la Tabla
        styles = getSampleStyleSheet()
        styleBH = styles['Normal']
        styleBH.alignment = TA_CENTER
        styleBH.fontSize = 9
        id = Paragraph('''ID''', styleBH)
        nombre = Paragraph('''Nombre''', styleBH)
        estado = Paragraph('''Estado''', styleBH)
        aFavor = Paragraph('''A Favor''', styleBH)
        enContra = Paragraph('''En Contra''', styleBH)
        responsable = Paragraph('''Responsable''', styleBH)
        data = [[id, nombre, estado, aFavor, enContra, responsable]]
        #Contenido de la Tabla
        styleN = styles['BodyText']
        styleN.alignment = TA_CENTER
        styleN.fontSize = 8
        high = 650
        for solicitud in listaSolicitudes:
            s = [Paragraph(str(solicitud.id), styleN), Paragraph(str(solicitud.nombre), styleN),
            Paragraph(str(solicitud.estado), styleN), Paragraph(str(solicitud.aFavor), styleN),
            Paragraph(str(solicitud.enContra), styleN), Paragraph(solicitud.responsable.first_name + ' ' + solicitud.responsable.last_name, styleN)]
            data.append(s)
            high = high - 18
        #Tamaño de la tabla
        width, height = A4
        table = Table(data, colWidths=[1 * cm, 8.5 * cm, 2.2 * cm, 2 * cm, 2 * cm, 3.4 * cm])
        table.setStyle(TableStyle([
            ('INNERGRID', (0,0), (-1, -1), 0.25, colors.black),
            ('BOX', (0,0), (-1, -1), 0.25, colors.black),
        ]))
        table.wrapOn(c, width, height)
        table.drawOn(c, 30, high)
        c.showPage()
        c.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response


#===Reporte de Ítems por Fase===
def reporteItemsFase(reporteForm, proyecto):
    """
    Genera un archivo pdf con la información solicitada.
    Recibe el formulario en donde se encuentran los parámetros de búsqueda y el el proyecto objetivo.
    Dada una fase del proyecto retorna un archivo pdf que contiene todos los ítems de dicha fase.
    """
    if reporteForm.is_valid():
        fase = reporteForm.cleaned_data['fase']
        items = Item.objects.filter(fase=fase)
        #Convierte el response en un archivo pdf en lugar de una dirección http
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename={}_Reporte_Items_Fase'.format(proyecto.nombre)
        buffer = BytesIO()
        #Cabecera del Archivo
        c = canvas.Canvas(buffer, pagesize=A4)
        c.setLineWidth(.3)
        c.setFont('Helvetica', 22)
        c.drawString(30, 750, proyecto.nombre)
        c.setFont('Helvetica', 12)
        c.drawString(30, 735, 'Reporte de Ítems por Fase')
        c.setFont('Helvetica', 10)
        c.drawString(30, 720, 'Fase: {}'.format(fase.nombre))
        c.drawString(30, 705, 'Cantidad de Ítems: {}'.format(len(items)))
        c.line(30, 690, 560, 690)
        c.setFont('Helvetica-Bold', 12)
        c.drawString(480, 750, str(datetime.now().date().__format__('%d/%m/%Y')))
        c.line(460, 747, 560, 747)
        #Cabecera de la Tabla
        styles = getSampleStyleSheet()
        styleBH = styles['Normal']
        styleBH.alignment = TA_CENTER
        styleBH.fontSize = 9
        identificador = Paragraph('''Identificador''', styleBH)
        estado = Paragraph('''Estado''', styleBH)
        descripcion = Paragraph('''Descripción''', styleBH)
        costo = Paragraph('''Costo''', styleBH)
        data = [[identificador, estado,  costo,descripcion]]
        #Contenido de la Tabla
        styleN = styles['BodyText']
        styleN.alignment = TA_CENTER
        styleN.fontSize = 8
        high = 650
        for item in items:
            s = [Paragraph(str(item.identificador), styleN), Paragraph(str(item.estado), styleN),
                 Paragraph(str(item.costo), styleN), Paragraph(str(item.descripcion), styleN)]
            data.append(s)
            high = high - 18
        #Tamaño de la tabla
        width, height = A4
        table = Table(data, colWidths=[4 * cm, 4 * cm, 2 * cm, 8.7 * cm])
        table.setStyle(TableStyle([
            ('INNERGRID', (0,0), (-1, -1), 0.25, colors.black),
            ('BOX', (0,0), (-1, -1), 0.25, colors.black),
        ]))
        table.wrapOn(c, width, height)
        table.drawOn(c, 30, high)
        c.showPage()
        c.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

#===Reporte de Ítems===
def reporteItems(reporteForm, proyecto):
    """
    Genera un archivo pdf con la información solicitada.
    Recibe el formulario en donde se encuentran los parámetros de búsqueda y el el proyecto objetivo.
    Dado uno o más estados, retorna todos los ítems del proyecto que se encuentren en dicho/s estado/s.
    """
    if reporteForm.is_valid():
        estados = reporteForm.cleaned_data['estados']
        items = []
        fases = Fase.objects.filter(proyecto=proyecto)
        for fase in fases:
            if 'En Desarrollo' in estados:
                i = Item.objects.filter(fase=fase, estado='En Desarrollo')
                for it in i:
                    items.append(it)
            if 'Aprobado' in estados:
                i = Item.objects.filter(fase=fase, estado='Aprobado')
                for it in i:
                    items.append(it)
            if 'Desactivado' in estados:
                i = Item.objects.filter(fase=fase, estado='Desactivado')
                for it in i:
                    items.append(it)
            if 'Para Aprobar' in estados:
                i = Item.objects.filter(fase=fase, estado='Para Aprobar')
                for it in i:
                    items.append(it)
            if 'En LB' in estados:
                i = Item.objects.filter(fase=fase, estado='En LB')
                for it in i:
                    items.append(it)
        #Convierte el response en un archivo pdf en lugar de una dirección http
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename={}_Reporte_Items'.format(proyecto.nombre)
        buffer = BytesIO()
        #Cabecera del Archivo
        c = canvas.Canvas(buffer, pagesize=A4)
        c.setLineWidth(.3)
        c.setFont('Helvetica', 22)
        c.drawString(30, 750, proyecto.nombre)
        c.setFont('Helvetica', 12)
        c.drawString(30, 735, 'Reporte de Ítems')
        c.setFont('Helvetica', 10)
        c.drawString(30, 705, 'Cantidad de Ítems: {}'.format(len(items)))
        c.line(30, 690, 560, 690)
        c.setFont('Helvetica-Bold', 12)
        c.drawString(480, 750, str(datetime.now().date().__format__('%d/%m/%Y')))
        c.line(460, 747, 560, 747)
        #Cabecera de la Tabla
        styles = getSampleStyleSheet()
        styleBH = styles['Normal']
        styleBH.alignment = TA_CENTER
        styleBH.fontSize = 9
        identificador = Paragraph('''Identificador''', styleBH)
        estado = Paragraph('''Estado''', styleBH)
        descripcion = Paragraph('''Descripción''', styleBH)
        costo = Paragraph('''Costo''', styleBH)
        data = [[identificador, estado,  costo,descripcion]]
        #Contenido de la Tabla
        styleN = styles['BodyText']
        styleN.alignment = TA_CENTER
        styleN.fontSize = 8
        high = 650
        for item in items:
            s = [Paragraph(str(item.identificador), styleN), Paragraph(str(item.estado), styleN),
                 Paragraph(str(item.costo), styleN), Paragraph(str(item.descripcion), styleN)]
            data.append(s)
            high = high - 18
        #Tamaño de la tabla
        width, height = A4
        table = Table(data, colWidths=[4 * cm, 4 * cm, 2 * cm, 8.7 * cm])
        table.setStyle(TableStyle([
            ('INNERGRID', (0,0), (-1, -1), 0.25, colors.black),
            ('BOX', (0,0), (-1, -1), 0.25, colors.black),
        ]))
        table.wrapOn(c, width, height)
        table.drawOn(c, 30, high)
        c.showPage()
        c.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response
    else:
        return redirect('proyectos:reportes', proyecto.id)

@login_required()
def reportes(request, id_proyecto):
    """
    Interfaz de reportes.
    Recibe la petición http y el id del proyecto objetivo.
    Despliega los formularios para cada tipo de reporte y maneja la respuesta recibida según el tipo de
    reporte deseado.
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    if request.method == 'POST':
        if 'solicitudes' in request.POST:
            reporteForm = ReporteSolicitudesForm(request.POST)
            response = reporteSolicitudes(reporteForm, proyecto)
        elif 'itemsFase' in request.POST:
            reporteForm = ReporteItemsFase(request.POST, proyecto=proyecto.id)
            response = reporteItemsFase(reporteForm, proyecto)
        elif 'items' in request.POST:
            reporteForm = ReporteItems(request.POST)
            response = reporteItems(reporteForm, proyecto)
        return response
    else:
        reporteSolicitudesForm = ReporteSolicitudesForm()
        reporteItemsForm = ReporteItems
        reporteItemsFaseForm = ReporteItemsFase(proyecto=proyecto.id)
        return render(request, 'proyectos/reportes.html', {'reporteSForm':reporteSolicitudesForm, 'proyecto':proyecto, 'reporteIForm':reporteItemsFaseForm, 'reporteForm':reporteItemsForm})


#Reporte de Líneas Base
@login_required()
def reporteLB(request, id_proyecto, id_fase):
    """
    Genera un archivo pdf con la información solicitada.
    Recibe la petición http, el id del proyecto y la fase objetivo.
    Genera un archivo en donde se encuentran todas las líneas base de la fase solicitada y los ítems
    que se encuentran en las mismas.
    """
    fase = Fase.objects.get(id=id_fase)
    lbs = LineaBase.objects.filter(fase=fase)
    #Convierte el response en un archivo pdf en lugar de una dirección http
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename={}_Reporte_LB'.format(fase.proyecto.nombre)
    buffer = BytesIO()
    #Cabecera del Archivo
    c = canvas.Canvas(buffer, pagesize=A4)
    c.setLineWidth(.3)
    c.setFont('Helvetica', 22)
    c.drawString(30, 750, fase.proyecto.nombre)
    c.setFont('Helvetica', 12)
    c.drawString(30, 735, 'Reporte de Líneas Base')
    c.setFont('Helvetica', 10)
    c.drawString(30, 720, 'Fase: {}'.format(fase.nombre))
    c.drawString(30, 705, 'Cantidad de Líneas Base: {}'.format(len(lbs)))
    c.line(30, 690, 560, 690)
    c.setFont('Helvetica-Bold', 12)
    c.drawString(480, 750, str(datetime.now().date().__format__('%d/%m/%Y')))
    c.line(460, 747, 560, 747)
    pos = 660
    for lb in lbs:
        c.setFont('Helvetica-Bold', 12)
        c.drawString(30, pos, 'Nombre: {}'.format(lb.nombre))
        c.drawString(250, pos, 'Fecha de Creación: {}'.format(lb.fechaCreacion))
        c.drawString(470, pos, 'Estado: {}'.format(lb.estado))
        pos = pos - 15
        c.drawString(30, pos, 'Creador: {} {}'.format(lb.creador.first_name, lb.creador.last_name))
        c.setFont('Helvetica', 10)
        for item in lb.items.all():
            pos = pos - 15
            c.drawString(90, pos, '{}   {}   {}'.format(item.identificador, item.costo, item.descripcion))
        pos = pos - 30
    c.showPage()
    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response
