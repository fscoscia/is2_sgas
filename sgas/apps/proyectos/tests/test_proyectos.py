import pytest

import datetime
from sgas.apps.proyectos.models import Proyecto, Fase
from django.contrib.auth.models import User, Group
from sgas.apps.usuarios.models import Perfil
from sgas.apps.proyectos.views import asignarComite, desasignarComite, finalizarProyecto
from django.test import RequestFactory, Client
from django.contrib.messages.storage.fallback import FallbackStorage
from django.urls import reverse
from django.contrib import messages

#===Test Listar Proyectos===
@pytest.mark.django_db
def test_view_ProyectoList():
     #Simulación de inicio de sesión
    user = User.objects.create(username='testuser')
    user.set_password('12345')
    user.save()
    client = Client()
    client.login(username='testuser', password='12345')
    # Verifica que la vista responda correctamente a un pedido GET
    response = client.get('http://127.0.0.1:8000/proyectos/listar/')
    #Verifica respuesta con la vista
    assert response.status_code == 200

#===Test Crear Proyecto===
@pytest.mark.django_db
def test_view_ProyectoCreate():
    #Usuario Gerente para creación de Proyecto
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    usuario.save()
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    perfil.save()
    # Creación de un nuevo Proyecto
    proyecto = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=5, estado='Pendiente', gerente=perfil)

    #Verifica si creó un proyecto, en este caso si
    assert Proyecto.objects.count() == 1

@pytest.mark.django_db
def test_crear_comite():
    #Verifica la creración del comité de control de cambios
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=5, estado='Pendiente', gerente=perfil)
    comite = Group.objects.create(name='comite%s' % proyecto.id)

    assert Group.objects.count() == 1


@pytest.mark.django_db
def test_asignar_comite_proyecto():
    #Verifica la asignación del comité al proyecto
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    # Verifica la creacion de un nuevo Proyecto
    proyecto = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=5, estado='Pendiente', gerente=perfil)
    comite = Group.objects.create(name='comite%s' % proyecto.id)
    proyecto.comite = comite
    assert proyecto.comite.name == 'comite{}'.format(proyecto.id)


@pytest.mark.django_db
def test_asignar_comite_usuario():
    #Verifica la asignación de un usuario al comité de control de cambios
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    usuario.save()
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    perfil.save()
    # Verifica la creacion de un nuevo Proyecto
    proyecto = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=5, estado='Pendiente', gerente=perfil)
    comite = Group.objects.create(name='comite%s' % proyecto.id)
    proyecto.comite = comite
    comite.save()
    proyecto.save()
    path = (r'<int:id_proyecto>/comite/nuevo/<int:id_usuario>/')

    #Simulación de inicio de sesión
    user = User.objects.create(username='testuser')
    user.set_password('12345')
    user.save()
    client = Client()
    client.login(username='testuser', password='12345')
    request = client.post(path)

    #Necesario cuando en la vista se utiliza request.user
    request.user = usuario
    asignarComite(request, proyecto.id, usuario.id)
    assert usuario.groups.filter(name = comite.name).exists()


@pytest.mark.django_db
def test_desasignar_comite_usuario():
    #Verifica la asignación de un usuario al comité de control de cambios
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    usuario.save()
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    perfil.save()
    # Verifica la creacion de un nuevo Proyecto
    proyecto = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=5, estado='Pendiente', gerente=perfil)
    comite = Group.objects.create(name='comite%s' % proyecto.id)
    proyecto.comite = comite
    comite.save()
    proyecto.save()
    path1 = (r'<int:id_proyecto>/comite/nuevo/<int:id_usuario>/')

    #Simulación de inicio de sesión
    user = User.objects.create(username='testuser')
    user.set_password('12345')
    user.save()
    client = Client()
    client.login(username='testuser', password='12345')
    request = client.post(path1)

    #Necesario cuando en la vista se utiliza request.user
    request.user = usuario
    asignarComite(request, proyecto.id, usuario.id)

    path2= (r'<int:id_proyecto>/comite/eliminar/<int:id_usuario>/')
    request = client.post(path2)
    request.user = usuario
    desasignarComite(request, proyecto.id, usuario.id)
    assert not usuario.groups.filter(name = comite.name).exists()


@pytest.mark.django_db
def test_finalizar_proyecto():
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    usuario.save()
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    perfil.save()
    # Verifica la creacion de un nuevo Proyecto
    proyecto = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto, estado='Cerrada')
    fase2 = Fase.objects.create(nombre='Fase2Proy1', posicion=2, proyecto=proyecto, estado='Cerrada')
    comite = Group.objects.create(name='comite%s' % proyecto.id)
    fase1.save()
    fase2.save()
    proyecto.comite = comite
    comite.save()
    proyecto.save()

    path = (r'<int:id_proyecto>/finalizar/')
    #Simulación de inicio de sesión
    user = User.objects.create(username='testuser')
    user.set_password('12345')
    user.save()
    client = Client()
    client.login(username='testuser', password='12345')
    request = client.post(path)
    request.user = usuario
    setattr(request, 'session', 'session')
    m = FallbackStorage(request)
    setattr(request, '_messages', m)

    finalizarProyecto(request, proyecto.id)

    proyecto = Proyecto.objects.get(id=proyecto.id)
    assert proyecto.estado == 'Finalizado'


@pytest.mark.django_db
def test_finalizar_proyecto2():
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    usuario.save()
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    perfil.save()
    # Verifica la creacion de un nuevo Proyecto
    proyecto = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto)
    fase2 = Fase.objects.create(nombre='Fase2Proy1', posicion=2, proyecto=proyecto, estado='Cerrada')
    comite = Group.objects.create(name='comite%s' % proyecto.id)
    fase1.save()
    fase2.save()
    proyecto.comite = comite
    comite.save()
    proyecto.save()

    path = (r'<int:id_proyecto>/finalizar/')
    #Simulación de inicio de sesión
    user = User.objects.create(username='testuser')
    user.set_password('12345')
    user.save()
    client = Client()
    client.login(username='testuser', password='12345')
    request = client.post(path)
    request.user = usuario
    setattr(request, 'session', 'session')
    m = FallbackStorage(request)
    setattr(request, '_messages', m)

    finalizarProyecto(request, proyecto.id)

    proyecto = Proyecto.objects.get(id=proyecto.id)
    assert proyecto.estado == 'Finalizado', 'Todas las fases deben estar cerradas'
