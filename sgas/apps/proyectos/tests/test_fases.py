import pytest
import datetime
from sgas.apps.proyectos.models import Fase, Proyecto, LineaBase, SolicitudCambio
from django.contrib.auth.models import User
from sgas.apps.usuarios.models import Perfil
from sgas.apps.proyectos.forms import LBForm, SolicitudForm
from sgas.apps.items.models import Item
from sgas.apps.tipoitem.models import TipoItem
from sgas.apps.relaciones.models import Relacion
from sgas.apps.proyectos.views import cerrarLB, finalizarFase
from django.test import RequestFactory
from django.contrib.messages.storage.fallback import FallbackStorage
from django.contrib import messages


#===Test Crear Fases===
@pytest.mark.django_db
def test_createFases():
    usuario = User.objects.create_user('hugo', 'hugo@fleitas.com', 'hugofleitas')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    #Verifica que la vista guarde las Fases vinculadas a un proyecto
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    fase2 = Fase.objects.create(nombre='Fase2Proy1', posicion=2, proyecto=proyecto1)
    assert Fase.objects.count() == 2

#===Test Crear Línea Base===
@pytest.mark.django_db
def test_crearLB1():
    """
    Creación correcta de una línea base
    """
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    tipoItem = TipoItem.objects.create(nombre='RF', estado='Activo', descripcion='Requisito Funcional', proyecto=proyecto1, fase=fase1)
    item1 = Item.objects.create(fase= fase1, identificador= 'RF_1', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    item2 = Item.objects.create(fase= fase1, identificador= 'RF_2', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    nombre = 'LB_Fase1Proy1_1'
    data = {
        'nombre': nombre,
        'fase': fase1,
        'items': [item1, item2],
        'cantItems': '1',
        'creador': usuario,
    }
    form = LBForm(nombre=nombre, fase=fase1, data=data)
    #assert form.errors == {}, 'should be empty'
    assert form.is_valid() is True, form.errors

#===Test Crear Línea Base===
@pytest.mark.django_db
def test_crearLB2():
    """
    Creación incorrecta de una línea base: los ítems deben estar aprobados.
    """
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    fase2 = Fase.objects.create(nombre='Fase2Proy1', posicion=2, proyecto=proyecto1)
    tipoItem = TipoItem.objects.create(nombre='RF', estado='Activo', descripcion='Requisito Funcional', proyecto=proyecto1, fase=fase1)
    item1 = Item.objects.create(fase= fase1, identificador= 'RF_1', tipo= tipoItem, costo= 1, estado= 'Pendiente', descripcion= 'prueba')
    item2 = Item.objects.create(fase= fase1, identificador= 'RF_2', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    nombre = 'LB_Fase1Proy1_1'
    data = {
        'nombre': nombre,
        'fase': fase1,
        'items': [item1, item2],
        'cantItems': '1',
        'creador': usuario,
    }
    form = LBForm(nombre=nombre, fase=fase1, data=data)
    assert form.is_valid(), 'Los ítems deben estar aprobados'


#===Test Crear Línea Base===
@pytest.mark.django_db
def test_crearLB3():
    """
    Creación incorrecta de una línea base: los ítems deben pertenecer a la misma fase.
    """
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    fase2 = Fase.objects.create(nombre='Fase2Proy1', posicion=2, proyecto=proyecto1)
    tipoItem = TipoItem.objects.create(nombre='RF', estado='Activo', descripcion='Requisito Funcional', proyecto=proyecto1, fase=fase1)
    item1 = Item.objects.create(fase= fase1, identificador= 'RF_1', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    item2 = Item.objects.create(fase= fase2, identificador= 'RF_2', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    nombre = 'LB_Fase1Proy1_1'
    data = {
        'nombre': nombre,
        'fase': fase1,
        'items': [item1, item2],
        'cantItems': '1',
        'creador': usuario,
    }
    form = LBForm(nombre=nombre, fase=fase1, data=data)
    assert form.is_valid(), 'Los ítems deben pertenecer a la misma fase'


#===Test Crear Solicitud de Cambio===
@pytest.mark.django_db
def test_crearSolicitud():
    """
    Creación correcta de una solicitud de cambio.
    """
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    tipoItem = TipoItem.objects.create(nombre='RF', estado='Activo', descripcion='Requisito Funcional', proyecto=proyecto1, fase=fase1)
    item1 = Item.objects.create(fase= fase1, identificador= 'RF_1', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    item2 = Item.objects.create(fase= fase1, identificador= 'RF_2', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    item3 = Item.objects.create(fase= fase1, identificador= 'RF_3', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    lb = LineaBase.objects.create(nombre='LB_Fase1Proy1_1', fase=fase1, cantItems='2', creador=usuario)
    lb.items.set([item1, item2])
    data = {
        'nombre': 'Solicitud1',
        'lb': lb,
        'items': [item1, item2],
        'motivo': 'Motivo de prueba',
        'responsable': usuario,
    }
    form = SolicitudForm(nombre='Solicitud1', lb=lb, data=data)
    assert form.is_valid() is True, form.errors


#===Test Crear Solicitud de Cambio===
@pytest.mark.django_db
def test_crearSolicitud2():
    """
    Creación incorrecta de una solicitud de cambio: Los ítems deben pertenecer a la misma línea base.
    """
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    tipoItem = TipoItem.objects.create(nombre='RF', estado='Activo', descripcion='Requisito Funcional', proyecto=proyecto1, fase=fase1)
    item1 = Item.objects.create(fase= fase1, identificador= 'RF_1', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    item2 = Item.objects.create(fase= fase1, identificador= 'RF_2', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    item3 = Item.objects.create(fase= fase1, identificador= 'RF_3', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    lb = LineaBase.objects.create(nombre='LB_Fase1Proy1_1', fase=fase1, cantItems='2', creador=usuario)
    lb.items.set([item1, item2])
    data = {
        'nombre': 'Solicitud1',
        'lb': lb,
        'items': [item1, item2, item3],
        'motivo': 'Motivo de prueba',
        'responsable': usuario,
    }
    form = SolicitudForm(nombre='Solicitud1', lb=lb, data=data)
    assert form.is_valid() is True, 'Los ítems deben pertenecer a la misma Línea Base'


#===Test Cerrar Línea Base===
@pytest.mark.django_db
def test_cerrarLB():
    """
    Testeo de cierre de una LB correcta
    """
    #Creación correcta de una línea base
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    tipoItem = TipoItem.objects.create(nombre='RF', estado='Activo', descripcion='Requisito Funcional', proyecto=proyecto1, fase=fase1)
    item1 = Item.objects.create(fase= fase1, identificador= 'RF_1', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    item2 = Item.objects.create(fase= fase1, identificador= 'RF_2', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    nombre = 'LB_Fase1Proy1_1'
    data = {
        'nombre': nombre,
        'fase': fase1,
        'items': [item1, item2],
        'cantItems': '1',
        'creador': usuario,
    }
    form = LBForm(nombre=nombre, fase=fase1, data=data)
    form.save(nombre=nombre, fase=fase1, cantItems=2, creador=usuario, items=[item1, item2])

    #Cierre de una Línea Base
    path = (r'<int:id_proyecto>/fases/<int:id_fase>/crearLB/')
    request1 = RequestFactory().get(path)
    request1.user = usuario
    #MILAGRO
    setattr(request1, 'session', 'session')
    messages = FallbackStorage(request1)
    setattr(request1, '_messages', messages)

    lb1 = LineaBase.objects.get(id=form.instance.id)
    cerrarLB(request1, proyecto1.id, fase1.id, lb1.id)
    lb = LineaBase.objects.get(id=form.instance.id)
    assert lb.estado == "Cerrada"

#===Test Finalizar Fase===
@pytest.mark.django_db
def test_finalizarFase():
    """
    Cierre correcto de una Fase de un Proyecto
    """
    #Creación de todo lo necesario para la prueba
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    tipoItem = TipoItem.objects.create(nombre='RF', estado='Activo', descripcion='Requisito Funcional', proyecto=proyecto1, fase=fase1)
    item1 = Item.objects.create(fase= fase1, identificador= 'RF_1', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    item2 = Item.objects.create(fase= fase1, identificador= 'RF_2', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    R = Relacion.objects.create(itemA=item1, itemB=item2, tipo=2)
    R.save()
    nombre = 'LB_Fase1Proy1_1'
    data = {
        'nombre': nombre,
        'fase': fase1,
        'items': [item1, item2],
        'cantItems': '1',
        'creador': usuario,
    }
    form = LBForm(nombre=nombre, fase=fase1, data=data)
    form.save(nombre=nombre, fase=fase1, cantItems=2, creador=usuario, items=[item1, item2])
    item1.estado = "En LB"
    item2.estado = "En LB"
    item1.save()
    item2.save()
    lb = LineaBase.objects.get(id=form.instance.id)
    lb.estado = "Cerrada"
    lb.save()

    #Cierre de la Fase
    path = (r'<int:id_proyecto>/fases/<int:id_fase>/cerrar/')
    request1 = RequestFactory().get(path)
    request1.user = usuario

    setattr(request1, 'session', 'session')
    messages = FallbackStorage(request1)
    setattr(request1, '_messages', messages)

    finalizarFase(request1, proyecto1.id, fase1.id)
    fase = Fase.objects.get(id=fase1.id)
    assert fase.estado == "Cerrada", print(messages)

#===Test Finalizar Fase 2===
@pytest.mark.django_db
def test_finalizarFase2():
    """
    Cierre correcto de una Fase de un Proyecto
    """
    #Creación de todo lo necesario para la prueba
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(),
                                       numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    tipoItem = TipoItem.objects.create(nombre='RF', estado='Activo', descripcion='Requisito Funcional', proyecto=proyecto1, fase=fase1)
    item1 = Item.objects.create(fase= fase1, identificador= 'RF_1', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    item2 = Item.objects.create(fase= fase1, identificador= 'RF_2', tipo= tipoItem, costo= 1, estado= 'Aprobado', descripcion= 'prueba')
    R = Relacion.objects.create(itemA=item1, itemB=item2, tipo=2)
    R.save()
    nombre = 'LB_Fase1Proy1_1'
    data = {
        'nombre': nombre,
        'fase': fase1,
        'items': [item1, item2],
        'cantItems': '1',
        'creador': usuario,
    }
    form = LBForm(nombre=nombre, fase=fase1, data=data)
    form.save(nombre=nombre, fase=fase1, cantItems=2, creador=usuario, items=[item1, item2])
    item1.estado = "En LB"
    item2.estado = "En LB"
    item1.save()
    item2.save()

    #Cierre de la Fase
    path = (r'<int:id_proyecto>/fases/<int:id_fase>/cerrar/')
    request1 = RequestFactory().get(path)

    setattr(request1, 'session', 'session')
    messages = FallbackStorage(request1)
    setattr(request1, '_messages', messages)

    finalizarFase(request1, proyecto1.id, fase1.id)
    fase = Fase.objects.get(id=fase1.id)
    assert fase.estado == "Cerrada", "Todas las Líneas Base deben estar cerradas"
