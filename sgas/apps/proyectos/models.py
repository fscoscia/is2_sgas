from django.db import models
from django.core.validators import MinValueValidator
from sgas.apps.usuarios.models import Perfil
from django.contrib.auth.models import Group
from sgas.apps.items.models import Item
from django.contrib.auth.models import User

ESTADOLB_CHOICES = [
    ("Abierta", "Abierta"),
    ("Rota", "Rota"),
    ("Cerrada", "Cerrada"),
    ("Comprometida", "Comprometida")
]

ESTADO_SOLICITUD_CHOICES = [
    ("Pendiente", "Pendiente"),
    ("Aprobada", "Aprobada"),
    ("Completada", "Completada"),
    ("Desaprobada", "Desaprobada")
]


# Create your models here.
class Proyecto(models.Model):
    nombre = models.CharField(max_length=150, blank=False)
    descripcion = models.TextField(max_length=400, blank=False)
    fechaCreacion = models.DateField(auto_now_add=True)
    fechaInicio = models.DateField(null=True)
    fechaFin = models.DateField(null=True)
    numFases = models.IntegerField(default=1, validators=[MinValueValidator(1)])
    estado = models.CharField(default='Pendiente', max_length=10)
    gerente = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    comite = models.OneToOneField(Group, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return '{}'.format(self.nombre)


class Fase(models.Model):
    nombre = models.CharField(max_length=50, blank=False)
    posicion = models.IntegerField(blank=False)
    numItems = models.IntegerField(default=0)
    numLB = models.IntegerField(default=0)
    estado = models.CharField(max_length=7, default='Abierta')
    proyecto = models.ForeignKey(Proyecto, null=False, blank=False, on_delete=models.CASCADE)


    def __str__(self):
        return '{}'.format(self.nombre)

class LineaBase(models.Model):
    nombre = models.CharField(max_length=80, null=True)
    fechaCreacion = models.DateField(auto_now_add=True)
    estado = models.CharField(max_length=80, choices=ESTADOLB_CHOICES, default="Abierta")
    cantItems = models.IntegerField(default=0)
    items = models.ManyToManyField(to=Item)
    creador = models.ForeignKey(to=User, null=False, blank=False, on_delete=models.CASCADE)
    fase = models.ForeignKey(to=Fase, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return '{}'.format(self.nombre)

class Historial(models.Model):

    fecha = models.DateTimeField(auto_now_add=True)
    operacion = models.CharField(max_length=150)
    autor = models.CharField(max_length=80, null=True)
    categoria = models.CharField(max_length=80)
    proyecto = models.ForeignKey(Proyecto, null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}: {}'.format(self.fecha.strftime("%d/%m/%Y %X"), self.autor, self.operacion)


class SolicitudCambio(models.Model):
    nombre = models.CharField(max_length=80, null=True)
    items = models.ManyToManyField(to=Item)
    estado = models.CharField(max_length=15, choices=ESTADO_SOLICITUD_CHOICES, default='Pendiente')
    responsable = models.ForeignKey(to=User, null=False, blank=False, on_delete=models.CASCADE)
    aFavor = models.IntegerField(default=0)
    enContra = models.IntegerField(default=0)
    motivo = models.TextField()
    lb = models.ForeignKey(to=LineaBase, on_delete=models.CASCADE, null=False)
    fecha = models.DateField(auto_now=True)
    hora = models.TimeField(auto_now=True)


class VotoSolicitud(models.Model):
    solicitud = models.ForeignKey(to=SolicitudCambio, on_delete=models.CASCADE, null=False)
    aFavor = models.BooleanField(default=False)
    enContra = models.BooleanField(default=False)
    usuario = models.ForeignKey(to=User, on_delete=models.CASCADE, null=False)
