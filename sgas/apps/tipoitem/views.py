from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, UpdateView, ListView
from sgas.apps.tipoitem.models import TipoItem, AdicionalNumero, AdicionalArchivo, AdicionalFecha, AdicionalCadena
from sgas.apps.tipoitem.forms import TipoItemForm, AdicionalNumeroFormset, AdicionalCadenaFormset, AdicionalFechaFormset, AdicionalArchivoFormset
from sgas.apps.proyectos.models import Proyecto, Historial
from sgas.apps.usuarios.models import Perfil
from django.contrib.auth.models import User
from django.db.models import Q


# ===Vista CrearTipoItem===
@login_required
def CrearTipoItem(request, idProyecto):
    """
    Vista basada en funciones.
    Recibe un request que puede ser POST o GET.
    Si el request es GET, renderiza los forms para Tipo de Item y sus posibles atributos adicionales.
    Si el request es POST, verifica que atributos adicionales posee el Tipo de Item y luego guarda los campos del Tipo de Item y de sus atributos adicionales.
    """
    proyecto = Proyecto.objects.get(id=idProyecto)
    template_name = 'tipoitem/tipoitem_form.html'
    if request.method == 'GET':
        tipoitemform = TipoItemForm(request.GET, id=idProyecto)
        formset1 = AdicionalNumeroFormset(queryset=AdicionalNumero.objects.none(), prefix='form1')
        formset2 = AdicionalCadenaFormset(queryset=AdicionalCadena.objects.none(), prefix='form2')
        formset3 = AdicionalFechaFormset(queryset=AdicionalFecha.objects.none(), prefix='form3')
        formset4 = AdicionalArchivoFormset(queryset=AdicionalArchivo.objects.none(), prefix='form4')
    elif request.method == 'POST':
        tipoitemform = TipoItemForm(request.POST, id=idProyecto)
        vacio1 = True
        vacio2 = True
        vacio3 = True
        vacio4 = True
        formset1 = AdicionalNumeroFormset(request.POST, prefix='form1')
        formset2 = AdicionalCadenaFormset(request.POST, prefix='form2')
        formset3 = AdicionalFechaFormset(request.POST, prefix='form3')
        formset4 = AdicionalArchivoFormset(request.POST, prefix='form4')
        i = 0
        for form1 in formset1:
            field = 'form1-{}-nombre'.format(i)
            nombre = form1.data[field]
            if nombre != '':
                vacio1 = False
            i += 1
        i = 0
        for form2 in formset2:
            field = 'form2-{}-nombre'.format(i)
            nombre = form2.data[field]
            if nombre != '':
                vacio2 = False
            i += 1
        i = 0
        for form3 in formset3:
            field = 'form3-{}-nombre'.format(i)
            nombre = form3.data[field]
            if nombre != '':
                vacio3 = False
            i += 1
        i = 0
        for form4 in formset4:
            field = 'form4-{}-nombre'.format(i)
            nombre = form4.data[field]
            if nombre != '':
                vacio4 = False
            i += 1
        if tipoitemform.is_valid() and (formset1.is_valid() or vacio1) and (formset2.is_valid() or vacio2) and (formset3.is_valid() or vacio3) and (formset4.is_valid() or vacio4):
            tipoitem = tipoitemform.save(proyecto=proyecto)
            if not vacio1:
                for form1 in formset1:
                    adicional = form1.save(commit=False)
                    adicional.tipo = tipoitem
                    adicional.save()
            if not vacio2:
                for form2 in formset2:
                    adicional = form2.save(commit=False)
                    adicional.tipo = tipoitem
                    adicional.save()
            if not vacio3:
                for form3 in formset3:
                    adicional = form3.save(commit=False)
                    adicional.tipo = tipoitem
                    adicional.save()
            if not vacio4:
                for form4 in formset4:
                    adicional = form4.save(commit=False)
                    adicional.tipo = tipoitem
                    adicional.save()
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Crear Tipo de Item {}'.format(tipoitem.nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Items")
            return redirect('tipoitem:listar_tipoitem', idProyecto=idProyecto)
    return render(request, template_name,
                  {'tipoitemform': tipoitemform,
                   'formset1': formset1,
                   'formset2': formset2,
                   'formset3': formset3,
                   'formset4': formset4,
                   'idProyecto': idProyecto}
                   )


# ===Vista ListarTipoItem===
class ListarTipoItem(LoginRequiredMixin, ListView):
    """
    Vista basada en modelos.
    Permite listar todos los tipos de items creados, mostrando los nombres de sus atributos adicionales.
    """
    redirect_field_name = 'redirect_to'
    model = TipoItem
    template_name = 'tipoitem/listar_tipoitem.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ListarTipoItem, self).get_context_data()
        context['idProyecto'] = self.kwargs['idProyecto']
        return context

    def get_queryset(self):
        return TipoItem.objects.filter(proyecto=self.kwargs['idProyecto']).order_by('id')


#===Lista de Tipos de Ítem para Importar===
@login_required
def listaImportar(request, idProyecto):
    """
    Muestra la lista de tipos de ítems que no pertenecen al proyecto en el que se está trabajando
    Recibe la petición http y el id del proyecto
    Renderiza la información solicitada en el documento html especificado.
    """
    control = TipoItem.objects.filter(proyecto=idProyecto)
    tipos = TipoItem.objects.filter(~Q(proyecto=idProyecto))
    if len(control) > 0:
        for x in range(0, len(control)):
            if tipos.filter(nombre=control[x].nombre):
                tipos = tipos.exclude(nombre=control[x].nombre)
    return render(request, 'tipoitem/listar_importar.html', {'tipos':tipos, 'idProyecto':idProyecto})


#===Importar Tipo de Ítem
@login_required
def importarTipoItem(request, idProyecto, idTipo):
    """
    Función para importar un tipo de ítem.
    Recibe la petición http, el id del proyecto actual y el id del tipo de ítem a importar.
    Crea una nueva instancia del tipo de ítem en el proyecto actual.
    Redirige a la lista de tipos de ítem del proyecto.
    """
    tipo = TipoItem.objects.get(id=idTipo)
    nuevo = TipoItem.objects.create()
    nuevo.nombre = tipo.nombre
    nuevo.estado = 'Inactivo'
    nuevo.descripcion = tipo.descripcion
    nuevo.proyecto = Proyecto.objects.get(id=idProyecto)
    adicionalNum = AdicionalNumero.objects.filter(tipo=idTipo)
    if len(adicionalNum) > 0:
        for x in range(0, len(adicionalNum)):
            AdicionalNumero.objects.create(tipo=nuevo, nombre=adicionalNum[x].nombre, opcional=adicionalNum[x].opcional)
    adicionalCad = AdicionalCadena.objects.filter(tipo=idTipo)
    if len(adicionalCad) > 0:
        for x in range(0, len(adicionalCad)):
            AdicionalCadena.objects.create(tipo=nuevo, nombre=adicionalCad[x].nombre, opcional=adicionalCad[x].opcional)
    adicionalF = AdicionalFecha.objects.filter(tipo=idTipo)
    if len(adicionalF) > 0:
        for x in range(0, len(adicionalF)):
            AdicionalFecha.objects.create(tipo=nuevo, nombre=adicionalF[x].nombre, opcional=adicionalF[x].opcional)
    adicionalArc = AdicionalArchivo.objects.filter(tipo=idTipo)
    if len(adicionalArc) > 0:
        for x in range(0, len(adicionalArc)):
            AdicionalArchivo.objects.create(tipo=nuevo, nombre=adicionalArc[x].nombre, opcional=adicionalArc[x].opcional)
    nuevo.save()
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    Historial.objects.create(operacion='Importar Tipo de Item {}'.format(nuevo.nombre, ), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Items")
    return redirect('tipoitem:listar_tipoitem', idProyecto)


# ===Modificar Tipo de Ítem===
def modificarTipoItem(request, idProyecto, idTipo):
    """
    Modifica un tipo de ítem.
    Recibe la petición http, el id del proyecto y el id del tipo de ítem.
    Muestra la información del tipo de ítem especificado para su modificación y luego guarda
    los cambios realizados.
    """
    tipo = TipoItem.objects.get(id=idTipo)
    adicionalNum = AdicionalNumero.objects.filter(tipo=idTipo)
    adicionalCad = AdicionalCadena.objects.filter(tipo=idTipo)
    adicionalFecha = AdicionalFecha.objects.filter(tipo=idTipo)
    adicionalArc = AdicionalArchivo.objects.filter(tipo=idTipo)
    proyecto = Proyecto.objects.get(id=idProyecto)
    template_name = 'tipoitem/tipoitem_form.html'
    if request.method == 'GET':
        tipoitemform = TipoItemForm(instance=tipo, id=idProyecto)
        formset1 = AdicionalNumeroFormset(queryset=adicionalNum, prefix='form1')
        formset2 = AdicionalCadenaFormset(queryset=adicionalCad, prefix='form2')
        formset3 = AdicionalFechaFormset(queryset=adicionalFecha, prefix='form3')
        formset4 = AdicionalArchivoFormset(queryset=adicionalArc, prefix='form4')
        return render(request, template_name, {'tipoitemform': tipoitemform, 'formset1': formset1,
                'formset2': formset2, 'formset3': formset3, 'formset4': formset4, 'idProyecto': idProyecto})
    else:
        t = tipo
        t.nombre='aux_cambio_is2_123'
        t.save()
        tipoitemform = TipoItemForm(request.POST, instance=tipo, id=idProyecto)
        vacio1 = True
        vacio2 = True
        vacio3 = True
        vacio4 = True
        formset1 = AdicionalNumeroFormset(request.POST, prefix='form1')
        formset2 = AdicionalCadenaFormset(request.POST, prefix='form2')
        formset3 = AdicionalFechaFormset(request.POST, prefix='form3')
        formset4 = AdicionalArchivoFormset(request.POST, prefix='form4')
        i = 0
        if not len(adicionalNum) > 0:
            for form1 in formset1:
                field = 'form1-{}-nombre'.format(i)
                nombre = form1.data[field]
                if nombre != '':
                    vacio1 = False
                i += 1
            i = 0
        if not len(adicionalCad) > 0:
            for form2 in formset2:
                field = 'form2-{}-nombre'.format(i)
                nombre = form2.data[field]
                if nombre != '':
                    vacio2 = False
                i += 1
            i = 0
        if not len(adicionalFecha) > 0:
            for form3 in formset3:
                field = 'form3-{}-nombre'.format(i)
                nombre = form3.data[field]
                if nombre != '':
                    vacio3 = False
                i += 1
            i = 0
        if not len(adicionalArc) > 0:
            for form4 in formset4:
                field = 'form4-{}-nombre'.format(i)
                nombre = form4.data[field]
                if nombre != '':
                    vacio4 = False
                i += 1
        if tipoitemform.is_valid() and (formset1.is_valid() or vacio1) and (formset2.is_valid() or vacio2) and (formset3.is_valid() or vacio3) and (formset4.is_valid() or vacio4):
            tipoitem = tipoitemform.save(proyecto=proyecto)
            if not vacio1 or (len(adicionalNum) > 0):
                for form1 in formset1:
                    if form1.is_valid():
                        adicional = form1.save(commit=False)
                        adicional.tipo = tipoitem
                        adicional.save()
            if not vacio2 or (len(adicionalCad) > 0):
                for form2 in formset2:
                    if form2.is_valid():
                        adicional = form2.save(commit=False)
                        adicional.tipo = tipoitem
                        adicional.save()
            if not vacio3 or (len(adicionalFecha) > 0):
                for form3 in formset3:
                    if form3.is_valid():
                        adicional = form3.save(commit=False)
                        adicional.tipo = tipoitem
                        adicional.save()
            if not vacio4 or (len(adicionalArc) > 0):
                for form4 in formset4:
                    if form4.is_valid():
                        adicional = form4.save(commit=False)
                        adicional.tipo = tipoitem
                        adicional.save()
            user = User.objects.get(username=request.user)
            perfil = Perfil.objects.get(user=user)
            Historial.objects.create(operacion='Modificar Tipo de Item {}'.format(tipoitem.nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Items")
        return redirect('tipoitem:listar_tipoitem', idProyecto=idProyecto)

# ===Eliminar Tipo de Ítem===
def eliminarTipoItem(request, idProyecto, idTipo):
    """
    Función para eliminar un tipo de ítem.
    Recibe la petición http, el id del proyecto y el id del tipo de ítem.
    Redirige a la lista de tipos de ítem del proyecto.
    """
    tipo = TipoItem.objects.get(id=idTipo)
    nombre = tipo.nombre
    tipo.delete()
    user = User.objects.get(username=request.user)
    perfil = Perfil.objects.get(user=user)
    Historial.objects.create(operacion='Modificar Tipo de Item {}'.format(nombre), autor=perfil.__str__(), proyecto=Proyecto.objects.get(id=idProyecto), categoria="Items")
    return redirect('tipoitem:listar_tipoitem', idProyecto=idProyecto)
