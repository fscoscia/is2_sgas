import pytest
import datetime
from sgas.apps.proyectos.models import Proyecto, Fase
from sgas.apps.tipoitem.forms import TipoItemForm
from sgas.apps.usuarios.models import User, Perfil
from sgas.apps.tipoitem.models import TipoItem, AdicionalNumero, AdicionalCadena, AdicionalFecha, AdicionalArchivo

@pytest.mark.django_db
def test_crearTipoItem():
    #Verifica la creación de un tipo de item
    usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
    perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
    proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(), numFases=2, estado='Pendiente', gerente=perfil)
    fase1 = Fase.objects.create(nombre='Fase1Proy1', posicion=1, proyecto=proyecto1)
    data = {
        'nombre': 'Test',
        'descripcion': 'Descripcion',
        'proyecto': proyecto1,
        'fase': fase1,
    }
    form = TipoItemForm(id=proyecto1.id, data=data)
    assert form.is_valid() is True, form.errors

@pytest.mark.django_db
def test_eliminarTipoItem():
    #Verifica la eliminación de un TipoItem
    tipo = TipoItem.objects.create(nombre='Tipo',descripcion='Descripcion')
    tipo.delete()
    assert TipoItem.objects.count() == 0

@pytest.mark.django_db
def test_eliminarAtributoAdicionalNumero():
    #Verifica la eliminación de un atributo adicional asociado a un tipo de item
    tipo = TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion')
    atributo = AdicionalNumero.objects.create(nombre='Campo', tipo=tipo)
    atributo.delete()
    assert AdicionalNumero.objects.count() == 0

@pytest.mark.django_db
def test_crearAtributoAdicionalNumero():
    #Verifica la creacion de un atributo adicional numerico asociado a un tipo de item
    tipo = TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion')
    atributo = AdicionalNumero.objects.create(nombre='Campo', tipo=tipo)
    assert AdicionalNumero.objects.count() == 1

@pytest.mark.django_db
def test_eliminarAtributoAdicionalCadena():
    #Verifica la eliminación de un atributo adicional cadena asociado a un tipo de item
    tipo = TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion')
    atributo = AdicionalCadena.objects.create(nombre='Campo', tipo=tipo)
    atributo.delete()
    assert AdicionalCadena.objects.count() == 0

@pytest.mark.django_db
def test_crearAtributoAdicionalCadena():
    #Verifica la creacion de un atributo adicional cadena asociado a un tipo de item
    tipo = TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion')
    atributo = AdicionalCadena.objects.create(nombre='Campo', tipo=tipo)
    assert AdicionalCadena.objects.count() == 1

@pytest.mark.django_db
def test_eliminarAtributoAdicionalFecha():
    #Verifica la eliminación de un atributo adicional fecha asociado a un tipo de item
    tipo = TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion')
    atributo = AdicionalFecha.objects.create(nombre='Campo', tipo=tipo)
    atributo.delete()
    assert AdicionalFecha.objects.count() == 0

@pytest.mark.django_db
def test_crearAtributoAdicionalFecha():
    #Verifica la creacion de un atributo adicional fecha asociado a un tipo de item
    tipo = TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion')
    atributo = AdicionalFecha.objects.create(nombre='Campo', tipo=tipo)
    assert AdicionalFecha.objects.count() == 1

@pytest.mark.django_db
def test_eliminarAtributoAdicionalArchivo():
    #Verifica la eliminación de un atributo adicional archivo asociado a un tipo de item
    tipo = TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion')
    atributo = AdicionalArchivo.objects.create(nombre='Campo', tipo=tipo)
    atributo.delete()
    assert AdicionalArchivo.objects.count() == 0

@pytest.mark.django_db
def test_crearAtributoAdicionalArchivo():
    #Verifica la creacion de un atributo adicional archivo asociado a un tipo de item
    tipo = TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion')
    atributo = AdicionalArchivo.objects.create(nombre='Campo', tipo=tipo)
    assert AdicionalArchivo.objects.count() == 1

@pytest.mark.django_db
def test_eliminarTipoItemError():
    #Verifica que no se puede eliminar un tipo de item que no existe
    tipo = TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion')
    try:
        tipo.delete()
        tipo.delete()
    except:
        assert False, 'No se puede eliminar un tipo de item que no existe'

@pytest.mark.django_db
def test_crearTipoItemError():
    #Verifica que no se puedan crear tipos de item con el mismo nombre
    try:
        usuario = User.objects.create_user('Juan', 'juan@perez.com', 'juanperez')
        perfil = Perfil.objects.create(ci=1111111, telefono=121212, user=usuario)
        proyecto1 = Proyecto.objects.create(nombre='Proyecto1', descripcion='Descrip1', fechaCreacion=datetime.date.today(), numFases=2, estado='Pendiente', gerente=perfil)
        TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion', proyecto=proyecto1)
        TipoItem.objects.create(nombre='Tipo', descripcion='Descripcion', proyecto=proyecto1)
    except:
        assert False, 'No se permiten tipos de item con el mismo nombre en un mismo proyecto'


# @pytest.mark.django_db
# def test_view_TipoItem_Listar(client):
#     #Verifica que la vista responda correctamente a un pedido de POST
#     response = client.get('http://127.0.0.1:8000/tipoitem/')
#     assert response.status_code == 200

