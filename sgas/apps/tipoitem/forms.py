from django import forms
from django.forms import modelformset_factory
from sgas.apps.tipoitem.models import TipoItem, AdicionalNumero, AdicionalCadena, AdicionalFecha, AdicionalArchivo
from sgas.apps.proyectos.models import Proyecto, Fase

class TipoItemForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        id = kwargs.pop('id')
        proyecto = Proyecto.objects.get(id=id)
        self.proyecto = proyecto
        choices = Fase.objects.filter(proyecto=proyecto)
        super(TipoItemForm, self).__init__(*args, **kwargs)
        self.fields['proyecto'].required = False
        self.fields['fase'].queryset = choices.order_by('id')
        self.fields['fase'].required = False

    def save(self, commit=True, *args, **kwargs):
        proyecto = kwargs.get('proyecto')
        instance = super(TipoItemForm, self).save(commit=False)
        instance.proyecto = proyecto
        if commit:
            instance.save()
        return instance


    def clean(self, *args, **kwargs):
        cleaned_data = self.cleaned_data
        nombre = cleaned_data.get('nombre')
        proyecto = self.proyecto
        if TipoItem.objects.filter(nombre=nombre, proyecto=proyecto).count() > 0:
            raise forms.ValidationError('Ya existe un tipo de ítem con ese nombre en este proyecto')
        return cleaned_data


    class Meta:
        model = TipoItem
        fields = ['nombre','descripcion','proyecto','fase']
        labels = {'nombre': 'Nombre','descripcion': 'Descripcion',}
        widgets = {
            'nombre': forms.TextInput(attrs={"class":"form-control", "placeholder":"Ingrese el nombre del Tipo de Item"}),
            'descripcion': forms.TextInput(attrs={"class": "form-control", "placeholder": "Ingrese una descripcion del Tipo de Item"}),
        }

AdicionalNumeroFormset = modelformset_factory(
    AdicionalNumero,
    fields=('nombre','opcional',),
    extra=1,
    widgets={
        'nombre': forms.TextInput(attrs={
        "class": "form-control",
        "style": "max-width: 50%",
        "placeholder": "Ingrese nombre del atributo adicional",}, ),
        'opcional': forms.CheckboxInput()
    }
)

AdicionalCadenaFormset = modelformset_factory(
    AdicionalCadena,
    fields=('nombre','opcional'),
    extra=1,
    widgets={
        'nombre': forms.TextInput(attrs={
        "class": "form-control",
        "style": "max-width: 50%",
        "placeholder": "Ingrese nombre del atributo adicional",}),
        'opcional': forms.CheckboxInput()
    }
)

AdicionalFechaFormset = modelformset_factory(
    AdicionalFecha,
    fields=('nombre','opcional'),
    extra=1,
    widgets={
        'nombre': forms.TextInput(attrs={
        "class": "form-control",
        "style": "max-width: 50%",
        "placeholder": "Ingrese nombre del atributo adicional",}),
        'opcional': forms.CheckboxInput()
    }
)

AdicionalArchivoFormset = modelformset_factory(
    AdicionalArchivo,
    fields=('nombre','opcional'),
    extra=1,
    widgets={
        'nombre': forms.TextInput(attrs={
        "class": "form-control",
        "style": "max-width: 50%",
        "placeholder": "Ingrese nombre del atributo adicional",}),
        'opcional': forms.CheckboxInput()
    }
)
