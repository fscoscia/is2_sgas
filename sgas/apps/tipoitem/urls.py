from django.urls import path
from sgas.apps.tipoitem.views import CrearTipoItem, ListarTipoItem, listaImportar, importarTipoItem, \
    modificarTipoItem, eliminarTipoItem

urlpatterns = [
    path(r'nuevo/', CrearTipoItem, name='crear_tipoitem'),
    path(r'modificar/<int:idTipo>', modificarTipoItem, name='modificar_tipoitem'),
    path(r'eliminar/<int:idTipo>', eliminarTipoItem, name='eliminar_tipoitem'),
    path(r'', ListarTipoItem.as_view(), name='listar_tipoitem'),
    path(r'importar/', listaImportar, name='listar_importar'),
    path(r'importar/<int:idTipo>', importarTipoItem, name='importar'),

]
