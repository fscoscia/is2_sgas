from django.db import models
from sgas.apps.proyectos.models import Fase

# Posibles estados de un Tipo de Item
ESTADO_CHOICES = [
    ("Activo", "Activo"),
    ("Inactivo", "Inactivo"),
]

# Definicion del modelo Tipo de Item
# Posee los atributos nombre, estado
#
class TipoItem(models.Model):
    nombre = models.CharField(max_length=80)
    estado = models.CharField(max_length=20, null=False, blank=False, choices=ESTADO_CHOICES, default='Inactivo')
    descripcion = models.CharField(max_length=160)
    numItems = models.IntegerField(default=0)
    proyecto = models.ForeignKey(to='proyectos.Proyecto', on_delete=models.CASCADE, null=True)
    fase = models.ForeignKey(to='proyectos.Fase', on_delete=models.CASCADE, null=True)

    class Meta:

        unique_together = ['nombre', 'proyecto']

class AdicionalNumero(models.Model):
    tipo = models.ForeignKey(TipoItem, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=80, null=False, blank=False)
    opcional = models.BooleanField(null=True, blank=True)

class AdicionalCadena(models.Model):
    tipo = models.ForeignKey(TipoItem, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=80, null=False, blank=False)
    opcional = models.BooleanField(null=True, blank=True)

class AdicionalFecha(models.Model):
    tipo = models.ForeignKey(TipoItem, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=80, null=False, blank=False)
    opcional = models.BooleanField(null=True, blank=True)

class AdicionalArchivo(models.Model):
    tipo = models.ForeignKey(TipoItem, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=80, null=False, blank=False)
    opcional = models.BooleanField(null=True, blank=True)
