
from django.contrib import admin
from django.urls import path, include

urlpatterns = [

    #En este apartado se listan las urls predefinidas para el
    #autenticador de Google y urls del SGAS.
    #
    #1. **URL** admin
    #2. **URL** cuentas
    #3. **URL** de proyectos
    #4. **URL** de usuarios


    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('', include(('sgas.apps.proyectos.urls', 'proyectos'), namespace='proyectos')),
    path('usuarios/', include(('sgas.apps.usuarios.urls', 'usuarios'), namespace='usuarios') ),
]
