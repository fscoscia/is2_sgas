from django.contrib.auth.models import User, Permission
from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView
from sgas.apps.usuarios.models import Perfil
from sgas.apps.usuarios.forms import PerfilForm, UsuarioForm


def home(request):
    return render(request, 'home.html')


class CrearPerfil(CreateView):
    model = Perfil
    form_class = PerfilForm
    template_name = 'usuarios/usuario_form.html'
    success_url = reverse_lazy('usuarios:home')

    def form_valid(self, form):
        user = User.objects.get(username = self.request.user)
        Perfil.objects.create(ci=self.request.POST['ci'], telefono=self.request.POST['telefono'], user= user)
        return redirect(self.success_url)


class ListarPerfil(ListView):
    model = Perfil
    template_name = 'usuarios/listar_perfiles.html'


def editarPerfil(request, id_perfil):
    perfil = Perfil.objects.get(id=id_perfil)
    usuario = User.objects.get(id=perfil.user.id)
    if request.method == 'GET':
        formP = PerfilForm(instance=perfil)
        formU = UsuarioForm(instance=usuario)
    else:
        formP = PerfilForm(request.POST, instance=perfil)
        formU = UsuarioForm(request.POST, instance=usuario)
        if all([formP.is_valid(), formU.is_valid()]):
            formP.save()
            formU.save()
        return redirect('usuarios:listar_perfiles')
    return render(request, 'usuarios/editar_perfil.html', {'formP': formP, 'formU': formU})


def listaAcceso(request):
    perm = Permission.objects.get(codename='acceso_usuario')
    usuario = User.objects.filter(~Q(user_permissions=perm))
    perfiles = Perfil.objects.filter(Q(user=usuario[0]))
    for x in range(1, len(usuario)):
        perfiles |= Perfil.objects.filter(Q(user=usuario[x]))
    contexto = {'perfiles':perfiles}
    return render(request, 'usuarios/usuario_acceso.html', contexto)

def concederAcceso(request, id_perfil):
    perfil = Perfil.objects.get(id=id_perfil)
    usuario = User.objects.get(id=perfil.user.id)
    perm = Permission.objects.get(codename='acceso_usuario')
    usuario.user_permissions.add(perm)
    return redirect('usuarios:lista_acceso')

def eliminarPerfil(request, id_perfil):
    perfil = Perfil.objects.get(id=id_perfil)
    if request.method == 'POST':
        usuario = User.objects.get(id=perfil.user.id)
        usuario.delete()
        perfil.delete()
        return (redirect('usuarios:listar_perfiles'))
    return render(request, 'usuarios/eliminar_perfil.html', {'perfil':perfil})

