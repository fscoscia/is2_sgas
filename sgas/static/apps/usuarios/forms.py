from django import forms
from django.contrib.auth.models import User
from sgas.apps.usuarios.models import Perfil

class PerfilForm(forms.ModelForm):

    class Meta:
        model = Perfil
        fields = ['ci','telefono',]
        labels = {'ci': 'CI','telefono': 'Teléfono',}
        widgets = {
            'ci': forms.TextInput(attrs={"class":"form-control", "placeholder":"Ingrese su número de CI"}),
            'telefono': forms.TextInput(attrs={"class":"form-control", "placeholder":"Ingrese su número de teléfono"}),
        }


class UsuarioForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['first_name','last_name', 'email',]
        labels = {'first_name': 'Nombre','last_name': 'Apellido', 'email':'Email',}
        widgets = {
            'first_name': forms.TextInput(attrs={"class":"form-control"}),
            'last_name': forms.TextInput(attrs={"class":"form-control"}),
            'email': forms.TextInput(attrs={"class":"form-control","readonly":"readonly"}),
        }
