from django.urls import path, include
from sgas.apps.usuarios.views import home, CrearPerfil, ListarPerfil, editarPerfil, listaAcceso, concederAcceso, eliminarPerfil
urlpatterns = [
    path(r'home/', home, name='home'),
    path(r'nuevo/', CrearPerfil.as_view(), name='crear_perfil'),
    path(r'listar/', ListarPerfil.as_view(), name='listar_perfiles'),
    path(r'acceso/', listaAcceso, name='lista_acceso'),
    path(r'acceso/<int:id_perfil>', concederAcceso, name='conceder_acceso'),
    path(r'editar/<int:id_perfil>/', editarPerfil, name='editar_perfil'),
    path(r'eliminar/<int:id_perfil>/', eliminarPerfil, name='eliminar_perfil'),
]
