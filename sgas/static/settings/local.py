# local.py
from .base import *
DEBUG = True
ALLOWED_HOSTS = ['testserver','127.0.0.1','localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'desarrollo',
        'USER': 'usuario_desarrollo',
        'PASSWORD': 'desarrollo',
        'HOST': 'localhost',
        'PORT': '',
    }
}

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_DIRS =(os.path.join(BASE_DIR),'sgas')

#Prueba fetch
