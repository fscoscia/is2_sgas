# local.py
from .base import *
DEBUG = True
ALLOWED_HOSTS = ['djangoproject.localhost', '127.0.0.1']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'produccion',
        'USER': 'usuario_produccion',
        'PASSWORD': 'produccion',
        'HOST': 'localhost',
        'PORT': '',
    }
}

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static/")
