import pytest
from sgas.apps.items.models import Item, CampoNumero, CampoCadena, CampoArchivo, CampoFecha

@pytest.mark.django_db
def test_crearItem():
    #Verifica la creación de un item
    Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    assert Item.objects.count() == 1

@pytest.mark.django_db
def test_crearCampoNumero():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    CampoNumero.objects.create(item=item, nombre='Numero1', numero=1, opcional=True)
    assert CampoNumero.objects.count() == 1

@pytest.mark.django_db
def test_crearCampoCadena():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    CampoCadena.objects.create(item=item, nombre='Numero1', cadena=1, opcional=True)
    assert CampoCadena.objects.count() == 1

@pytest.mark.django_db
def test_crearCampoFecha():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    CampoFecha.objects.create(item=item, nombre='Numero1', opcional=True)
    assert CampoFecha.objects.count() == 1

@pytest.mark.django_db
def test_crearCampoArchivo():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    CampoArchivo.objects.create(item=item, nombre='Numero1', opcional=True)
    assert CampoArchivo.objects.count() == 1

@pytest.mark.django_db
def test_eliminarCampoNumero():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    campo = CampoNumero.objects.create(item=item, nombre='Numero1', numero=1, opcional=True)
    campo.delete()
    assert CampoNumero.objects.count() == 0

@pytest.mark.django_db
def test_eliminarCampoCadena():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    campo = CampoCadena.objects.create(item=item, nombre='Numero1', cadena=1, opcional=True)
    campo.delete()
    assert CampoCadena.objects.count() == 0

@pytest.mark.django_db
def test_eliminarCampoFecha():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    campo = CampoFecha.objects.create(item=item, nombre='Numero1', opcional=True)
    campo.delete()
    assert CampoNumero.objects.count() == 0

@pytest.mark.django_db
def test_eliminarCampoArchivo():
    item = Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    campo = CampoArchivo.objects.create(item=item, nombre='Numero1', opcional=True)
    campo.delete()
    assert CampoNumero.objects.count() == 0
    
@pytest.mark.django_db
def test_liminarItem():
    #Verifica la eliminación lógica de un ítem
    Item.objects.create(identificador='Item1', costo=1, descripcion='Descripcion')
    item = Item.objects.get(identificador='Item1')
    item.estado = 'Desactivado'
    item.save()
    assert Item.objects.get(identificador='Item1').estado == 'Desactivado'
